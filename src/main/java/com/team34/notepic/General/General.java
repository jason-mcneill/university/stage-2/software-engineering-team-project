package com.team34.notepic.General;



import com.team34.notepic.Security.UserAuthenticationProvider;
import com.team34.notepic.Security.UserSession;
import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.User.Admin;
import com.team34.notepic.User.Student;
import com.team34.notepic.User.Teacher;
import com.team34.notepic.User.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.*;

/**
 *     The General class acts as a controller for mapping links / performing methods that
 *     don't fit into any of the other class controllers or won't work if put in another
 *     controller e.g. the login/registration is best placed in the User class but won't
 *     work there due to User being a parent class.
 */
@Controller
@SessionAttributes("userSession")
@RequestMapping(
        method={RequestMethod.POST,RequestMethod.GET}
        //https://stackoverflow.com/questions/35498223/spring-boot-request-method-post-not-supported-tried-everything
)
public class General {


    /**
     * gets the user session
     *
     * @return the linked hash map of string to object which is used
     */
    @ModelAttribute("userSession")
    public LinkedHashMap<String, Object> setUpUserSession() {
        return new LinkedHashMap<String, Object>();
    }

    /**
     * Checks to see if the username and password grant access past the login, if true, the the user is given an
     * authentication token and an authority which represents the user role, then they are taken to their appropriate
     * user overview, if not, they are redirected back to the login pag
     *
     * @param parameters paramaters passed in from jsp - login credentials
     * @return the mapping for the user
     * @author Jason
     */
    @PostMapping("/login")
    // Redirect authenticated users back to their overview page
    public ModelAndView login(@RequestParam Map<String, String> parameters) {
        ModelAndView modelAndView = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            modelAndView = new ModelAndView("/user/login");
        } else {
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            redirect:
            for (GrantedAuthority grantedAuthority : authorities) {
                switch (grantedAuthority.getAuthority()) {
                    case "TEACHER":
                        modelAndView = new ModelAndView("redirect:/teacheroverview");
                        break redirect;
                    case "ADMIN":
                        modelAndView = new ModelAndView("redirect:/adminoverview");
                        break redirect;
                    case "STUDENT":
                        modelAndView = new ModelAndView("redirect:/studentoverview");
                        break redirect;
                }
            }
        }
        return modelAndView;
    }

    @Autowired
    private AuthenticationManagerBuilder authenticationManagerBuilder;

    @GetMapping("/registrationPage")
    public ModelAndView registrationPage(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        //take user to registration page
        return new ModelAndView("/user/register");
    }


    /**
     * Splits an array into an array list of array lists
     *
     * @param arrayToSplit the array list to split up
     * @param amount the number of values which the array should be split by e.g. every 4 values
     * @return an array list of array lists which are the split up parts of the original array
     * @author Ben
     */
    public static ArrayList<ArrayList<String>> splitByAmount(ArrayList<String> arrayToSplit, int amount){
        final ArrayList<ArrayList<String>> pairs = new ArrayList<>();
        if(arrayToSplit != null) {
            for (int i = 0; i < arrayToSplit.size(); i += amount) {
                pairs.add(new ArrayList<String>(arrayToSplit.subList(i, i + amount)));
            }
        }
        return pairs;
    }


    /**
     * Finds the topic name in the database from its topic ID, placed here as there is not a topic class.
     *
     * @param topicID to get name from
     * @return string value of topic name
     */
    public static String getTopicName(String topicID){
        String topicName = "";
        try{
            ArrayList<String> columns = new ArrayList<>();
            columns.add("TopicID");
            columns.add("TopicName");
            String condition = " WHERE " + "TopicID = " + "'" + topicID + "'";
            HashMap<String,  ArrayList<String>> topic = DatabaseCRUD.read("Topics", columns, condition);
            topicName = topic.get("TopicID").get(1);
            return topicName;
        }catch (Exception e){
            e.printStackTrace();
            return topicName;
        }
    }

    /**
     * mapping to log the user in
     * DELETING??
     * @param userSession the userSession hashmap
     * @return mapping to login jsp
     */
    @GetMapping("/teacherAccountLogin")
    public String teacherAccountLogin(@ModelAttribute("userSession") HashMap<String, Object> userSession){
        userSession.put("userRole", "Teacher");
        //take user to login page with set user role
        return "/user/login";
    }

    /**
     * DELETING??
     * @param userSession
     * @return
     */

    /**
     * DELETING??
     *
     * @param userSession
     * @return
     */
    @GetMapping("/studentAccountLogin")
    public String studentAccountLogin(@ModelAttribute("userSession") HashMap<String, Object> userSession){
        userSession.put("userRole", "Student");
        //take user to login page with set user role
        return "/user/login";
    }

    /**
     * Mapping to the front page
     *
     * @param userSession
     * @return mapping to the front page
     */
    @GetMapping("/")
    public ModelAndView mainPage(@ModelAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        return new ModelAndView("/index");
    }

    /**
     * Mapping to log out the user
     *
     * @param userSession
     * @return redirect mapping to front page
     */
    @GetMapping("/logout")
    public ModelAndView userLogout(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        UserSession.clearUserSession(userSession);
        return new ModelAndView("redirect:/");
    }

    /**
     * @param userSession
     * @return mapping to login page
     *  DELETING?
     */
    @GetMapping("/publiclogin")
    public ModelAndView publicLogin(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        return new ModelAndView("/public/login/index");
    }

    /**
     * mapping for user to change password
     * @return mapping to change password page
     */
    @GetMapping("/userrequestpasswordchange")
    public String userRequestPasswordChange(){
        return "/user/changepassword";
    }


    /**
     * Gets the user from the session, compares the password entered to their stored password. If they equal,
     * then the password is updated for their instance and the database
     *
     * @param parameters
     * @param model
     * @param userSession
     * @return
     */
    @PostMapping("/changeuserpassword")
    public String changeUserPassword(@RequestParam Map<String, String> parameters, ModelMap model,
                                     @ModelAttribute("userSession") HashMap<String, Object> userSession){
        if(!parameters.get("newPass").equals("")){
            User user = new User();
            if((Teacher)userSession.get("Teacher") != null){
                user = (Teacher)userSession.get("Teacher");
            }
            if((Student)userSession.get("Student") != null){
                user = (Student)userSession.get("Student");
            }
            if((Admin)userSession.get("Admin") != null){
                user = (Admin)userSession.get("Admin");
            }
            if (parameters.get("oldPass").equals(user.getPassword())){
                try{
                    user.updatePassword(parameters.get("newPass"));
                    model.addAttribute("passwordMessage", "Password Updated!");
                }
                catch(Exception e){
                    e.printStackTrace();
                    model.addAttribute("passwordMessage", "Failed to change password.");
                }
            }else{
                model.addAttribute("passwordMessage", "Incorrect Password");
            }
            if((Teacher)userSession.get("Teacher") != null){
                assert user instanceof Teacher;
                userSession.put("Teacher",(Teacher)user);
            }
            if((Student)userSession.get("Student") != null){
                assert user instanceof Student;
                userSession.put("Student",(Student)user);
            }
            if((Admin)userSession.get("Admin") != null){
                assert user instanceof Admin;
                userSession.put("Admin",(Admin)user);
            }}
        else{
            model.addAttribute("passwordMessage", "New password can not be blank.");
        }
        return "/user/changepassword";
    }

    /**
     * When a parameter returned is an Array, it is returned as a string so this function converts the string back
     * to an array
     *
     * @param quizData the array as a string
     * @return the string converted to an array
     */
    public static String[] getArrayFromDataString(String quizData){
        quizData = quizData.replace("[","");
        quizData = quizData.replace("]","");
        quizData = quizData.replace(" ","");
        String[] quizDataArray = quizData.split(",");
        return quizDataArray;
    }


    public static String calcPercentage(String studentMark, String totalMarks){
        double percentage = Math.round((Double.parseDouble(studentMark)/Double.parseDouble(totalMarks)) * 100);
        return Double.toString(percentage);
    }


    /**
     * Depending on the user role, an instance of the user is created and added to the database, then the user is
     * granted an appropriate authority (role) and then sent to their appropriate index page
     *
     * @param parameters the values the user enters into the registration form
     * @param model a container for data passed from controller to jsps
     * @param userSession hash map which stores values that can be access anywhere in the session
     * @return the mapping to the appropriate user page.
     * @throws SQLException
     */
    @PostMapping("/registerUser")
    public String registerUser(@RequestParam Map<String, String> parameters, ModelMap model,
                               @ModelAttribute("userSession") LinkedHashMap<String, Object> userSession) throws SQLException {
        //set input from jsp form as parameters
        String userRole = parameters.get("userRole");
        String username;
        //IF STATEMENTS FOR "CREATE ADMIN/TEACHER/STUDENT" DEPENDING ON ROLE SELECTED
        //if userRole = Admin/Teacher/Student
        switch (userRole){
            case "Admin":
                //Create Admin
                //set input from webpage as parameters
                Admin admin = new Admin();
                username = uniqueUsername(Character.toUpperCase(parameters.get("firstName").charAt(0)) + parameters.get("lastName"), 0);
                admin.setUsername(username);
                admin.setPassword(parameters.get("password"));
                admin.setUserRole("Admin");
                admin.setFirstName(parameters.get("firstName"));
                admin.setLastName(parameters.get("lastName"));
                admin.setDOB(parameters.get("dateOfBirth"));
                admin.setEmail(parameters.get("email"));
                admin.setNameOfSchool(parameters.get("nameOfSchool"));
                admin.addToDatabase();
                userSession.put("Admin", admin);
                model.addAttribute("message", "An admin has been added!");
                // Create authentication
                ArrayList<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
                grantedAuthorityList.add(new SimpleGrantedAuthority("ADMIN"));
                // Add an admin authentication token and sign them in automatically
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, parameters.get("password"), grantedAuthorityList);
                UserAuthenticationProvider authenticationProvider = new UserAuthenticationProvider();
                Authentication authentication = authenticationProvider.authenticate(authToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return "/admin/overview/index";
            case "Teacher":
                Teacher teacher = new Teacher();
                username = uniqueUsername(Character.toUpperCase(parameters.get("firstName").charAt(0)) + parameters.get("lastName"), 0);
                teacher.setUsername(username);
                teacher.setPassword(parameters.get("password"));
                teacher.setUserRole("Teacher");
                teacher.setFirstName(parameters.get("firstName"));
                teacher.setLastName(parameters.get("lastName"));
                teacher.setDOB(parameters.get("dateOfBirth"));
                teacher.setEmail(parameters.get("email"));
                teacher.setTitle(parameters.get("title"));
                teacher.setTeacherRole(parameters.get("teacherRole"));
                teacher.addToDatabase();
                model.addAttribute("message", "A teacher has been added!");
                return "/admin/overview/index";
            case "Student":
                Student student = new Student();
                username = uniqueUsername(Character.toUpperCase(parameters.get("firstName").charAt(0)) + parameters.get("lastName"), 0);
                student.setUsername(username);
                student.setPassword(parameters.get("password"));
                student.setUserRole("Student");
                student.setFirstName(parameters.get("firstName"));
                student.setLastName(parameters.get("lastName"));
                student.setDOB(parameters.get("dateOfBirth"));
                student.setEmail(parameters.get("email"));
                student.setYearOfStudy(parameters.get("yearOfStudy"));
                student.addToDatabase();
                model.addAttribute("message", "A student has been added!");
                return "/admin/overview/index";
        }
        return "/index";
    }

    /**
     * Adds a number to the end of a username which is not unique to make it unique, number is iterated, if the username
     * is in use
     *
     * @param username the username which is not unique which needs to be
     * @param count the count which finds the value that should be added to the username
     * @return the new unique username with number appended to it
     */
    public String uniqueUsername(String username, int count) {
        ArrayList<String> tableColumns = new ArrayList<>();
        HashMap<String, ArrayList<String>> results = new HashMap<>();
        tableColumns.add("Username");
        if (count == 0) {
            // If the count is 0, check if the username entered is unique
            results = DatabaseCRUD.read("Users", tableColumns, " WHERE Users.Username = '" + username + "'");
            // If the username already exists
            if (results.size() != 0) {
                count += 1;
                // Check the new username is unique
                username = uniqueUsername(username, count);
            }
        } else {
            // If the count isn't 0, check the number appended to the username is also unique
            results = DatabaseCRUD.read("Users", tableColumns, " WHERE Users.Username = '" + username + count + "'");
            if (results.size() != 0) {
                count += 1;
                // Check the new username is unique
                username = uniqueUsername(username, count);
            } else {
                username += count;
            }
        }
        return username;
    }



}
