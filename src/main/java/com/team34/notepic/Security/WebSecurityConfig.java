package com.team34.notepic.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.writers.frameoptions.WhiteListedAllowFromStrategy;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;

import java.util.Arrays;

/**
 * A configuration class for Spring security which contains the mapping allowed for each authority (role)
 * @author Jason
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserAuthenticationProvider authenticationProvider;

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
    }

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                    // Deny the following links to authenticated users to but allow to visiting users
                    .mvcMatchers("/", "/adminlogin", "/publiclogin", "/login", "/registrationPage").access("not (hasAnyAuthority('ADMIN', 'TEACHER', 'STUDENT'))")
                    // The following are accessible to admins only
                    .mvcMatchers("/adminoverview", "/adminimportpublicdata", "/adminviewimportpublicdata",
                            "/createpublicaccount", "/addgroupuser", "/removegroupuser", "/adminviewgroup",
                            "/admincreategroup", "/admindeletegroup").hasAuthority("ADMIN")
                    // The following are accessible to teachers only
                    .mvcMatchers("/teacheroverview", "/teacherviewgroup", "/teacherviewlearningmaterial",
                            "/teacherviewquiz", "/teacherviewassignments", "/teacherviewstudentprogress",
                            "/teachergroupprogress", "/teacherstudentprogress", "/teacherviewresults" ,
                            "/teachercreatequiz", "/teachercreateassignment" , "/teacherviewquizzes",
                            "/teacherupdatequizname", "/teacherupdatequizdescription", "/teacherupdatequizstatus",
                            "/teacherdeletequiz", "/teacherdeletequestion", "/teacheraddquizquestion",
                            "/deletelearningmaterial",  "/updatelearningmaterial", "/createlearningmaterial",
                            "/update-feedback", "/view-submission", "/teacherupdateassignmenttopic",
                            "/teacherupdateassignmentduedatetime", "/teacherupdateassignmentstatus",
                            "/teacherupdateassignmentmarks", "/teacherupdateassignmentname",
                            "/teacher-topic-overview", "/createtopic", "/amendtopic", "/deletetopic").hasAuthority("TEACHER")
                    // The following are accessible to students only
                    .mvcMatchers("/studentoverview", "/studentviewgroup", "/studentviewlearningmaterial",
                            "/studentviewquiz", "/studentviewassignment", "/studentviewquizzes",
                            "/studentcompleteassignment", "/studentcompletequiz",
                            "/studentanswerquestion", "/add-rating",
                            "/studentselectquestion", "/studentsubmitquiz",  "/submitassignmentpdf",
                            "/amend-personal-goal", "/studentviewassignments"
                            ).hasAuthority("STUDENT")
                    // The following can be accessed by all
                    .mvcMatchers("/registerUser" , "/changeuserpassword", "/userrequestpasswordchange", "/selectLearningMaterial").permitAll()
                // Allow all access to resources
                .antMatchers("/css/**").permitAll()
                .and()
                .authorizeRequests()
                    // Any other requests require the user be authenticated to access
                    .anyRequest().fullyAuthenticated()
                .and()
                .formLogin()
                    // The page to redirect users to if not signed in and accessing a private page
                    .loginPage("/login")
                    // This is the url to send to validate the credentials
                    .loginProcessingUrl("/process_login")
                    // Handle redirecting the user if successful
                    .successHandler(new SuccessHandler())
                    .permitAll()
                    .and()
                .logout();
    }
}
