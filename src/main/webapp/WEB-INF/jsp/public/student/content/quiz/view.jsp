<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${userSession.Quiz.name} - Preview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<body>
<h1>${userSession.Quiz.name}</h1>
<h1>${userSession.Quiz.description}</h1>
<h1>Total Marks: ${userSession.Quiz.totalScore}</h1>
<h2>Questions: </h2>

<table>
    <tr>
        <th>Title</th>
        <th>Mark's Worth</th>
    </tr>
    <ul>
        <c:forEach var="question" items="${userSession.quizQuestions}">
        <tr>
            <td>${question[2]}</td>
            <td>${question[8]}</td>
        </tr>
    </c:forEach>
    </ul>
</table>
<p><a href="/studentcompletequiz">Start Quiz</a></p>
<p><a href="/studentviewgroup">Back to ${userSession.Group.name} Overview</a></p>
</body>
</html>
