package com.team34.notepic.Content;

import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.General.General;
import com.team34.notepic.Group;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.team34.notepic.General.General.getTopicName;

/**
 *A class which is used as an instance of Assignments, containing all functionality done for assignments.
 * @SessionAttribute A hashmap of type string -> object, can be used whenever the session is available
 */
@SessionAttributes("userSession")
public class Assignment {

    private String assignmentID;
    private String groupID;
    private String assignmentType;
    private String name;
    private String markWeight;
    private String questionPDFLink;
    private String dueDateTime;
    private String status;
    private String topicID;

    public void setAssignmentID(String assignmentID) { this.assignmentID = assignmentID; }
    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }
    public void setAssignmentType(String assignmentType) {
        this.assignmentType = assignmentType;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setMarkWeight(String markWeight) { this.markWeight = markWeight; }
    public void setQuestionPDFLink(String questionPDFLink) {
        this.questionPDFLink = questionPDFLink;
    }
    public void setDueDateTime(String dueDateTime) { this.dueDateTime = dueDateTime; }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setTopicID(String topicID) { this.topicID = topicID; }


    public String getAssignmentID() {return this.assignmentID; }
    public String getGroupID() {return this.groupID; }
    public String getAssignmentType() {return this.assignmentType; }
    public String getName() {return this.name; }
    public String getMarkWeight() {return this.markWeight; }
    public String getQuestionPDFLink() {return this.questionPDFLink; }
    public String getDueDateTime() {return this.dueDateTime; }
    public String getStatus() {return this.status; }
    public String getTopicID() {return this.topicID; }

    /**
     * Creates a temporary assignment in the database
     * @author Ben
     */
    public void createTempInDatabase() {
        Map<String,String> tableContents = new HashMap<>();
        tableContents.put("AssignmentID", this.assignmentID);
        tableContents.put("DisplayName", this.name);
        tableContents.put("GroupID", this.groupID);
        String tableName = "Assignments";
        DatabaseCRUD.create(tableName, tableContents);
    }

    /**
     * From the assignment instances group ID to create condition to get all assignments in the group
     * @return An array list of array lists, each array list represents an assignment and its associated data.
     * @author Ian
     */
    public ArrayList<ArrayList<String>> getAssignmentsFromGroupID(){
        try {
            ArrayList<String> assignmentsColumns = new ArrayList<>();
            assignmentsColumns.add("GroupID");
            assignmentsColumns.add("AssignmentID");
            assignmentsColumns.add("AssignmentType");
            assignmentsColumns.add("DisplayName");
            assignmentsColumns.add("MarkWeight");
            assignmentsColumns.add("QuestionPDFLink");
            assignmentsColumns.add("DueDate");
            assignmentsColumns.add("Status");
            assignmentsColumns.add("TopicID");
            String condition = " WHERE " + "GroupID = " + "'" + this.groupID + "'";
            HashMap<String, ArrayList<String>> assignmentsMap = DatabaseCRUD.read("Assignments", assignmentsColumns, condition);
            assert assignmentsMap != null: "assignmentsMap is 'null'";
            ArrayList<String> assignmentData = assignmentsMap.get("GroupID");
            return General.splitByAmount(assignmentData, 9);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * Sets the instance ID to the ID found in the database where the name is equal to the name of the assignment
     * instance
     * @author Ben
     */
    public void getIDFromName(){
        try {
            ArrayList<String> assignmentsColumns = new ArrayList<>();
            assignmentsColumns.add("AssignmentID");
            assignmentsColumns.add("DisplayName");
            String condition = " WHERE " + "DisplayName = " + "'" + this.name + "'";
            HashMap<String, ArrayList<String>> assignmentsMap = DatabaseCRUD.read("Assignments", assignmentsColumns, condition);
            assert assignmentsMap != null;
            ArrayList<String> assignmentData = assignmentsMap.get("AssignmentID");
            this.assignmentID = assignmentData.get(0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Updates the database with the values from the instance which calls the function.
     * @author Ian
     */
    public void updateDatabase(){
        try {
            String condition = "AssignmentID = '" + this.assignmentID + "'";
            DatabaseCRUD.update("Assignments", "AssignmentType", this.assignmentType, condition);
            DatabaseCRUD.update("Assignments", "DisplayName", this.name, condition);
            DatabaseCRUD.update("Assignments", "MarkWeight", this.markWeight, condition);
            DatabaseCRUD.update("Assignments", "QuestionPDFLink", this.questionPDFLink, condition);
            DatabaseCRUD.update("Assignments", "Status", this.status, condition);
            DatabaseCRUD.update("Assignments", "DueDate", this.dueDateTime, condition);
            DatabaseCRUD.update("Assignments", "TopicID", this.topicID, condition);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * For both assignment types, the students mark is retrieved and an average is calculated from their score andd the
     * assignment total marks, then an array list is returned. If the mark does not in exist in the database, the mark
     * is returned as incomplete and the percentage is 'N/A', if the mark is 'unmarked' in the database, this is
     * returned with a percentage of 0, if the mark is neither and a number, this is returned with the calculated
     * percentage
     *
     * @param username The username of the student who's data is being retrieved
     * @return An array list of the mark, percentage, assignment type, and date completed
     * @author Ben
     */
    public ArrayList<String> submissionData(String username){
        ArrayList<String> Username = new ArrayList<>();
        Username.add(username);
        try {
            ArrayList<String> subData = new ArrayList<>();
            boolean completed = false;
            if (this.assignmentType.equals("PDF")) {
                String mark = getPDFSubMarkFromUsername(username);
                if (mark == null) {
                    subData.add("Incomplete");
                    subData.add("N/A");
                    subData.add("PDF");
                } else {
                    if (mark.equals("unmarked")) {
                        completed = true;
                        subData.add(mark);
                        subData.add("0");
                        subData.add("PDF");
                    } else {
                        completed = true;
                        subData.add(mark);
                        subData.add(General.calcPercentage(mark, this.markWeight));
                        subData.add("PDF");
                    }
                }
            }

            if (this.assignmentType.equals("Quiz")) {
                Quiz quiz = new Quiz();
                quiz.setAssignmentID(this.assignmentID);
                if(getQuizIDFromAssignmentID() != null){
                    completed = true;
                    quiz.setQuizID(getQuizIDFromAssignmentID());
                    quiz.updateClassFromDatabase();
                    ArrayList<ArrayList<String>> results = quiz.getResultData(Username);
                    if (results.isEmpty()){
                        completed = false;
                    } else{
                        if(results.get(0).get(1).equals("incomplete")){
                            subData.add("Incomplete");
                            subData.add("N/A");
                            subData.add("Quiz");
                        }else {
                            subData.add(results.get(0).get(1));
                            subData.add(results.get(0).get(2));
                            subData.add("Quiz");
                        }
                    }
                } else {
                    subData.add("Incomplete");
                    subData.add("N/A");
                    subData.add("Quiz");
                }
            }
            if(!completed){
                subData.add("N/A");
            }else{
                if(getWhenCompleted(username) == null){
                    subData.add("N/A");
                }else {
                    subData.add(getWhenCompleted(username));
                }
            }

            return subData;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * For an instance of assignment, this function finds the date the assignment was submitted for a given student,
     * referring between two different tables in the database, depending on the assignment type.
     *
     * @param username The username of the student to find when the assignment.
     * @return Returns the date from when the assignment was submitted.
     * @author Ben
     */
    public String getWhenCompleted(String username) {
        ArrayList<String> columns = new ArrayList<>();
        HashMap<String, ArrayList<String>> assignmentsMap;
        columns.add("DateTimeSubmitted");
        String condition = " WHERE " + "AssignmentID = '" + this.assignmentID + "' AND Username = '" + username + "'";
        if(this.assignmentType.equals("Quiz")) {
            assignmentsMap = DatabaseCRUD.read("AssignmentSubmissionQuiz", columns, condition);
        }else{
            assignmentsMap = DatabaseCRUD.read("AssignmentSubmissionPDF" , columns, condition);
        }
        if (assignmentsMap.isEmpty()){
            return null;
        }else{
            ArrayList<String> assignmentData = assignmentsMap.get("DateTimeSubmitted");
            return assignmentData.get(0);
        }
    }

    /**
     * Returns the mark of an assignment from a students submission, which the teacher enters. This can be 'unmarked' -
     * if the student has completed the assignment, null - if there is no entry, or a numberical value set by the
     * teacher.
     *
     * @param username The username of the student to retrieve their mark
     * @return the teachers mark for the instance of assignment when the type is PDF
     * @author Ben
     */
    public String getPDFSubMarkFromUsername(String username){
        try {
            ArrayList<String> columns = new ArrayList<>();
            columns.add("TeacherMark");
            String condition = " WHERE " + "AssignmentID = '" + this.assignmentID + "' AND Username = '" + username + "'";
            HashMap<String, ArrayList<String>> assignmentsMap = DatabaseCRUD.read("AssignmentSubmissionPDF", columns, condition);
            if (assignmentsMap.isEmpty()){
                return null;
            }else{
                ArrayList<String> assignmentData = assignmentsMap.get("TeacherMark");
                return assignmentData.get(0);
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Uses the AssignmentQuizzes table which links assignments to the type quiz to return ...
     *
     * @return the quizID associated with the assignment ID
     * @author Ben
     */
    public String getQuizIDFromAssignmentID(){
        try {
            ArrayList<String> columns = new ArrayList<>();
            columns.add("AssignmentID");
            columns.add("QuizID");
            String condition = " WHERE AssignmentID = '" + this.assignmentID + "'";
            HashMap<String, ArrayList<String>> assignmentsMap = DatabaseCRUD.read("AssignmentQuizzes", columns, condition);
            if (assignmentsMap.isEmpty()){
                return null;
            }else{
                ArrayList<String> assignmentData = assignmentsMap.get("QuizID");

                return assignmentData.get(1);
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Removes the link between an assignment and quiz then removes the associated quiz from the database.
     * @author Ben
     */
    public void removeQuizLink() {
        try {
            String condition = " WHERE AssignmentID = '" + this.assignmentID + "'";
            DatabaseCRUD.delete("AssignmentQuizzes", condition);
            String quizID = this.getQuizIDFromAssignmentID();
            Quiz quiz = new Quiz();
            quiz.setQuizID(quizID);
            quiz.removeFromDatabase();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void updateTotalScore(){
        try {
            String condition = "AssignmentID = '" + this.assignmentID + "'";
            DatabaseCRUD.update("Assignments", "MarkWeight", this.markWeight, condition);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Removes the assignment from the associated database, depending on the assignment type
     */
    public void removeFromDatabase() {
        String condition = " WHERE AssignmentID = '" + this.assignmentID + "'";
        if (this.assignmentType.equals("PDF")) {
            DatabaseCRUD.delete("AssignmentSubmissionPDF", condition);
        }else {
            DatabaseCRUD.delete("AssignmentSubmissionQuiz", condition);
        }
        DatabaseCRUD.delete("Assignments", condition);
    }

    /**
     *Gathers the data on an assignment from the database, from the instance of assignment's ID
     * @author Ian
     */
    public void updateClassFromDatabase() {
        ArrayList<String> assignmentTableColumns = new ArrayList<>();
        assignmentTableColumns.add("AssignmentID");
        assignmentTableColumns.add("GroupID");
        assignmentTableColumns.add("AssignmentType");
        assignmentTableColumns.add("DisplayName");
        assignmentTableColumns.add("MarkWeight");
        assignmentTableColumns.add("QuestionPDFLink");
        assignmentTableColumns.add("DueDate");
        assignmentTableColumns.add("Status");
        assignmentTableColumns.add("TopicID");
        String condition = " WHERE " + "AssignmentID = " + "'" + this.assignmentID + "'";
        HashMap<String, ArrayList<String>> data = DatabaseCRUD.read("Assignments", assignmentTableColumns, condition);
        assert data != null : "Data is 'null'";
        this.groupID = data.get("AssignmentID").get(1);
        this.assignmentType = data.get("AssignmentID").get(2);
        this.name = data.get("AssignmentID").get(3);
        this.markWeight = data.get("AssignmentID").get(4);
        this.questionPDFLink = data.get("AssignmentID").get(5);
        this.dueDateTime = data.get("DueDate").get(6);
        this.status = data.get("Status").get(7);
        this.topicID = data.get("TopicID").get(8);
    }

    /**
     * Adds the students submission data to a database, including a time stamp, link to the submission, their username,
     *  the associated assignment ID and mark. Adds the value 'unmarked' so the teacher can add their mark here.
     *
     * @param username the username of the student who submitted the assignment
     * @param pdfLink the link of the PDF submitted by the student to be uploaded to AWS File Storage System
     * @author Jason
     */
    public void submitAssignmentPDF(String username, String pdfLink) {
        // Create the table columns for the assignment submission pdf page
        Map<String,String> tableContents = new HashMap<>();
        tableContents.put("Username", username);
        tableContents.put("AssignmentID", this.assignmentID);
        tableContents.put("SubmissionPDFLink", pdfLink);
        tableContents.put("DateTimeSubmitted" , new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
        // By default the pdf will be unmarked until the teacher submits a mark
        tableContents.put("TeacherMark", "unmarked");
        DatabaseCRUD.create("AssignmentSubmissionPDF", tableContents);
    }


    /**
     * Updates the topics assignments array for every assignment in every topic
     *
     * @param topics the subgroup to of groups to split up content
     * @param students an array list of student usernames in the group
     * @return an array list of array lists with data on all assignments in the all topics
     * @throws SQLException
     * @author Ben
     */
    public ArrayList<ArrayList<String>> getTopicAssignmentData(String[][] topics, ArrayList<String> students) throws SQLException {
        ArrayList<String> topicAssignments = new ArrayList<>();
        for (String[] topicData : topics) {
            Topic topic = new Topic();
            topic.setTopicID(topicData[0]);
            topic.updateClassFromDatabase();
            getAssignmentData(students, topicAssignments, topic);
        }
        return General.splitByAmount(topicAssignments, 6);
    }

    /**
     * For every topic assignment, gets the assignment name, topic ID, minimum mark (%), max mark (%), average (%).
     *
     *
     * @param students an array list of all the students in a group
     * @param topicAssignments an array list of all assignments in a topic
     * @param topic an instance of a topic
     * @return a 2D array of topic assignment data
     * @author Ben
     */
    public String[][] getAssignmentData(ArrayList<String> students, ArrayList<String> topicAssignments, Topic topic) {
        String[][] assignmentsInTopic = topic.getAssignmentsInTopic();
        for (String[] assignmentTopicPair : assignmentsInTopic) {
            this.setAssignmentID(assignmentTopicPair[1]);
            this.updateClassFromDatabase();
            topicAssignments.add(this.name);
            topicAssignments.add(this.topicID);
            double maxPercent = 0;
            double minPercent = 100;
            int count = 0;
            double avgPercent = 0;
            if (this.getAssignmentType().equals("Quiz")){
                Quiz quiz = new Quiz();
                quiz.setQuizID(this.getQuizIDFromAssignmentID());
                quiz.updateClassFromDatabase();
                ArrayList<ArrayList<String>> resultData = quiz.getResultData(students);
                String percent;
                for (ArrayList<String> result : resultData){
                    percent = result.get(2);
                    if (!percent.equals("incomplete")){
                        double percentage = Double.parseDouble(percent);
                        if (percentage > maxPercent){
                            maxPercent = percentage;
                        }
                        if (percentage < minPercent){
                            minPercent = percentage;
                        }
                        avgPercent += percentage;
                        count += 1;
                    }
                }
                if(count == 0){
                    count = 1;
                }
                avgPercent = avgPercent/count;
                topicAssignments.add(Double.toString(maxPercent));
                topicAssignments.add(Double.toString(minPercent));
                topicAssignments.add(Double.toString(avgPercent));
            }else{
                maxPercent = 0;
                minPercent = 100;
                for (String username : students) {
                    this.assignmentID = this.getAssignmentID();
                    String mark = this.getPDFSubMarkFromUsername(username);
                    if (mark != null && !mark.equals("incomplete") && !mark.equals("unmarked")){
                        double percentage = (Double.parseDouble(General.calcPercentage(mark, this.getMarkWeight())));
                        if (percentage > maxPercent){
                            maxPercent = percentage;
                        }
                        if (percentage < minPercent){
                            minPercent = percentage;
                        }
                        avgPercent += percentage;
                        count += 1;
                    }
                }
                if (avgPercent == 0){
                    topicAssignments.add("0");
                    topicAssignments.add("0");
                    topicAssignments.add("0");
                }else{
                    avgPercent = avgPercent/count;
                    topicAssignments.add(Double.toString(maxPercent));
                    topicAssignments.add(Double.toString(minPercent));
                    topicAssignments.add(Double.toString(avgPercent));
                }
            }
            System.out.println(this.assignmentID);
            topicAssignments.add(this.assignmentID);
        }
        return assignmentsInTopic;
    }

    /**
     *Returns data on the student including their name, percentage, when the completed the assignment and the assignment
     * ID
     *
     * @param username the username of the student to retrieve a percentage for
     * @return an array list of results data
     * @throws SQLException
     * @author Ben
     */
    public ArrayList<String> getStudentPercentage(String username) throws SQLException {
        ArrayList<String> assignmentData = new ArrayList<>();
        ArrayList<String> student = new ArrayList<>();
        student.add(username);
        double percentage = 0;
        if (this.assignmentType.equals("Quiz")) {
            Quiz quiz = new Quiz();
            quiz.setQuizID(this.getQuizIDFromAssignmentID());
            quiz.updateClassFromDatabase();
            ArrayList<ArrayList<String>> resultData = quiz.getResultData(student);
            String percent = null;
            for (ArrayList<String> result : resultData) {
                percent = result.get(2);
                if (!percent.equals("incomplete")) {
                    percentage = Double.parseDouble(percent);
                }
            }
        }else {
            this.assignmentID = this.getAssignmentID();
            String mark = this.getPDFSubMarkFromUsername(username);
            if (mark != null && !mark.equals("incomplete") && !mark.equals("unmarked")) {
                percentage = (Double.parseDouble(General.calcPercentage(mark, this.getMarkWeight())));
            }
        }
        if(!this.status.equals("Hidden")) {
            assignmentData.add(this.name);
            assignmentData.add(Double.toString(percentage));
            assignmentData.add(getWhenCompleted(username));
            assignmentData.add(this.assignmentID);
        }
        return assignmentData;
    }

    /**
     * for the topic, returns an array list of an array list with assignment data for list of students
     *
     * @param topicID the topic to retrieve the assignment data for
     * @param students an array list of student usernames in the group
     * @return an array list of the assignment data in the topic
     * @author Ben
     */
    public ArrayList<ArrayList<String>> getAssignmentDataForTopic(String topicID, ArrayList<String> students)  {
        try {
            ArrayList<String> topicAssignments = new ArrayList<>();
            Topic topic = new Topic();
            topic.setTopicID(topicID);
            topic.updateClassFromDatabase();
            getAssignmentData(students, topicAssignments, topic);
            return General.splitByAmount(topicAssignments, 6);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * adds assignments submission of type quiz is added to the associated table with time stamp and their mark
     *
     * @param username the username of the student who completed the assignment
     * @param mark the mark calculated automatically when the student completes the quiz
     */
    public void submitAssignmentQuiz(String username, String mark)  {
        Map<String,String> tableContents = new HashMap<>();
        tableContents.put("Username", username);
        tableContents.put("AssignmentID", this.assignmentID);
        tableContents.put("DateTimeSubmitted" , new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
        // By default the pdf will be unmarked until the teacher submits a mark
        tableContents.put("StudentMark", mark);
        DatabaseCRUD.create("AssignmentSubmissionQuiz", tableContents);
    }

    /**
     * returns the values of a students submission, the pdf link, mark, time stamp, username and feedback
     *
     * @param username the students username to retrieve the submission data for
     * @return an array of the submission data
     */
    public String[] getPDFSubmission(String username) {
        ArrayList<String> pdfTableColumns = new ArrayList<>();
        pdfTableColumns.add("SubmissionPDFLink");
        pdfTableColumns.add("TeacherMark");
        pdfTableColumns.add("DateTimeSubmitted");
        pdfTableColumns.add("Username");
        pdfTableColumns.add("Feedback");
        String condition = " WHERE " + "Username = " + "'" + username + "'" + " AND AssignmentID = '" + this.assignmentID + "'";
        String [] pdfSubmission = DatabaseCRUD.readAlt("AssignmentSubmissionPDF", pdfTableColumns, condition)[0];
        if (pdfSubmission != null) {
            return pdfSubmission;
        }else{
            return null;
        }
    }

    public void updatePDFSubmission(String username, String teacherMark, String feedback) {
        String condition = "Username = '" + username + "' AND AssignmentID = '" + this.assignmentID + "'";
        DatabaseCRUD.update("AssignmentSubmissionPDF", "TeacherMark", teacherMark, condition);
        DatabaseCRUD.update("AssignmentSubmissionPDF", "Feedback", feedback, condition);
    }

    public static void updateAssignmentsArrays(Model model,
                                               @ModelAttribute("userSession") HashMap<String, Object> userSession){

        ArrayList<ArrayList<String>> liveAssignments = new ArrayList<>();
        ArrayList<ArrayList<String>> closedAssignments = new ArrayList<>();
        ArrayList<ArrayList<String>> hiddenAssignments = new ArrayList<>();

        Group group = (Group) userSession.get("Group");
        Assignment assignment = new Assignment();
        assignment.setGroupID(group.getGroupID());
        ArrayList<ArrayList<String>> assignments = assignment.getAssignmentsFromGroupID();
        if(!assignments.isEmpty()){
            for (ArrayList<String> assignmentInDB : assignments){
                assignmentInDB.set(8, getTopicName(assignmentInDB.get(8)));
                String assignmentDateTimeString = assignmentInDB.get(6);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                try {
                    Date assignmentDateTime = format.parse(assignmentDateTimeString);
                    if (assignmentDateTime.before(new Date())){
                        closedAssignments.add(assignmentInDB);
                    }else{
                        if (assignmentInDB.get(7).equals("Live")){
                            liveAssignments.add(assignmentInDB);
                        }
                        if (assignmentInDB.get(7).equals("Hidden")){
                            hiddenAssignments.add(assignmentInDB);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }else{
            model.addAttribute("assignmentsMessage", "You haven't created any assignments yet!");
        }
        userSession.put("liveAssignments", liveAssignments);
        userSession.put("hiddenAssignments", hiddenAssignments);
        userSession.put("closedAssignments", closedAssignments);
        userSession.put("assignments", assignments);
    }
}
