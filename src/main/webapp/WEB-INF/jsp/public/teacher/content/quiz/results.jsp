<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${userSession.Quiz.name} - Results</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/></head>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - QUIZ RESULTS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>

<h1>${userSession.Quiz.name} Results:</h1>
<h3>Total Marks: ${userSession.Quiz.totalScore}</h3>
<table>
    <tr>
        <th>Student</th>
        <th>Mark</th>
        <th>Percentage</th>
    </tr>
    <ul>
        <c:forEach var="studentVals" items="${userSession.resultData}">
            <tr>
                <td>${studentVals[0]}</td>
                <td>${studentVals[1]}</td>
                <td>${studentVals[2]}</td>
            </tr>
        </c:forEach>
    </ul>
</table>
<p>${resultsMessage}</p>
<p><a href="/teacherviewquizzes">Back to ${userSession.Group.name} Quizzes</a></p>
</body>
</html>
