<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Ben
  Date: 12/01/2021
  Time: 23:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${userSession.Assignment.name} - Results</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/></head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW RESULTS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<h1>${userSession.Assignment.name} Results:</h1>
<h3>Total Marks: ${userSession.Assignment.markWeight}</h3>
<h3>Due Date: ${userSession.Assignment.dueDateTime}</h3>

<table>
    <tr>
        <th>Student</th>
        <th>Mark</th>
        <th>Percentage</th>
        <th>Submitted On</th>
        <th id="view" hidden="hidden">View</th>
    </tr>
    <ul>
        <c:forEach var="studentVals" items="${userSession.resultData}">
            <tr>
                <td>${studentVals[4]}</td>
                <td>${studentVals[0]}</td>
                <td>${studentVals[1]}</td>
                <td>${studentVals[3]}</td>
                <c:if test="${studentVals[2] == 'PDF'}">
                    <c:if test="${studentVals[0] != 'Incomplete'}">
                        <form action="/view-submission" method="post">
                            <input type="hidden" id="username" name="username" value="${studentVals[4]}">
                            <c:choose>
                                <c:when test="${studentVals[0] == 'unmarked'}">
                                    <td>
                                        <button type="submit">Mark</button>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td>
                                        <button type="submit">View</button>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                        </form>
                    </c:if>
                </c:if>
            </tr>
        </c:forEach>
    </ul>
</table>

<p>${resultsMessage}</p>
<p><a href="/teacherviewassignments">Back to ${userSession.Group.name} Assignments</a></p>
</body>

<script>
    // If any of the assignments are pdfs display the view title
    var isPDF = false;
    <c:forEach var="studentVals" items="${userSession.resultData}">
        <c:if test="${studentVals[2] == 'PDF'}">
            isPDF = true;
        </c:if>
    </c:forEach>
    // Enable the title
    if (isPDF) {
        document.getElementById("view").hidden = false;
    }
</script>

</html>