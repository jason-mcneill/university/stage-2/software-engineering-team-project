<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Admin Overview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/admin.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>ADMINISTRATOR - OVERVIEW</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Your School Overview</h1>

<h3>Welcome Administrator ${userSession.Admin.firstName}, of ${userSession.Admin.nameOfSchool}.</h3>
<i>Your Details:</i><br>
<label>Username: ${userSession.Admin.username}</label><br>
<label>First Name: ${userSession.Admin.firstName}</label><br>
<label>Last Name: ${userSession.Admin.lastName}</label><br>
<label>Name of School: ${userSession.Admin.nameOfSchool}</label><br>

<p><a href="/adminviewimportpublicdata">Import School Users Data from CSV</a></p>

<p><a href="/createpublicaccount">Create New Teacher or Student Account</a></p>

<p>${message}</p>

<h1>The Groups in your School:</h1>
<i>Select a group to add or remove Teachers and Students to them.</i>
<br><br>
<form id="teacherSelectGroup" action="/adminviewgroup" method="POST">
    <select id="groupName" name="groupName">
        <option value="" selected disabled>Select Group</option>
        <c:forEach var="name" items="${userSession.groupNames}">
            <option value="${name}">${name}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Submit">
</form>
<p>${selectGroupMessage}</p>
<h2>Change the School's Groups</h2>
<form action="/admincreategroup">
        <label for="nameToCreate">Enter Group Name to Create: </label>
        <input type="text" id="nameToCreate" name="nameToCreate">
        <input type="submit" value="Submit">
</form>
<br>
<form action="/admindeletegroup">
    <label for="nameToDelete">Enter Group Name to Delete: </label>
    <input type="text" id="nameToDelete" name="nameToDelete">
    <input type="submit" value="Submit">
</form>
<p>${groupmessage}</p>
</body>
</html>