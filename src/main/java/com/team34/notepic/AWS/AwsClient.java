package com.team34.notepic.AWS;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@Service
public class AwsClient {

    private AmazonS3 s3client;

    /**
     * The IAM user credentials that are used to authorise a connection to AWS S3 along with the region that
     * the EPiC Headway buckets are stored in.
     */
    private String rootBucketName;
    private String accessKey = "";
    private String secretKey = "";
    private String region = "eu-west-2";

    /**
     * This will create an instance of the AWS credentials object, taking the IAM access key and secret key
     * as parameters to authorise the credential instance. An S3 client is then setup using the client builder,
     * establishing the s3 region to connect to.
     * @author Jason
     */
    public void initializeAmazon() {
        BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKey, secretKey);
        s3client = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
                .build();
    }

    /**
     * This function takes a multipart file (a representation of a file that is received over multiple requests), creates
     * a new file stream and returns a singular file
     * @param file The multipart file object being sent via multiple http requests from the relevant webpage.
     * @return Returns the file uploaded by the user as a File object
     * @throws IOException Throw an error if there is an issue with the multi-request file passed through or conversion
     * into a File object
     * @author Jason
     */
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    /**
     * Uploads the file to the appropriate root bucket on AWS S3, using the bucket name and filename passed in
     * as a key to access the file object. The s3client object is used to perform the upload.
     * @param fileName The name of the file to be stored.
     * @param file The file itself that is to be uploaded.
     * @param bucketName The name of the folder in which the file is found.
     * @author Jason
     */
    private void uploadFileToBucket(String fileName, File file, String bucketName) {
        // Add the object to the bucket with a public read permission
        s3client.putObject(new PutObjectRequest(rootBucketName, bucketName + "/" + fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

    /**
     * Deletes the file in AWS S3 using the fileURL as a key to identify where the object is stored in the bucket/
     * folder.
     * @param fileUrl The url used to identify the location of the file in the S3 bucket, containing the name of the
     * file.
     * @param folderName The folder that the file is stored in.
     * @param bucketName The root bucket/folder in which the file and folderName is stored in.
     * @return A message indicating the file has been deleted.
     * @author Jason
     */
    public String deleteFileFromS3Bucket(String fileUrl, String folderName, String bucketName) {
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        s3client.deleteObject(new DeleteObjectRequest(bucketName + "/" + folderName, fileName));
        return "The file has been deleted.";
    }

    /**
     * The rootBucketName (which is a root folder containing the learning material/assignment teacher questions
     * @param multipartFile The multipart file object being sent via multiple http requests from the relevant webpage.
     * @param folderName The name of the folder where the file is to be uploaded to.
     * @param filename The name of the file.
     * @param bucket The root bucket/folder in which the file is to be uploaded to.
     * @return A message indicating that the file has either uploaded successfully or failed.
     * @author Jason
     */
    public String uploadFile(MultipartFile multipartFile, String folderName, String filename, String bucket) {
        // This is the root bucket, which is either learning-material or assignments
        rootBucketName = bucket;
        try {
            File file = convertMultiPartToFile(multipartFile);
            uploadFileToBucket(filename, file, folderName);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
            return "There was an error uploading the file.";
        }
        return "The file was successfully uploaded.";
    }
}
