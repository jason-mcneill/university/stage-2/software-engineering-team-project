<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Learning Material - Add</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - ADD LEARNING MATERIAL</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Learning Content</h1>
<p>This material will become visible for students in <b>${userSession.Group.name}</b>.</p>
<form action="/createlearningmaterial" name="createlearningmaterial" enctype="multipart/form-data" method="POST">
    <label for="materialName">Lecture Material Title:</label>
    <input type="text" id="materialName" name="materialName"><br><br>

    <input type="file" accept=".pdf" id="file" name="file"><br><br>

    <label>Set Status</label><br>
    <input type="radio" id="live" name="status" value="Live" checked="checked">
    <label for="live">Live</label><br>
    <input type="radio" id="hidden" name="status" value="Hidden" >
    <label for="hidden">Hidden</label><br><br>

    <select id="topics" name="topics">
        <option value="" selected disabled>Select a topic</option>
        <c:forEach begin="0" end="${fn:length(userSession.Topics)}"
                   var="topic" varStatus="loop" items="${userSession.Topics}">
            <option value="${topic[0]}">${topic[1]}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit">
</form>
<p>${awsMessage}</p>

</body>
<script>
    $("form[name='createlearningmaterial']").validate({
        rules: {
            materialName: {
                required: true,
            },
            topics:{
                required: true,
            },
            file:{
                required: true,
            },
        },
        messages: {
            materialName: {
                required: "Please enter the Material Name.",
            },
            topics: {
                required: "Please select a Topic.",
            },
            file: {
                required: "Please select a File.",
            },
        }
    })
</script>
</html>