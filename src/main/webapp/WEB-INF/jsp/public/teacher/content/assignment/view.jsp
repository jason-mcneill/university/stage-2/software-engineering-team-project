<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${userSession.assignmentPDFData[3]} - ${userSession.Assignment.name}</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW ASSIGNMENT</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>${userSession.assignmentPDFData[3]}'s Submission</h1>
<h4 id="status">Status: Marked</h4>
<h4 id="completedate">Complete Date: ${userSession.assignmentPDFData[2]}</h4>
<iframe width="400px" height="600px" src="${userSession.assignmentPDFData[0]}"></iframe>
<div id="unmarked">
    <form action="/update-feedback" method="post">
        <h3>Your feedback</h3>
        <textarea id="feedback" name="feedback" rows="4" cols="50" placeholder="Add some feedback about the student's work."></textarea><br>
        <h3>Your mark / ${userSession.Assignment.markWeight}</h3>
        <input id="mark" name="mark" type="number" value="" min="0" max="${userSession.Assignment.markWeight}"><br><br>
        <button id="submitfeedback" name="submitfeedback" type="submit">Update</button>
    </form>
</div>

<p>${submissionMessage}</p>

<p><a href="/teacherviewassignments">Back to ${userSession.Group.name} Assignments</a></p>
</body>

<script>
    // Customize the page as to whether the teacher has edited the content or not
    var mark = String('${userSession.assignmentPDFData[1]}');
    // If there is no mark then the teacher needs to be prompted to mark it
    if (mark === "unmarked") {
        document.getElementById("status").innerHTML = "Status: Unmarked";
        document.getElementById("feedback").innerHTML = "";
        document.getElementById("submitfeedback").innerHTML = "Mark";
    } else {
        document.getElementById("mark").value = "${userSession.assignmentPDFData[1]}";
        document.getElementById("feedback").innerHTML = "${userSession.assignmentPDFData[4]}";
    }
</script>

</html>