<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Teacher.username} - Group Overview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW GROUP</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<h1>${userSession.Group.name} Members: </h1>
<ul>
    <c:forEach var="user" items="${userSession.usersInGroup}">
        <li>${user}</li>
    </c:forEach>
</ul>
<p><a href="/teacherviewlearningmaterial">Learning Material</a></p>
<p><a href="/teacherviewquizzes">Quizzes</a></p>
<p><a href="/teacherviewassignments">Assignments</a></p>
<p><a href="/teacherviewprogress">View Progress</a></p>
<br>
<div class="left-pane">
    <a class="btn-link" href="/teacheroverview">Return to overview</a>
</div>

<p>${viewGroupMessage}</p>
</body>
</html>