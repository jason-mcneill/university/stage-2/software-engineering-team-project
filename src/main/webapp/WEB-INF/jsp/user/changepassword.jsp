<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Password Change</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
<h1>Change Password</h1>
<form id="changePassword" action="/changeuserpassword" method="POST">
    <label for="oldPass">Previous Password:</label><br>
    <input type="text" id="oldPass" name="oldPass"><br>

    <label for="newPass">New Password:</label><br>
    <input type="text" id="newPass" name="newPass"><br><br>

    <input type="submit" value="Submit">
</form>
<p>${passwordMessage}</p>
<p><a href="/logout">Log Out</a></p>
</body>
</html>