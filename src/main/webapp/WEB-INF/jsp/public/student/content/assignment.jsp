<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Group.name} - Assignments</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - ASSIGNMENTS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>${userSession.Group.name} Assignments: </h1>
<h2>To do:</h2>
<p>${toDoMessage}</p>
<table>
    <tr>
        <th>Title</th>
        <th>Type</th>
        <th>Total Marks</th>
        <th>Due Date</th>
        <th>Topic</th>
    </tr>
    <ul>
        <c:forEach var="quiz" items="${userSession.assignmentsToDo}">
            <tr>
                <td>${quiz[0]}</td>
                <td>${quiz[1]}</td>
                <td>${quiz[2]}</td>
                <td>${quiz[3]}</td>
                <td>${quiz[6]}</td>
            </tr>
        </c:forEach>
    </ul>
</table>
<%--
can submit without choosing anything in select needs validation
--%>
<h2>Select a Assignment to complete below.</h2>
<form id="studentSelectAssignment" action="/studentcompleteassignment" method="POST">
    <select id="assignmentData" name="assignmentData">
        <option value="" selected disabled>Select Assignment</option>
        <c:forEach var="assignment" items="${userSession.assignmentsToDo}">
            <option value="${assignment}">${assignment[0]}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Begin">
</form>
<p>${assignmentMessage}</p>
<h2>Completed Assignments:</h2>
<table>
    <tr>
        <th>Title</th>
        <th>Type</th>
        <th>Total Marks</th>
        <th>Due Date</th>
        <th>Your Marks</th>
        <th>Percentage%</th>
        <th>Topic</th>
        <th>Submitted On</th>
    </tr>
    <c:forEach var="quiz" items="${userSession.assignmentsCompleted}">
        <tr>
            <td>${quiz[0]}</td>
            <td>${quiz[1]}</td>
            <td>${quiz[3]}</td>
            <td>${quiz[5]}</td>
            <td>${quiz[2]}</td>
            <td>${quiz[4]}</td>
            <td>${quiz[7]}</td>
            <td>${quiz[8]}</td>
        </tr>
    </c:forEach>
</table>
<p>${completedMessage}</p>
<p><a href="/studentviewgroup">Back to ${userSession.Group.name} Overview</a></p>
</body>
</html>