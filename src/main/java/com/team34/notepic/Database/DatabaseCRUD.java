package com.team34.notepic.Database;

import com.amazonaws.services.dynamodbv2.xspec.S;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.springframework.security.core.parameters.P;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import java.sql.*;
import java.util.*;

/** Provides a connection to the database with create, read, update and delete functionality.
 */
public class DatabaseCRUD {
    private static Session session;
    private static Connection connection;

    /**
     * Connects to the database via SSH, tunnels the SSH connection through a local port and forwards to a remote
     * port on the server
     * @author Ben
     */
    public static void connectViaSSH(){
        int lport=5656;
        String host="linux.cs.ncl.ac.uk";
        String rhost="cs-db.ncl.ac.uk";
        int rport=3306;

        // TODO Enter Newcastle University Credentials Here
        String SSHuser="b9023073"; //school username b12345
        String SSHpassword="MilkWhetherWent17052001"; //school password

        String dbuserName = "t2033t34";
        String dbpassword = "Pep:Mass5His";
        String url = "jdbc:mysql://localhost:"+lport+"/"+dbuserName;
        //String privateKeyPath = "C:/Users/Ben/.ssh/id_rsa";
        String driverName="com.mysql.cj.jdbc.Driver";
        try{
            //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            //jsch.addIdentity(privateKeyPath);
            session=jsch.getSession(SSHuser, host, 22);
            session.setPassword(SSHpassword);
            session.setConfig(config);
            session.connect();
            session.setPortForwardingL(lport, rhost, rport);
            Class.forName(driverName);
            connection = DriverManager.getConnection (url, dbuserName, dbpassword);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Closes the connection to the database.
     * @author Ben
     */
    private static void closeConnection() {
        try{
        connection.close();
        if(connection != null && !connection.isClosed()){
            //System.out.println("Closing Database Connection");
            connection.close();
        }
        if(session !=null && session.isConnected()){
            //System.out.println("Closing SSH Connection");
            session.disconnect();
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Creates a query to add new entries into the database, dynamically creating the condition with the number of '?'
     * matching the number of values.
     * @param tableName The name of the table the data is being added to.
     * @param values A hashmap of values being added to the table, representing column names and the value for the column.
     */
    public static void create(String tableName, Map<String,String> values) {
        try {
            //start of query string
            String query = "INSERT INTO " + tableName + " (";
            String qmarks = "";
            // Getting a Set of Key-value pairs
            Set entrySet = values.entrySet();
            // Obtaining an iterator for the entry set
            Iterator it = entrySet.iterator();
            //iterate through the inserted the key for each pair into the query, also create a string of '?' with the
            // same number as pairs
            while (it.hasNext()) {
                Map.Entry ME = (Map.Entry) it.next();
                query += ME.getKey() + ",";
                qmarks += "?,";
            }
            //remove last comma from both created strings
            query = removeLastChar(query);
            qmarks = removeLastChar(qmarks);
            //end the strings, bring together
            qmarks += ")";
            query += ") VALUES (" + qmarks;
            PreparedStatement statement = connection.prepareStatement(query);
            // Obtaining an iterator for the entry set
            Iterator iter = entrySet.iterator();
            int i = 1;
            while (iter.hasNext()) {
                Map.Entry ME = (Map.Entry) iter.next();
                statement.setString(i, (String) ME.getValue());
                i += 1;
            }
            // execute the query to the database
            statement.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Removes the last character of a string.
     * @param string The string which the last character will be removed from.
     * @return A substring of the string passed into the function, with the last character from the original string removed.
     */
        public static String removeLastChar(String string){
        // removes the last character of a string
        return string.substring(0, string.length() - 1);
    }

    /**
     * Reads the database using an SQL Query and returns the data as a HashMap, set up for repeated data so that the
     * hashmap does not lose repeated values, by adding them to the same array.
     *
     * @param tableName The table which is having the data read from.
     * @param tableColumns Used to provide a key for each value in the returned HashMap.
     * @param condition WHERE clause in constructed the SQL query.
     * @return Returns a HashMap of the results from the table queried.
     * @author Ben
     */
    public static HashMap<String, ArrayList<String>> read(String tableName, ArrayList<String> tableColumns, String condition) {
        try {
            // Create query. The condition can be left null if there is no WHERE clause needed
            String query = "SELECT * FROM " + tableName + condition;
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            HashMap<String, ArrayList<String>> results = new HashMap<>();
            //creates a multi value map
            ArrayList<String> values = new ArrayList<String>();
            while (rs.next()) {
                // Use the columns passed in as a key and attach the corresponding value
                for (String column : tableColumns) {
                    values.add(rs.getString(column));
                    results.put(column, values);
                }
            }
            return results;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * An alternative read function to receive data from the database as a two-dimensional array.
     *
     * @param tableName The table which is having the data read from.
     * @param tableColumns Used to provide a key for each value in the returned two-dimensional array.
     * @param condition WHERE clause in constructed the SQL query.
     * @return Returns a two-dimensional array of the results from the table queried.
     * @author Jason
     */
    public static String[][] readAlt(String tableName, ArrayList<String> tableColumns, String condition) {
        try {
            // Create query. The condition can be left null if there is no WHERE clause needed
            String query = "SELECT * FROM " + tableName + condition;
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = statement.executeQuery(query);
            // Determine the number of rows
            int numOfRows = 0;
            String[][] results = new String[0][tableColumns.size()];
            while (rs.next()) {
                numOfRows += 1;
                if (rs.isLast()) {
                    results = new String[numOfRows][tableColumns.size()];
                    rs.beforeFirst();
                    int rowIndex = 0;
                    // Generate the values
                    while (rs.next()) {
                        // Use the columns passed in as a key and attach the corresponding value
                        int columnIndex = 0;
                        for (String column : tableColumns) {
                            results[rowIndex][columnIndex] = rs.getString(column);
                            columnIndex += 1;
                        }
                        rowIndex += 1;
                    }
                }
            }
            return results;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Reads from two different tables from the database.
     *
     * @param tables The two tables being queried.
     * @param tablePrimaryForeignColumn The column which is the primary key in one table and the foreign key in another.
     * @param joinCondition The condition required to be met for values to join.
     * @param joinedTableColumns The resulting table after the join is performed.
     * @return Returns a HashMap of the results from the two tables queried.
     * @author Ian
     */
    public static HashMap<String, String> readJoined(ArrayList<String> tables, ArrayList<String>
            tablePrimaryForeignColumn, ArrayList<String> joinCondition,
                                                     ArrayList<String> joinedTableColumns) {
        try {
            String query = "SELECT * FROM " + tables.get(0);

            // Iterate, joining the first table (at the zeroth index) with the second table, then the third etc... using the primary key / foreign column name to match them.
            // An optional joinCondition can be used as a WHERE clause, with the first value at index 0 being used for the first two tables and so on
            for (int i = 1; i < tables.size(); i++) {
                query += " INNER JOIN " + tables.get(i) + " ON "
                        + tables.get(0) + "." + tablePrimaryForeignColumn.get(0) + " = " + tables.get(i) + "." + tablePrimaryForeignColumn.get(i) + joinCondition.get(i - 1);
            }
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            // Map the values to a corresponding column (found in the
            HashMap<String, String> results = new HashMap<>();
            if (resultSet.next()) {
                for (String column : joinedTableColumns) {
                    results.put(column, resultSet.getString(column));
                }
            }
            return results;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Constructs an update SQL query to update the a value in the database.
     *
     * @param tableName The table which contains the value to be updated.
     * @param columnName The column to be updated.
     * @param value The value to be changed to the database.
     * @param condition WHERE clause in constructed the SQL query.
     * @author Ian
     */
    public static void update(String tableName, String columnName, String value, String condition)  {
        try {
            //String query = "UPDATE " + tableName + " SET " + columnName + " = " + value + " WHERE " + condition;
            String query = "UPDATE " + tableName + " SET " + columnName + " = '" + value + "' WHERE " + condition;
            PreparedStatement statement = connection.prepareStatement(query);
            // execute the query to the database
            statement.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Deletes a field from a class.
     *
     * @param tableName The table which contains the value to be deleted
     * @param condition The value(s) being deleted from the given table that fulfil the given condition.
     * @author Jason
     */
    public static void delete(String tableName, String condition)  {
        try {
            String query = "DELETE FROM " + tableName + condition;
            PreparedStatement statement = connection.prepareStatement(query);
            // execute the query to the database
            statement.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}