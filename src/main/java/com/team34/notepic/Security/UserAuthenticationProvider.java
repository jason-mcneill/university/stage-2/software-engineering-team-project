package com.team34.notepic.Security;

import com.team34.notepic.Database.DatabaseCRUD;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * compares users credentials to see if they match a user in the database, if so they are granted an appropriate
 * authority (role) and redirected to their overview, but if the credentials are incorrect and do not match a user,
 * they are redirected back to the login page
 * @author Jason
 */
@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        ArrayList<GrantedAuthority> grantedAuthorityList = new ArrayList<>();

        // Acquire the user information if they exist
        HashMap<String, ArrayList<String>> userDetails = new HashMap<>();
        try {
            userDetails = acquireUser(username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BadCredentialsException("There was an error connecting to the database.");
        }
        
        if (userDetails.size() != 0) {
            switch(userDetails.get("UserRole").get(2)) {
                case "Admin":
                    grantedAuthorityList.add(new SimpleGrantedAuthority("ADMIN"));
                    break;
                case "Teacher":
                    grantedAuthorityList.add(new SimpleGrantedAuthority("TEACHER"));
                    break;
                case "Student":
                    grantedAuthorityList.add(new SimpleGrantedAuthority("STUDENT"));
                    break;
            }
            return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorityList);
        } else {
            throw new BadCredentialsException("Sorry, but the details you provided were incorrect.");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public HashMap<String, ArrayList<String>> acquireUser(String username, String password) throws SQLException {
        String tableName = "Users";
        ArrayList<String> joinedTableColumns = new ArrayList<>();
        joinedTableColumns.add("Username"); joinedTableColumns.add("Password"); joinedTableColumns.add("UserRole");
        joinedTableColumns.add("Firstname"); joinedTableColumns.add("Lastname"); joinedTableColumns.add("DOB");
        joinedTableColumns.add("Email");
        String condition = (" WHERE Users.Username = " + "'" + username + "'" + " AND Users.Password = " + "'" + password + "'");
        return DatabaseCRUD.read(tableName, joinedTableColumns, condition);
    }

}
