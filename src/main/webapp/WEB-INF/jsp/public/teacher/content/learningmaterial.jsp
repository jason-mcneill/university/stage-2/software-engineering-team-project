<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Group.name} - Lecture Material</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW LEARNING MATERIAL</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<p><a href="/teacheraddlearningmaterial">Add learning material</a></p>
<p><a href="/teacheramendlearningmaterial">Amend learning material</a></p>
<p><a href="/teacherdeletelearningmaterial">Delete learning material</a></p>

<h2>Learning Material</h2>
<p>Select learning material, either live or hidden, to view</p>
<form id="selectLearningMaterial" name="selectLearningMaterial" action="/selectLearningMaterial" enctype="multipart/form-data" method="POST">
    <select id="learningMaterial" name="learningMaterial">
        <option value="" selected disabled>${userSession.Group.name} Learning Material</option>
        <c:forEach begin="0" end="${fn:length(userSession.Topics)}"
                   var="topic" varStatus="loop" items="${userSession.Topics}">
            <c:if test="${topic[2] == userSession.Group.groupID}">
                <optgroup label="${topic[1]}">
            </c:if>
                <c:forEach begin="0" end="${fn:length(userSession.learningMaterial)}"
                           var="learningMaterial" varStatus="loop" items="${userSession.learningMaterial}">
                    <c:if test="${topic[0] == learningMaterial[6]}">
                        <option value="${loop.index}">${learningMaterial[3]}</option>
                    </c:if>
                </c:forEach>
            </optgroup>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Select">
</form>
<p>Content status: ${userSession.selectedLearningMaterial[5]}</p>
<div class="left-pane">
    <iframe width="400px" height="600px" src="${userSession.selectedLearningMaterial[2]}"></iframe>
</div>
<h2>Topics</h2>
<p><a href="/teacher-topic-overview">View your topics</a></p>
</body>

<script>
    $("form[name='selectLearningMaterial']").validate({
        rules: {
            learningMaterial: {
                required: true,
            },
        },
        messages: {
            learningMaterial: {
                required: "Please enter the Material Name.",
            },
        }
    })
</script>
</html>