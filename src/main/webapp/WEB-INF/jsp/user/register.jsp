<!DOCTYPE HTML>
<html>
<head>
    <title>Admin - Register</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>

<h1>Register a new school administrator account:</h1>
<form id="registerAdmin" action="/registerUser" method="POST">
    <i>Your username will be automatically generated.</i><br><br>
    <label for="password">Password:</label><br>
    <input type="text" id="password" name="password"><br>

    <label for="firstName">First Name:</label><br>
    <input type="text" id="firstName" name="firstName"><br>

    <label for="lastName">Last Name:</label><br>
    <input type="text" id="lastName" name="lastName"><br>

    <label for="dateOfBirth">Date of Birth:</label><br>
    <input type="text" id="dateOfBirth" name="dateOfBirth"><br>

    <label for="email">Email:</label><br>
    <input type="text" id="email" name="email"><br>

    <label for="nameOfSchool">Name of School:</label><br>
    <input type="text" id="nameOfSchool" name="nameOfSchool"><br>
    <input type="hidden" id="userRole" name="userRole" value="Admin">
    <br><input type="submit" value="Submit">
</form>

</body>

</html>