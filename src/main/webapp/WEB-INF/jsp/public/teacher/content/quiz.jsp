<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Group.name} - Quiz</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW QUIZZES</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<body>
<h1>${userSession.Group.name} Quizzes: </h1>
<p>${quizMessage}</p>
<table>
    <tr>
        <th>ID</th>
        <th>Status</th>
        <th>Edit</th>
        <th>Results</th>
    </tr>
    <c:forEach var="quiz" items="${userSession.quizzesInGroup}">
        <tr>
            <td>${quiz[0]}</td>
            <td>${quiz[2]}</td>
            <form action="/teacheramendquiz" method="post">
                <input type="hidden" id="quizData" name="quizData" value="${quiz[1]}">
                <td>
                    <button type="submit">Edit</button>
                </td>
            </form>
            <form action="/teacherviewquizresults" method="post">
                <input type="hidden" id="quizResults" name="quizResults" value="${quiz[1]}">
                <td>
                    <button type="submit">Results</button>
                </td>
            </form>
        </tr>
    </c:forEach>
</table>
<p><a href="/teachercreatequiz">Add a new quiz</a></p>
<p><a href="/teacherviewgroup">Back to ${userSession.Group.name} Overview</a></p>
</body>
</html>