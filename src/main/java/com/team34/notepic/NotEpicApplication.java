package com.team34.notepic;

import com.team34.notepic.Database.DatabaseCRUD;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;

import javax.servlet.annotation.WebServlet;
import java.sql.*;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Controller
@WebServlet("/NotEpicApplication")
public class NotEpicApplication {

    public static void main(String[] args) throws SQLException {
        SpringApplication.run(NotEpicApplication.class, args);
        DatabaseCRUD.connectViaSSH();
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasename("messages");
        return source;
    }

}