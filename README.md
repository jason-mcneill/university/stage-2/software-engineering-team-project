About
-
This web software is a learning and education tool that aims to allow staff and students to learn in an online environment. It allows students to track their progress and for staff to view the progress of their own students. Staff can provide learning material and upload quizzes and assignments for their students to complete and for staff to be able to then provide a mark.

Usage
-
To begin, enter University Credentials in the DatabaseCRUD class (lines 31/32).

This software requires an administrator to create an account and can then create student and teacher accounts. 

To register an administration account, click on the "Admin Registration" button and fill in the form, providing a secure password, first name, last name date of birth, email address and the name of your school.
Then you will be able to access your administrator account where you can add school users from either a CSV or manually.

To add school user's data from a CSV file:
* Log into your administrator account with your automatically generated username and your password.
* Click on the "Import School Users Data from CSV" text.
* Click on the "Choose file" button to upload the CSV file you wish to use.
* Click on the "Import" button to import the CSV file and then generate each account will be generated.

To manually add a school user:
* Log into your administrator account with your automatically generated username and your password.
* Click on the "Create New Teacher or Student Account" text.
* Complete the form on screen, entering a temporary password for this student or teacher, selecting their user role, first name, last name, date of birth and email address.
* Click on the "Submit" button to add this user to the system.

Students and teachers can then access the system from the web application using the login provided by the administrators and are encouraged to then change their password on first login. To do this, once logged in, click on the "Change Password" text near the bottom of the page and enter your admin assigned password in the "Previous Password" box, and your new password in the "New Password" box, then click on submit to confirm this change.

More information on the usage of this software for teachers and students can be found in the user manual.

License
-
This project has been developed by Team 34 Times the Charm (Alex Jackson, Ben Keenan, Callum Laking, Ian Givero and Jason McNeill), Copyright, 2021.