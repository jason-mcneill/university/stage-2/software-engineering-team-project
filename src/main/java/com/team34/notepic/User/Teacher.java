package com.team34.notepic.User;

import com.team34.notepic.Content.*;
import com.team34.notepic.AWS.AwsCloud;
import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.General.General;
import com.team34.notepic.Group;
import com.team34.notepic.Security.UserSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("userSession")
@RequestMapping(method={RequestMethod.POST,RequestMethod.GET})
public class Teacher extends User {

    private String title;
    private String teacherRole;

    public void setTitle(String title) {
        this.title = title;
    };
    public String getTitle() {return this.title;};

    public void setTeacherRole(String teacherRole) {
        this.teacherRole = teacherRole;
    };
    public String getTeacherRole() {return this.teacherRole;};

    /**
     * Adds a Teacher to the Teacher table in the database.
     * @throws SQLException Thrown when DatabaseCRUD.create has an invalid SQL Query.
     */
    public void addToDatabase() throws SQLException {
        super.addToDatabase();
        Map<String,String> values = new HashMap<String, String>();
        values.put("Username",this.getUsername());
        values.put("Title",this.getTitle());
        values.put("TeacherRole",this.getTeacherRole());
        DatabaseCRUD.create("Teacher", values);
    }

    /**
     * Reads learning material from the database and adds it to the user session
     * @param userSession linked hash map
     */
    public void createTempLearningMaterialSession(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        LearningMaterial learningMaterial = new LearningMaterial();
        Group group = (Group) userSession.get("Group");
        learningMaterial.setGroupID(group.getGroupID());
        userSession.put("learningMaterial", learningMaterial.readFromDatabase(null));
    }

    /**
     * Generates an upload link to allow a file to be uploaded to AWS
     * @param file A multipart file object
     * @param model A model object which is given attributes
     * @param userSession A linked hashmap
     * @param bucket A string
     * @return A URL where the content is uploaded to
     */
    public String uploadContent(@RequestPart(value = "file") MultipartFile file, Model model,
                                @ModelAttribute("userSession") HashMap<String, Object> userSession,
                                String bucket) {
        Group group = (Group) userSession.get("Group");
        Teacher teacher = (Teacher) userSession.get("Teacher");
        // Create the filename and bucket location
        String filename = (teacher.username + "-" + new Date().getTime() + ".pdf");
        String folder = (group.getGroupID());
        // Upload the pdf file to AWS S3
        AwsCloud awsCloud = new AwsCloud();
        model.addAttribute("awsMessage", awsCloud.uploadFile(file, folder, filename, bucket));
        return "https://" + bucket + ".s3.eu-west-2.amazonaws.com" + "/" + folder + "/" + filename;
    }

    /**
     * Deletes content from AWS
     * @param index String value
     * @param userSession Linked hashmap
     * @param attributes RedirectAttributes object to show a delete message to the user
     * @param tempLearningMaterial String identifying the content to be deleted
     */
    public void deleteContent(@RequestPart(value = "learningMaterial") String index,
                              @ModelAttribute("userSession") HashMap<String, Object> userSession, RedirectAttributes attributes,
                              String[][] tempLearningMaterial) {
        // Acquire the group id for finding the right folder to search
        Group group = (Group) userSession.get("Group");
        String folder = group.getGroupID();
        // Delete the appropriate file from AWS
        AwsCloud awsCloud = new AwsCloud();
        attributes.addFlashAttribute("deleteMessage", awsCloud.deleteFile(tempLearningMaterial[Integer.parseInt(index)][2], folder, "lecture-material"));
    }

    /**
     * Allows a teacher to add learning material to the database
     * @param file The file being uploaded to AWS
     * @param model Model object
     * @param userSession a linked hashmap
     * @return Page the teacher will be redirected to
     */
    @PostMapping("/createlearningmaterial")
    public ModelAndView addLearningMaterial(@RequestPart(value = "file") MultipartFile file,
                                            @RequestParam Map<String, String> parameters, Model model,
                                            @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        Group group = (Group) userSession.get("Group");
        // Upload the content to aws
        String filePath = uploadContent(file, model, userSession, "lecture-material");
        // Add relevant information to the database
        LearningMaterial learningMaterial = new LearningMaterial();
        learningMaterial.setName(parameters.get("materialName"));
        learningMaterial.setGroupID(group.getGroupID());
        learningMaterial.setDateCreated(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
        learningMaterial.setPdfLink(filePath);
        learningMaterial.setStatus(parameters.get("status"));
        learningMaterial.setTopicID(parameters.get("topics"));
        learningMaterial.addToDatabase();
        return new ModelAndView("/public/teacher/content/learningmaterial/add");
    }

    @GetMapping("/teacherdeletelearningmaterial")
    public String teacherDeleteLearningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        createTempLearningMaterialSession(userSession);
        return "/public/teacher/content/learningmaterial/delete";
    }

    /**
     * Deletes learning material from the database.
     * @param index Used to find object to delete
     * @param attributes RedirectAttributes object
     * @param userSession a linked hashmap
     * @return String of redirect location
     */
    @PostMapping("/deletelearningmaterial")
    public ModelAndView deleteLearningMaterial(@RequestPart(value = "learningMaterial") String index, RedirectAttributes attributes,
                                               @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        if(index != null) {
            // Acquire the 2d array of lecture material
            String[][] tempLearningMaterial = (String[][]) userSession.get("learningMaterial");
            // Delete the file from aws
            deleteContent(index, userSession, attributes, tempLearningMaterial);
            // Delete the appropriate lecture material from the database
            LearningMaterial learningMaterial = new LearningMaterial();
            learningMaterial.setLearningMaterialID(tempLearningMaterial[Integer.parseInt(index)][0]);
            learningMaterial.deleteFromDatabase();
            // Clear the learning material from the session
            userSession.remove("learningMaterial");
        }else{
            attributes.addFlashAttribute("deleteMessage", "Please select a piece of Learning Material.");
        }
        return new ModelAndView("redirect:/teacherdeletelearningmaterial");
    }

    @GetMapping("/teacheramendlearningmaterial")
    public String teacherAmendLearningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        createTempLearningMaterialSession(userSession);
        return "/public/teacher/content/learningmaterial/amend";
    }

    /**
     * Deletes the files from AWS and replaces it
     * @param index index of the learning material
     * @param attributes a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @param file file to replace
     * @param parameters choice of file name entered
     * @return mapping
     */
    @PostMapping("/updatelearningmaterial")
    public ModelAndView updateLearningMaterial(@RequestPart(value = "learningMaterial") String index, RedirectAttributes attributes,
                                               @ModelAttribute("userSession") HashMap<String, Object> userSession,
                                               @RequestPart(value = "file") MultipartFile file, @RequestParam Map<String, String> parameters) {
    if(!parameters.get("materialName").equals("") && parameters.get("status") != null && parameters.get("topics") != null && file != null && parameters.get("learningMaterial") != null) {
        // Make changes to AWS
        Group group = (Group) userSession.get("Group");
        Teacher teacher = (Teacher) userSession.get("Teacher");
        String[][] tempLearningMaterial = (String[][]) userSession.get("learningMaterial");
        // Create the filename and bucket location
        String filename = (teacher.username + "-" + new Date().getTime() + ".pdf");
        String folder = (group.getGroupID());
        String bucket = "lecture-material";
        AwsCloud awsCloud = new AwsCloud();
        // Delete the previous pdf file
        awsCloud.deleteFile(tempLearningMaterial[Integer.parseInt(index)][2], folder, "lecture-material");
        // Upload the new pdf file
        awsCloud.uploadFile(file, folder, filename, bucket);
        // Upload the changes to the database
        LearningMaterial learningMaterial = new LearningMaterial();
        learningMaterial.setLearningMaterialID(tempLearningMaterial[Integer.parseInt(index)][0]);
        learningMaterial.setName(parameters.get("materialName"));
        learningMaterial.setPdfLink("https://lecture-material.s3.eu-west-2.amazonaws.com/" + folder + "/" + filename);
        learningMaterial.setStatus(parameters.get("status"));
        learningMaterial.setTopicID(parameters.get("topics"));
        learningMaterial.updateDatabase();
        attributes.addFlashAttribute("updateMessage", parameters.get("materialName") + " was successfully updated.");
        // Clear the learning material from the session
        userSession.remove("learningMaterial");

    }else {
        attributes.addFlashAttribute("updateMessage", "Please add all data to create Learning Material");
    }
        return new ModelAndView("redirect:/teacheramendlearningmaterial");
    }


    @PostMapping("/teacherviewgroup")
    public String teacherViewGroup(@RequestParam Map<String, String> parameters, Model model,
                                   @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        if(parameters.get("groupName") != null) {
            Group.getGroupDataFromName(parameters.get("groupName"), model, userSession);
            return "/public/teacher/group";
        }else{
            model.addAttribute("teacherOverviewMessage" , "Please select a group.");
            return "public/teacher/index";
        }
    }

    //Use the group instance from here as is quicker than reading from database.

    @GetMapping("/teacherviewlearningmaterial")
    public String teacherViewLearningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        // Add the topics to the session
        UserSession.addTopicsToSession(userSession);
        createTempLearningMaterialSession(userSession);
        return "/public/teacher/content/learningmaterial";
    }

    @GetMapping("/selectLearningMaterial")
    public ModelAndView selectLearningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession,
                                               @RequestPart(value = "learningMaterial") String index) {
        String[][] selectedLearningMaterial = (String[][]) userSession.get("learningMaterial");
        userSession.put("selectedLearningMaterial", selectedLearningMaterial[Integer.parseInt(index)]);
        // Check if the user is a student or teacher
        if (userSession.get("Student") == null) {
            return new ModelAndView("redirect:/teacherviewlearningmaterial");
        } else {
            return new ModelAndView("redirect:/studentlearningmaterial");
        }
    }

    public static ArrayList<String> getStudentsInGroup(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        ArrayList<String> allUsers = (ArrayList<String>) userSession.get("usersInGroup");
        return getStudentsFromAllUsersInGroup(allUsers);
    }

    @GetMapping("/teacherviewquizzes")
    public String teacherViewQuizzes(Model model,
                                     @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        try {
            Quiz quiz = (Quiz) userSession.get("Quiz");
            if (quiz.getName().equals("temp")) {
                // this means when the user returns to the quizzes overview without changing a blank quiz when they create
                // one, the quiz will be deleted from the database otherwise the temp value will not be unique
                quiz.updateClassFromDatabase();
                quiz.removeFromDatabase();
            }
            userSession.remove("Question");
            userSession.remove("quizQuestions");
            userSession.remove("resultData");
            userSession.remove("Quiz");
            userSession.remove("Assignment");
            userSession.put("isAssignment", "0");
            Assignment.updateAssignmentsArrays(model, userSession);

            //this will remove the temp quiz added to the database and any questions, also removes session objects
            //if there is any
        } catch (Exception e) {
            // this will catch most times, Im not printing stack trace to keep system out clean.
        }
        Quiz.updateQuizList(model, userSession);
        return "/public/teacher/content/quiz";
    }

    /**
     * Creates assignment of either type quiz or PDF
     * @param parameters values from jsp form
     * @param model  a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @param file if type = pdf
     * @return mapping
     */
    @GetMapping("/teachercreateassignment")
    public String teacherCreateAssignment(@RequestParam Map<String, String> parameters, Model model,
                                          @ModelAttribute("userSession") HashMap<String, Object> userSession,
                                          @RequestPart(value = "file") MultipartFile file) {
        try {
            if (parameters.get("name").isEmpty() || parameters.get("name").equals("temp") || parameters.get("topicID").equals("")) {
                model.addAttribute("createAssignmentMessage", "Name cannot be blank or equal 'temp'");
                return "/public/teacher/content/assignment/add";
            } else {
                //create temp here so that we can get ID if the name added by teacher is a duplicate
                Group group = (Group) userSession.get("Group");
                Assignment assignment = new Assignment();
                assignment.setGroupID(group.getGroupID());
                assignment.setName("temp");
                assignment.createTempInDatabase();
                assignment.getIDFromName();
                //update from here
                String assignmentType = parameters.get("assignmentType");
                assignment.setAssignmentType(assignmentType);
                assignment.setName(parameters.get("name"));
                assignment.setDueDateTime(parameters.get("dueDateTime"));
                assignment.setStatus(parameters.get("status"));
                assignment.setTopicID(parameters.get("topicID"));
                if (assignmentType.equals("PDF")) {
                    assignment.setQuestionPDFLink(uploadContent(file, model, userSession, "assignment-questions"));
                    assignment.setMarkWeight(parameters.get("marks"));
                    assignment.updateDatabase();
                    Assignment.updateAssignmentsArrays(model, userSession);
                    return "/public/teacher/content/assignment";
                } else {
                    assignment.setQuestionPDFLink(null);
                    assignment.setMarkWeight("0");
                    assignment.updateDatabase();
                    userSession.put("isAssignment", "1");
                    userSession.put("Assignment", assignment);
                    return teacherCreateQuiz(model, userSession);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
            return "/public/teacher/content/assignment";
        }

    }

    @GetMapping("/teachercreatequiz")
    public String teacherCreateQuiz(Model model,
                                    @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        try {
            Group group = (Group) userSession.get("Group");
            if (userSession.get("Quiz") != null) {
                //if the user refreshes the page, the quiz will not be removed, and a new quiz will be created
                //this means that the quiz will be deleted
                Quiz quiz = (Quiz) userSession.get("Quiz");
                if (quiz.getName().equals("temp")) {
                    quiz.removeFromDatabase();
                }
                userSession.remove("Quiz");
            }
            Quiz quiz = new Quiz();
            quiz.setQuizID(null);
            quiz.setGroupID(group.getGroupID());
            quiz.setTotalScore("0");
            quiz.setName("temp");
            quiz.setDescription(null);
            quiz.setStatus("Live");
            String isAssignment = (String) userSession.get("isAssignment");
            if (isAssignment == null){
                quiz.setIsAssignment("0");
            }else {
                quiz.setIsAssignment("1");
            }
            quiz.addToDatabase();
            quiz.getIDFromDatabase();

            if (quiz.getIsAssignment().equals("1")){
                Assignment assignment = (Assignment) userSession.get("Assignment");
                quiz.setName(assignment.getName());
                quiz.setStatus(assignment.getStatus());
                quiz.setAssignmentID(assignment.getAssignmentID());
                quiz.setAsAssignment(assignment.getAssignmentID());
                quiz.updateDatabase();
                userSession.put("Quiz", quiz);
            }else{
                quiz.updateDatabase();
                userSession.put("Quiz", quiz);
            }
            return "/public/teacher/content/quiz/add";
        } catch (Exception e) {
            e.printStackTrace();
            return "/public/teacher/content/quiz/add";
        }

    }

    /**
     * Allows the teacher to view an overview of their information, clearing any unnecesary data from the session
     * @param userSession A linked hashmap
     * @param model A model object
     * @return Redirects teacher to a general overview page
     */
    @GetMapping("/teacheroverview")
    public ModelAndView viewOverview(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getName();
        if (!UserSession.doesSessionExist(userSession)) {
            UserSession.createUserSession("Teacher", authentication.getName(), userSession, model);
        }
        // Clear the learning material
        userSession.remove("selectedLearningMaterial");
        //Clear the Quiz
        if (userSession.get("Quiz") != null) {
            //if the user refreshes the page, the quiz will not be removed, and a new quiz will be created
            //this means that the quiz will be deleted
            Quiz quiz = (Quiz) userSession.get("Quiz");
            if (quiz.getName().equals("temp")) {
                quiz.removeFromDatabase();
            }
            userSession.remove("Quiz");
        }
        return new ModelAndView("/public/teacher/index");
    }

    @GetMapping("/teacherdeletequestion")
    public String teacherDeleteQuestion(@RequestParam Map<String, String> parameters, Model model,
                                         @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Quiz quiz = (Quiz) userSession.get("Quiz");
        Question question = new Question();
        question.setQuestionID(parameters.get("questionToDelete"));
        int totalScore = Integer.parseInt(quiz.getTotalScore());
        totalScore -= Integer.parseInt(question.getMarkWeightFromID());
        quiz.setTotalScore(Integer.toString(totalScore));
        quiz.removeQuestionID(parameters.get("questionToDelete"));
        question.removeFromDatabase();
        Quiz.updateQuizQuestionData(quiz, model, userSession);
        quiz.updateDatabase();
        userSession.put("Quiz",quiz);
        model.addAttribute("deleteMessage", "Question Deleted!");
        return "/public/teacher/content/quiz/amend";
    }

    @GetMapping("/teacheraddquizquestion")
    public String teacherAddQuizQuestion(@RequestParam Map<String, String> parameters, Model model,
                                         @ModelAttribute("userSession") HashMap<String, Object> userSession){
        addQuestion(parameters, model, userSession);
        return "/public/teacher/content/quiz/amend";
    }

    public void addQuestion(Map<String, String> parameters, Model model,
                            @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            // to allow repeated names, need to make a temporary question, then get the ID from the temporary field.
            if (parameters.get("question").equals("") || parameters.get("question").equals("temp") || parameters.get("choice1").equals("") ||parameters.get("choice2").equals("") || parameters.get("correctChoice").equals("") || parameters.get("marksWorth").equals("")) {
                model.addAttribute("questionMessage", "Not enough question data is entered, note: Question cannot be 'temp' and you must enter at least 2 choices.");
            } else {
                Question question = new Question();
                Quiz quiz = (Quiz) userSession.get("Quiz");
                question.setQuizID(quiz.getQuizID());
                question.createTemporary();
                question.setName(parameters.get("question"));
                question.setChoice1(parameters.get("choice1"));
                question.setChoice2(parameters.get("choice2"));
                question.setChoice3(parameters.get("choice3"));
                question.setChoice4(parameters.get("choice4"));
                question.setCorrectIndex(parameters.get("correctChoice"));
                question.setMarkWeight(parameters.get("marksWorth"));
                question.updateDatabase();
                quiz.addQuestionID(question.getQuestionID());
                int totalScore = Integer.parseInt(quiz.getTotalScore());
                totalScore += Integer.parseInt(question.getMarkWeight());
                quiz.setTotalScore(Integer.toString(totalScore));
                quiz.updateDatabase();
                if (quiz.getIsAssignment().equals("1")) {
                    Assignment assignment = (Assignment) userSession.get("Assignment");
                    assignment.setMarkWeight(quiz.getTotalScore());
                    assignment.updateTotalScore();
                }
                userSession.put("Quiz", quiz);
                Quiz.updateQuizQuestionData(quiz, model, userSession);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @PostMapping("/teacheraddquizdata")
    public String teacherAddQuizData(@RequestParam Map<String, String> parameters, Model model,
                                 @ModelAttribute("userSession") HashMap<String, Object> userSession){
        //code to add/create quiz
        try {
            if(parameters.get("displayName").isEmpty() || parameters.get("displayName").equals("temp") ){
                model.addAttribute("quizMessage", "Name cannot be blank or equal 'temp'.");
            }else {
                Quiz quiz = (Quiz) userSession.get("Quiz");
                quiz.setName(parameters.get("displayName"));
                quiz.setDescription(parameters.get("description"));
                quiz.setStatus(parameters.get("status"));
                quiz.updateDatabase();
                userSession.put("Quiz", quiz);
            }
        }
        catch(Exception e) {
            model.addAttribute("quizMessage","Name and Description can not be blank.");
            e.printStackTrace();
        }
        return "/public/teacher/content/quiz/add";
    }

    @GetMapping("/teacheramendquiz")
    public String teacherAmendQuiz(@RequestParam Map<String, String> parameters, Model model,
                                   @ModelAttribute("userSession") HashMap<String, Object> userSession){
        String quizID = parameters.get("quizData");
        Quiz.setQuizData(quizID, model, userSession);
        return "/public/teacher/content/quiz/amend";
    }

    @GetMapping("/teacherdeletequiz")
    public String teacherDeleteQuiz(@ModelAttribute("userSession") HashMap<String, Object> userSession, Model model){
        try {
            Quiz quiz = (Quiz) userSession.get("Quiz");
            quiz.removeFromDatabase();
            userSession.remove("Quiz");
            ArrayList<ArrayList<String>> questions = (ArrayList<ArrayList<String>>)userSession.get("quizzesInGroup");
            ArrayList<String> quizQuestionData = new ArrayList<>();
            quizQuestionData.add(quiz.getName());
            quizQuestionData.add(quiz.getQuizID());
            quizQuestionData.add(quiz.getStatus());
            questions.remove(quizQuestionData);
            userSession.put("quizzesInGroup",questions);
            Quiz.updateQuizList(model,userSession);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "/public/teacher/content/quiz";
    }

    @GetMapping("/teacherupdatequizname")
    public String teacherUpdateQuizName(@RequestParam Map<String, String> parameters, Model model,
                                        @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(!parameters.get("quizName").equals("")) {
                Quiz quiz = (Quiz) userSession.get("Quiz");
                quiz.setName(parameters.get("quizName"));
                quiz.updateDatabase();
                quiz.updateClassFromDatabase();
                userSession.put("Quiz", quiz);
            }else{
                model.addAttribute("nameMessage", "Please enter a name.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "/public/teacher/content/quiz/amend";
    }

    @GetMapping("/teacherupdatequizdescription")
    public String teacherUpdateQuizDescription(@RequestParam Map<String, String> parameters, Model model,
                                               @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(!parameters.get("quizDescription").equals("")) {
            Quiz quiz = (Quiz) userSession.get("Quiz");
            quiz.setDescription(parameters.get("quizDescription"));
            quiz.updateDatabase();
            quiz.updateClassFromDatabase();
            userSession.put("Quiz", quiz);
        }else{
                model.addAttribute("descMessage", "Please enter a description.");
            }}
        catch(Exception e){
            e.printStackTrace();
        }
        return "/public/teacher/content/quiz/amend";
    }

    @GetMapping("/teacherupdatequizstatus")
    public String teacherUpdateQuizStatus(@RequestParam Map<String, String> parameters, Model model,
                                               @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(parameters.get("quizStatus")!= null) {
            Quiz quiz = (Quiz) userSession.get("Quiz");
            quiz.setStatus(parameters.get("quizStatus"));
            quiz.updateDatabase();
            quiz.updateClassFromDatabase();
            userSession.put("Quiz", quiz);
        }else{
                model.addAttribute("statusMessage", "Please enter a status.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "/public/teacher/content/quiz/amend";
    }

    @GetMapping("/teacherviewquizresults")
    public String teacherViewQuizResults(@RequestParam Map<String, String> parameters, Model model,
                                   @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            String quizID = parameters.get("quizResults");
            Quiz quiz = new Quiz();
            quiz.setQuizID(quizID);
            setQuizResultsArray(model, userSession, quiz);
        } catch(Exception e){
            e.printStackTrace();
        }
        return "/public/teacher/content/quiz/results";
    }

    @PostMapping("/view-submission")
    public ModelAndView viewSubmission(@ModelAttribute("userSession") HashMap<String, Object> userSession,
                                       @RequestParam Map<String, String> parameters) throws SQLException {
        String[] pdfData = (String[]) userSession.get("assignmentPDFData");
        // Acquire the username either from the parameter passed or from the session
        String username;
        // Always prioritise the parameter value in case the teacher has selected another student
        if (parameters.get("username") != null) {
            username = parameters.get("username");
        } else {
            username = pdfData[3];
        }
        Assignment assignment = (Assignment) userSession.get("Assignment");
        String[] assignmentPDFData = assignment.getPDFSubmission(username);
        // Put the pdf data in the session
        userSession.put("assignmentPDFData", assignmentPDFData);
        return new ModelAndView("/public/teacher/content/assignment/view");
    }

    @PostMapping("/update-feedback")
    public ModelAndView updateFeedback(@ModelAttribute("userSession") HashMap<String, Object> userSession, RedirectAttributes attributes,
                                       @RequestParam Map<String, String> parameters) throws SQLException {
        System.out.println(parameters.values());
        System.out.println("mark: " + parameters.get("mark"));
        System.out.println("feedback: " + parameters.get("feedback"));
        String[] assignmentPDFData = (String[]) userSession.get("assignmentPDFData");
        System.out.println(Arrays.toString(assignmentPDFData));
        Assignment assignment = (Assignment) userSession.get("Assignment");
        if(!parameters.get("mark").equals("") && parameters.get("feedback").equals("")) {
            assignment.updatePDFSubmission(assignmentPDFData[3], parameters.get("mark"), assignmentPDFData[4]);
            attributes.addFlashAttribute("submissionMessage", "Details about the submission have been amended.");
        }if(parameters.get("mark").equals("") && !parameters.get("feedback").equals("")) {
            assignment.updatePDFSubmission(assignmentPDFData[3], assignmentPDFData[1], parameters.get("feedback"));
        }if(!parameters.get("mark").equals("") && !parameters.get("feedback").equals("")) {
            assignment.updatePDFSubmission(assignmentPDFData[3], parameters.get("mark"), parameters.get("feedback"));
        }
        if(parameters.get("mark").equals("") && parameters.get("feedback").equals("")) {
            attributes.addFlashAttribute("submissionMessage", "Please enter a ");
        }
        return new ModelAndView("redirect:/view-submission");

    }

    @GetMapping("/teacherviewassignmentresults")
    public String teacherViewAssignmentResults(@RequestParam Map<String, String> parameters, Model model,
                                         @ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {

        Assignment assignment = new Assignment();
        if(parameters.get("assignmentDataResults") != null){
            String assignmentDataString = parameters.get("assignmentDataResults");
            System.out.println("ADS: " + assignmentDataString);
            String[] assignmentData = General.getArrayFromDataString(assignmentDataString);
            assignment.setAssignmentID(assignmentData[1]);
            assignment.updateClassFromDatabase();
        }else{
            assignment = (Assignment) userSession.get("Assignment");
        }
        ArrayList<String> allUsers = (ArrayList<String>)userSession.get("usersInGroup");
        ArrayList<String> studentsInGroup = getStudentsFromAllUsersInGroup(allUsers);
        ArrayList<ArrayList<String>> submissionData = new ArrayList<>();
        for (String username : studentsInGroup){
            ArrayList<String> data = assignment.submissionData(username);
            data.add(username);
            submissionData.add(data);
        }
        userSession.put("resultData", submissionData);
        userSession.put("Assignment", assignment);
        return "/public/teacher/content/assignment/results";
    }

    /**
     * Creates an array of results from all quiz results
     * @param model A Model object
     * @param userSession a linked hashmap
     * @param quiz Quiz object which results are obtained from
     * @throws SQLException Thrown if an invalid SQL query gets produced.
     */
    public void setQuizResultsArray(Model model, @ModelAttribute("userSession") HashMap<String, Object> userSession, Quiz quiz) throws SQLException {
        quiz.updateClassFromDatabase();
        ArrayList<String> allUsers = (ArrayList<String>)userSession.get("usersInGroup");
        ArrayList<String> studentsInGroup = getStudentsFromAllUsersInGroup(allUsers);
        ArrayList<ArrayList<String>> resultData = quiz.getResultData(studentsInGroup);
        if(resultData == null){
            model.addAttribute("resultsMessage", "No students have completed the quiz yet!");
        }else{
            System.out.println("Quiz Result Data: " + resultData);
            userSession.put("resultData", resultData);
            userSession.put("Quiz", quiz);
        }
    }

    public static ArrayList<String> getStudentsFromAllUsersInGroup(ArrayList<String> usernames){
        ArrayList<String> students = new ArrayList<>();
        for (String username : usernames){
            boolean isStudent = findIfStudent(username);
            if (isStudent){
                students.add(username);
            }
        }
        return students;
    }

    /**
     * See if a specified student can be found.
     * @param username String of the student's username to find
     * @return Boolean that returns true if student is found, false if not.
     */
    public static boolean findIfStudent(String username){
        try {
            ArrayList<String> tableColumns = new ArrayList<>();
            tableColumns.add("Username");
            tableColumns.add("UserRole");
            HashMap<String, ArrayList<String>> results = DatabaseCRUD.read("Users", tableColumns, " WHERE Username = '" + username + "'");
            if (results.get("Username").get(1).equals("Student")){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    @GetMapping("/teacherviewassignments")
    public String teacherViewAssignments(Model model,
                                        @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Assignment.updateAssignmentsArrays(model, userSession);
        return "/public/teacher/content/assignment";
    }

    @GetMapping("/teacheraddlearningmaterial")
    public String teacherAddLearningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        // Add the topics to the session
        UserSession.addTopicsToSession(userSession);
        return "/public/teacher/content/learningmaterial/add";
    }

    @GetMapping("/teacherviewprogress")
    public String teacherViewProgress(@ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        setProgressData(userSession);
        return "/public/teacher/progress/index";
    }

    @PostMapping("/amend-feedback")
    public ModelAndView amendFeedback(@ModelAttribute("userSession") HashMap<String, Object> userSession,
                                      @RequestParam Map<String, String> parameters, RedirectAttributes attributes) throws SQLException {
        String username = (String) userSession.get("student");
        Group group = (Group) userSession.get("Group");
        DatabaseCRUD.update("User_Group", "Feedback", parameters.get("feedback"), "Username = " + "'" + username + "'" + " AND GroupID = '" + group.getGroupID() + "'");
        // Update the local session value so the teacher can see the value change
        ((String[]) userSession.get("studentGoalFeedback"))[0] = (parameters.get("personalgoal"));
        attributes.addFlashAttribute("feedbackMessage", "Your feedback was amended.");
        return new ModelAndView("redirect:/teacherselectstudentprogress");
    }

    @PostMapping("/selectedstudentprogress")
    public ModelAndView viewStudentProgress(@RequestParam Map<String, String> parameters, Model model,
                                            @ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        System.out.println(userSession.values());
        String username = parameters.get("studentUsername");
        Student.getStudentProgressData(userSession, username, model);
        userSession.put("student", username);
        return new ModelAndView("redirect:/teacherselectstudentprogress");
    }

    @GetMapping("/teacherselectstudentprogress")
    public String teacherViewStudentProgress(@RequestParam Map<String, String> parameters, Model model,
                                             @ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        // Acquire the student personal goal and add to the session
        Student student = new Student();
        student.setUsername((String) userSession.get("student"));
        userSession.put("studentGoalFeedback", student.getStudentGoalFeedback(userSession, false)[0]);
        return "/public/teacher/progress/index";
    }

    public void setProgressData(@ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        //get student usernames from all usernames in group and add to the session
        ArrayList<String> allUsers = (ArrayList<String>) userSession.get("usersInGroup");
        ArrayList<String> studentsInGroup = getStudentsFromAllUsersInGroup(allUsers);
        userSession.put("studentsInGroup", studentsInGroup);
        //add group topics array to the session
        UserSession.addTopicDataToSession(userSession);
        //create array of learning material data in the session
        createTempLearningMaterialSession(userSession);
        //LearningMaterial learningMaterial = userSession.get("learningMaterial")
        String[][] topics = (String[][]) userSession.get("Topics");
        Assignment assignment = new Assignment();
        ArrayList<ArrayList<String>> topicAssignments = assignment.getTopicAssignmentData(topics, studentsInGroup);
        //System.out.println("TA: " + topicAssignments);
        userSession.put("topicAssignments", topicAssignments);
    }

    @GetMapping("/teacheraddassignment")
    public String teacherAddAssignment(Model model, @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        Group group = (Group) userSession.get("Group");
        ArrayList<ArrayList<String>> topics = getTopicsInGroup(group.getGroupID());
        if (topics.isEmpty()) {
            model.addAttribute("topicMessage", "No Topics, create one to add an assignment to.");
        }
        userSession.put("Topics", topics);
        return "/public/teacher/content/assignment/add";
    }

    public ArrayList<ArrayList<String>> getTopicsInGroup(String groupID) {
        ArrayList<ArrayList<String>> topics = new ArrayList<>();
        try {

            ArrayList<String> tableColumns = new ArrayList<>();
            tableColumns.add("TopicID");
            tableColumns.add("TopicName");
            tableColumns.add("GroupID");
            HashMap<String, ArrayList<String>> results = DatabaseCRUD.read("Topics", tableColumns, " WHERE GroupID = '" + groupID + "'");
            ArrayList<ArrayList<String>> topicsData = General.splitByAmount(results.get("TopicID"), 3);
            for (ArrayList<String> topic : topicsData) {
                ArrayList<String> topicTemp = new ArrayList<>();
                topicTemp.add(topic.get(0));
                topicTemp.add(topic.get(1));
                topics.add(topicTemp);
            }
            return topics;
        } catch (Exception e) {
            e.printStackTrace();
            return topics;
        }
    }

    @PostMapping("/teacheramendassignment")
    public String teacherAmendAssignment(@RequestParam Map<String, String> parameters, Model model,
                                         @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        Group group = (Group) userSession.get("Group");
        ArrayList<ArrayList<String>> topics = getTopicsInGroup(group.getGroupID());
        if (topics.isEmpty()) {
            model.addAttribute("topicMessage", "No Topics, create one to add an assignment to.");
        }
        userSession.put("Topics", topics);
        if (parameters.get("assignmentData") != null) {
            String assignmentDataString = parameters.get("assignmentData");
            String[] assignmentData = General.getArrayFromDataString(assignmentDataString);
            Assignment assignment = new Assignment();
            assignment.setGroupID(assignmentData[0]);
            assignment.setAssignmentID(assignmentData[1]);
            assignment.setAssignmentType(assignmentData[2]);
            assignment.setName(assignmentData[3]);
            assignment.setMarkWeight(assignmentData[4]);
            assignment.setQuestionPDFLink(assignmentData[5]);
            assignment.setDueDateTime(assignmentData[6]);
            assignment.setStatus(assignmentData[7]);
            userSession.put("Assignment", assignment);
            return "/public/teacher/content/assignment/amend";
        } else {
            model.addAttribute("assignmentMessage", "Please select an assignment.");
            return "/public/teacher/content/assignment";
        }
    }

    @GetMapping("/teacherupdateassignmentname")
    public String teacherUpdateAssignmentName(Model model, @RequestParam Map<String, String> parameters,
                                              @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(parameters.get("assignmentName") != null && !parameters.get("assignmentName").equals("")) {
                Assignment assignment = (Assignment) userSession.get("Assignment");
                assignment.setName(parameters.get("assignmentName"));
                assignment.updateDatabase();
                assignment.updateClassFromDatabase();
                userSession.put("Assignment", assignment);
            }else{
                model.addAttribute("nameMessage", "Please enter a name.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            model.addAttribute("nameMessage", "Please enter a name.");

        }
        return "/public/teacher/content/assignment/amend";
    }

    @GetMapping("/teacherupdateassignmentmarks")
    public String teacherUpdateAssignmentMarks(Model model, @RequestParam Map<String, String> parameters,
                                               @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(parameters.get("assignmentMarks") != null && !parameters.get("assignmentMarks").equals("")) {
                Assignment assignment = (Assignment) userSession.get("Assignment");
                assignment.setMarkWeight(parameters.get("assignmentMarks"));
                assignment.updateDatabase();
                assignment.updateClassFromDatabase();
                userSession.put("Assignment", assignment);
            }else{
                model.addAttribute("markMessage", "Please enter a mark.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            model.addAttribute("markMessage", "Please enter a mark.");
        }
        return "/public/teacher/content/assignment/amend";
    }

    @GetMapping("/teacherupdateassignmentstatus")
    public String teacherUpdateAssignmentStatus(Model model, @RequestParam Map<String, String> parameters,
                                                @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(parameters.get("assignmentStatus") != null) {
                Assignment assignment = (Assignment) userSession.get("Assignment");
                assignment.setStatus(parameters.get("assignmentStatus"));
                assignment.updateDatabase();
                assignment.updateClassFromDatabase();
                userSession.put("Assignment", assignment);
            }else{
                model.addAttribute("statusMessage", "Please select a status.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            model.addAttribute("statusMessage", "Database Error");
        }
        return "/public/teacher/content/assignment/amend";
    }

    @GetMapping("/teacherupdateassignmentduedatetime")
    public String teacherUpdateAssignmentDueDateTime(Model model, @RequestParam Map<String, String> parameters,
                                                 @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(parameters.get("assignmentDueDateTime") != null && !parameters.get("assignmentDueDateTime").equals("")) {
                Assignment assignment = (Assignment) userSession.get("Assignment");
                assignment.setDueDateTime(parameters.get("assignmentDueDateTime"));
                assignment.updateDatabase();
                assignment.updateClassFromDatabase();
                userSession.put("Assignment", assignment);
            }else{
                model.addAttribute("dateMessage", "Enter a due date and time.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            model.addAttribute("dateMessage", "Enter a due date and time.");
        }
        return "/public/teacher/content/assignment/amend";
    }

    @GetMapping("/teacherupdateassignmenttopic")
    public String teacherUpdateAssignmentTopic(Model model, @RequestParam Map<String, String> parameters,
                                                     @ModelAttribute("userSession") HashMap<String, Object> userSession){
        try {
            if(parameters.get("topicID") != null) {
                Assignment assignment = (Assignment) userSession.get("Assignment");
                assignment.setTopicID(parameters.get("topicID"));
                assignment.updateDatabase();
                assignment.updateClassFromDatabase();
                userSession.put("Assignment", assignment);
            }else{
                model.addAttribute("topicMessage", "Select a Topic.");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            model.addAttribute("topicMessage", "Select a Topic.");
        }
        return "/public/teacher/content/assignment/amend";
    }

    @GetMapping("/teacherdeleteassignment")
    public String teacherDeleteAssignment(@RequestParam Map<String, String> parameters, Model model,
                                          @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        if (parameters.get("assignmentData") != null) {
            String assignmentDataString = parameters.get("assignmentData");
            String[] assignmentData = General.getArrayFromDataString(assignmentDataString);
            Assignment assignment = new Assignment();
            assignment.setAssignmentID(assignmentData[1]);
            try {
                if (assignmentData[2].equals("Quiz")) {
                    Quiz quiz = new Quiz();
                    String quizID = assignment.getQuizIDFromAssignmentID();
                    assignment.removeQuizLink();
                    quiz.setQuizID(quizID);
                    quiz.removeFromDatabase();
                }
                assignment.removeFromDatabase();
                model.addAttribute("deleleMessage", "Assignment has been deleted.");
            } catch (Exception e) {
                model.addAttribute("deleleMessage", "Failed to delete assignment.");
            }
        }else{
            model.addAttribute("deleleMessage", "Please select an assignment.");
        }
        return "/public/teacher/content/assignment";
    }

    @GetMapping("/teacher-topic-overview")
    public ModelAndView topicOverview(@ModelAttribute("userSession") HashMap<String, Object> userSession) throws SQLException {
        UserSession.addTopicsToSession(userSession);
        return new ModelAndView("/public/teacher/content/topics/index");
    }

    @PostMapping("/amendtopic")
    public ModelAndView amendTopic(@ModelAttribute("userSession") HashMap<String, Object> userSession, @RequestParam Map<String, String> parameters,
                                   RedirectAttributes attributes) throws SQLException {
        Topic topic = new Topic();
        topic.setTopicID(parameters.get("topicamendid"));
        topic.setTopicName(parameters.get("topicname"));
        topic.updateDatabase();
        attributes.addFlashAttribute("topicMessage", "The topic has been updated.");
        return new ModelAndView("redirect:/teacher-topic-overview");
    }

    @PostMapping("/deletetopic")
    public ModelAndView deleteTopic(@ModelAttribute("userSession") HashMap<String, Object> userSession, @RequestParam Map<String, String> parameters,
                                    RedirectAttributes attributes) throws SQLException {
        Topic topic = new Topic();
        topic.setTopicID(parameters.get("topicdeleteid"));
        // Delete content associated to the topic
        deleteTopicContent(parameters.get("topicdeleteid"));
        topic.deleteFromDatabase();
        attributes.addFlashAttribute("topicMessage", "The topic has been deleted.");
        return new ModelAndView("redirect:/teacher-topic-overview");
    }

    public void deleteTopicContent(String topicID) {
        // TODO Fix the delete topic functionality
        // Delete any content associated with the topic that is being deleted
        String[][] assignments = null;
        Topic topic = new Topic();
        topic.setTopicID(topicID);
        assignments = topic.getAssignmentsInTopic();
        // Loop through every assignment and delete it
        System.out.println(assignments);
        for (int row = 0; row < assignments.length; row++) {
            Assignment assignment = new Assignment();
            assignment.setAssignmentID(assignments[row][1]);
            // Delete every assignment and associated quiz
            assignment.removeQuizLink();
            assignment.removeFromDatabase();
        }
        // Delete the learning material
        String[][] learningMaterials = null;
        learningMaterials = topic.getLearningMaterialInTopic();
        // Loop through the material and delete it
        for (int row = 0; row < assignments.length; row++) {

            LearningMaterial learningMaterial = new LearningMaterial();
            learningMaterial.setLearningMaterialID(learningMaterials[row][1]);
            // Delete the learning material
            learningMaterial.deleteFromDatabase();
        }
    }

    @PostMapping("/createtopic")
    public ModelAndView createTopic(@ModelAttribute("userSession") HashMap<String, Object> userSession, @RequestParam Map<String, String> parameters,
                                    RedirectAttributes attributes) throws SQLException {
        Topic topic = new Topic();
        Group group = (Group) userSession.get("Group");
        topic.setTopicName(parameters.get("createtopicname"));
        topic.setGroupID(group.getGroupID());
        topic.addToDatabase();
        attributes.addFlashAttribute("topicMessage", "The topic has been added.");
        return new ModelAndView("redirect:/teacher-topic-overview");
    }

}
