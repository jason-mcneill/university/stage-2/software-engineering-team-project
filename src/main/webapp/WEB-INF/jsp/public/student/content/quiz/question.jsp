<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> ${userSession.Quiz.name} - Question</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<body>
<h1>Quiz</h1>
<h3>Title: ${userSession.Quiz.name}</h3>
<h3>${userSession.Quiz.description}</h3>
<h3>Total Marks: ${userSession.Quiz.totalScore}</h3>
<br>
<h2>Question: ${userSession.Question.name}</h2>
<h2>Marks: ${userSession.Question.markWeight}</h2><br><br>

<h4>1. ${userSession.Question.choice1}</h4>
<h4>2. ${userSession.Question.choice2}</h4>
<h4>3. ${userSession.Question.choice3}</h4>
<h4>4. ${userSession.Question.choice4}</h4>

<br>
<label for="studentQuestionAnswer">Select Answer:</label><br><br>
<form id="studentQuestionAnswer" name="studentQuestionAnswer"action="/studentanswerquestion" method="POST">
    <input type="radio" id="1" name="answer" value="1" checked="true">
    <label for="1">1</label><br>
    <input type="radio" id="2" name="answer" value="2">
    <label for="2">2</label><br>
    <input type="radio" id="3" name="answer" value="3">
    <label for="3">3</label><br>
    <input type="radio" id="4" name="answer" value="4">
    <label for="4">4</label><br>
    <input type="submit" value="Submit">
</form>
<br><br>
<label for="selectQuestion">Select Question:</label><br><br>
<form id="selectQuestion" action="/studentselectquestion" method="POST">
    <c:forEach var="question"   varStatus="i" items="${userSession.quizQuestions}">
        <button value="${question}" name="questionNumber"><td>Question ${i.count}: ${question[2]}</td></button>
    </c:forEach>
</form>
<h3>Current Answers:</h3>
<c:forEach var="question" varStatus="i" items="${userSession.quizAnswers}">
    <i>Question ${i.count}: </i>
    <i>${question}</i>
    <br>
</c:forEach>
<br>
<form id="nextQuestion" action="/studentselectquestion" method="POST">
    <button value="+1" name="questionNumber">Next Question</button>
</form>
<p>${questionMessage}</p>
<form id="completeQuiz" action="/studentsubmitquiz" method="POST">
    <button>Submit Quiz</button>
</form>
<p><a href="/studentviewgroup">Quit Quiz</a></p>
</body>

</html>