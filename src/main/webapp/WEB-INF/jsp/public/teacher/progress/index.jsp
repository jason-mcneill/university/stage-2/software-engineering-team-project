<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Teacher.username} - Progress Overview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/collapsible/style.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/></head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW PROGRESS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<h1>${userSession.Group.name} Progress</h1><br>
<c:forEach begin="0" end="${fn:length(userSession.Topics)}"
           var="topic" varStatus="loop" items="${userSession.Topics}">
    <%-- For each topic found in the same group as the user create a button of that topic --%>
    <c:if test="${topic[2] == userSession.Group.groupID}">
        <button type="button" class="collapsible" style="display: flex; justify-content: space-between; margin-left: 25%; margin-right: 25%">${topic[1]}</button>
    </c:if>
    <div class="material">
        <button type="button" class="collapsible" style="display: flex; justify-content: space-between; margin-left: 25%; margin-right: 25%">Learning Material | Class Average Time: ${topic[3]} minutes | Class Average Rating: ${topic[4]}</button>
        <div class="material">
    <c:forEach begin="0" end="${fn:length(userSession.learningMaterial)}"
               var="learningMaterial" varStatus="loop" items="${userSession.learningMaterial}">
        <c:if test="${topic[0] == learningMaterial[6]}">
            <p>${learningMaterial[3]}  |  Avg Time Spent: ${learningMaterial[7]} minutes |  Avg Rating: ${learningMaterial[8]}</p>
        </c:if>
    </c:forEach>
        </div>
        <button type="button" class="collapsible">Assignments | Average %: ${topic[5]} </button>
        <div class="material">
            <c:forEach begin="0" end="${fn:length(userSession.topicAssignments)}"
                       var="assignment" varStatus="loop" items="${userSession.topicAssignments}">
                <c:if test="${topic[0] == assignment[1]}">
                    <p>${assignment[0]} | Best Score: ${assignment[2]}% | Worst Score: ${assignment[3]}% | Average: ${assignment[4]}% </p>
                </c:if>
            </c:forEach>
        </div>
    </div>
</c:forEach>
<br><br>

<center>

<form id="teacherselectstudentform" action="/selectedstudentprogress" method="POST">
    <select id="teacherselectstudent" name="studentUsername">
        <option value="" selected disabled>Select Student</option>
        <c:forEach var="name" items="${userSession.studentsInGroup}">
            <option value="${name}">${name}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Submit">
</form>
<p>${selectMessage}</p>

<h2>${userSession.student}</h2>

<div id="studentPersonalGoal" style="display: none">
    <h3>Personal Goal</h3>
    <p id="personalgoal">${userSession.studentGoalFeedback[0]}</p>
</div>
<div id="teacherFeedback" style="display: none">
    <form action="/amend-feedback" method="post">
        <textarea id="feedback" name="feedback" rows="4" cols="50">${userSession.studentGoalFeedback[1]}</textarea><br>
        <button id="submitfeedback" name="submitfeedback" type="submit">Amend</button>
    </form>
</div>

<p>${feedbackMessage}</p>

<c:forEach var="topic" varStatus="loop" items="${userSession.StudentTopicData}">
    <%-- For each topic found in the same group as the user create a button of that topic --%>
    <button type="button" class="collapsible" style="display: flex; justify-content: space-between; margin-left: 25%; margin-right: 25%">${topic.key.get(1)}
        <a id="updateGraph" href="" onclick="createStudentGraph(${topic.key.get(0)}); return false;">Graph</a>
    </button>
    <div class="material">
        <button type="button" class="collapsible">Learning Material | Average Time: ${topic.value.get(0).get(0).get(0)} min | Average Rating: ${topic.value.get(0).get(0).get(1)}</button>
        <div class="material">
            <c:forEach var="learningMaterial"  items="${topic.value.get(1)}">
                    <p>${learningMaterial.get(0)}  |   Time Spent: ${learningMaterial.get(1)} min  |  Rating: ${learningMaterial.get(2)}</p>
            </c:forEach>
        </div>
        <button type="button" class="collapsible">Assignments | Average %: ${topic.value.get(0).get(0).get(2)}</button>
        <div class="material">
            <c:forEach var="assignment" items="${topic.value.get(2)}">
                    <p>${assignment.get(0)} | Percentage: ${assignment.get(1)}</p>
            </c:forEach>
        </div>
    </div>
</c:forEach>

</center>

<div id="progressGraphs">
    <div id="learningMaterial" style="height: 370px; width: 700px;"></div>
    <div id="assignment" style="height: 370px; width: 700px;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/collapsible.js"></script>

<script>
    function createStudentGraph(topicID) {
        var materialDataStudent = [];
        var materialDataClass = [];
        var assignmentDataStudent = [];
        var assignmentDataClass = [];
        var learningMaterialChart = new CanvasJS.Chart("learningMaterial", {
            theme: "dark1", // "light1", "dark1", "dark2"
            animationEnabled: true,
            exportEnabled: true,
            axisY: [{
                title: "Time Spent on Learning Material (${userSession.student})",
                suffix: " mins",
                lineColor: "#ff53e7",
                tickColor: "#ff53e7",
                labelFontColor: "#ff53e7",
                titleFontColor:"#ff53e7"
            }, {
                title: "Time Spent on Learning Material (Class Average)",
                suffix: " mins",
                lineColor: "#27fff3",
                tickColor: "#27fff3",
                labelFontColor: "#27fff3",
                titleFontColor:"#27fff3"
            }],
            data: [{
                type: "line",
                markerSize: 0,
                dataPoints: materialDataStudent,
                xValueType: "dateTime",
                color: "#ff53e7",

            }, {
                type: "line",
                markerSize: 0,
                dataPoints: materialDataClass,
                xValueType: "dateTime",
                color: "#27fff3",
            }]
        });
        var assignmentChart = new CanvasJS.Chart("assignment", {
            theme: "dark1", // "light1", "dark1", "dark2"
            animationEnabled: true,
            exportEnabled: true,
            axisY: [{
                title: "Assignment Score (${userSession.student})",
                suffix: " %",
                lineColor: "#32F647",
                tickColor: "#32F647",
                labelFontColor: "#32F647",
                titleFontColor:"#32F647"
            }, {
                title: "Assignment Score (Class Average)",
                suffix: " %",
                lineColor: "#ffb410",
                tickColor: "#ffb410",
                labelFontColor: "#ffb410",
                titleFontColor:"#ffb410"
            }],
            data: [{
                type: "line",
                markerSize: 0,
                dataPoints: assignmentDataStudent,
                xValueType: "dateTime",
                color: "#32F647",
            }, {
                type: "line",
                markerSize: 0,
                dataPoints: assignmentDataClass,
                xValueType: "dateTime",
                color: "#ffb410",
            }]
        });

        function compare(dataPointDate1, dataPointDate2) {
            return dataPointDate1.x - dataPointDate2.x;
        }

        <c:forEach var="topic" varStatus="loop" items="${userSession.StudentTopicData}">
        // Ensure that only learning material/assignments from the selected topic are shown
        if (topicID === ${topic.key.get(0)}) {
            // Acquire the student learning material data
            <c:forEach var="studentLearningMaterial" items="${topic.value.get(1)}">
            // Acquire the date for the learning material
            <c:forEach var="learningMaterialInfo" items="${userSession.learningMaterial}">
            <c:if test="${learningMaterialInfo[0] == studentLearningMaterial.get(3)}">
            materialDataStudent.push({
                x: Date.parse("${learningMaterialInfo[4]}"),
                y: ${studentLearningMaterial.get(1)},
                label: "${learningMaterialInfo[3]}",
            });
            </c:if>
            </c:forEach>
            </c:forEach>

            // Acquire the class learning material data
            <c:forEach var="learningMaterialClass" items="${userSession.learningMaterial}">
            // Check the topic matches the learning material average
            <c:if test="${topic.key.get(0) == learningMaterialClass[6]}">
            materialDataClass.push({
                x: Date.parse("${learningMaterialClass[4]}"),
                y: ${learningMaterialClass[7]},
                label: "${learningMaterialClass[3]}",
            });
            </c:if>
            </c:forEach>

            // Acquire the student assignment data
            <c:forEach var="assignmentInfo" items="${topic.value.get(2)}">
                <c:if test="${assignmentInfo.get(2) != null}">
                assignmentDataStudent.push({
                    x: Date.parse("${assignmentInfo.get(2)}"),
                    y: ${assignmentInfo.get(1)},
                    label: "${assignmentInfo.get(0)}",
                });
                // Go through each assignment
                <c:forEach var="assignment" varStatus="loop" items="${userSession.topicAssignments}">
                console.log("${assignment}");
                console.log("${assignmentInfo.get(3)}");
                console.log("----");
                <c:if test="${assignment[5] == assignmentInfo.get(3)}">
                    assignmentDataClass.push({
                        x: Date.parse("${assignmentInfo.get(2)}"),
                        y: ${assignment[4]},
                        label: "${assignmentInfo.get(0)}",
                    });
                    </c:if>
                </c:forEach>
                </c:if>
            </c:forEach>

        }
        </c:forEach>

        // Sort the graphs in date order
        learningMaterialChart.options.data[0].dataPoints.sort(compare);
        learningMaterialChart.options.data[1].dataPoints.sort(compare);
        assignmentChart.options.data[0].dataPoints.sort(compare);
        assignmentChart.options.data[1].dataPoints.sort(compare);

        learningMaterialChart.render();
        assignmentChart.render();
    }

    var student = ("${userSession.student}");
    if (student != null) {
        document.getElementById("studentPersonalGoal").style.display = "inline";
        document.getElementById("teacherFeedback").style.display = "inline";
    }
</script>
</body>
</html>