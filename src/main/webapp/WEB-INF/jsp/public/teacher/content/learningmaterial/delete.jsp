<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Learning Material - Delete</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - DELETE LEARNING MATERIAL</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Delete Learning Content</h1>
<p>Select active Learning material to delete</p>
<form id="deleteLectureMaterial" name="deleteLectureMaterial" action="/deletelearningmaterial" enctype="multipart/form-data" method="POST">
    <select id="learningMaterial" name="learningMaterial">
        <option value="" selected disabled>Select Learning Material</option>
        <c:forEach begin="0" end="${fn:length(userSession.learningMaterial)}"
                   var="learningMaterial" varStatus="loop" items="${userSession.learningMaterial}">
            <option value="${loop.index}">${learningMaterial[3]}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Delete">
</form>
<p>${deleteMessage}</p>
</body>
<script>
    $("form[name='deleteLectureMaterial']").validate({
        rules: {
            learningMaterial: {
                required: true,
            },
        },
        messages: {
            materialName: {
                required: "Please enter the Material Name.",
            }}
    })
</script>
</html>