package com.team34.notepic.Content;

import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.General.General;
import com.team34.notepic.Group;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.sql.SQLException;
import java.util.*;

import static com.team34.notepic.General.General.splitByAmount;
import static com.team34.notepic.Group.getQuizzesFromGroupID;

/**
 * @author Ben
 */
public class Quiz {
    private String quizID;
    private String groupID;
    private String totalScore;
    private String name;
    private String description;
    private String status;
    private String isAssignment;
    private String assignmentID;
    private ArrayList<String> questionIDs = new ArrayList<>();

    public void setAssignmentID(String assignmentID) { this.assignmentID = assignmentID; }
    public String getAssignmentID() { return this.assignmentID; }
    public void setIsAssignment(String isAssignment) { this.isAssignment = isAssignment; }
    public String getIsAssignment() { return this.isAssignment; }
    public void setStatus(String status) { this.status = status; }
    public String getStatus() { return this.status; }
    public ArrayList<String> getQuestionIDs() { return this.questionIDs; }
    public void setQuizID(String quizID) { this.quizID = quizID; }
    public void setGroupID(String groupID) { this.groupID = groupID; }
    public void setTotalScore(String totalScore) { this.totalScore = totalScore; }
    public void setName(String name) { this.name = name; }
    public void setDescription(String description) { this.description = description; }
    public String getQuizID() { return this.quizID; }
    public String getGroup() { return this.groupID; }
    public String getTotalScore() { return this.totalScore; }
    public String getName() { return this.name; }
    public String getDescription() { return this.description; }

    /**
     * Adds the instance of quiz to the database
     */
    public void addToDatabase() {
        Map<String, String> tableContents = new HashMap<>();
        tableContents.put("QuizID", this.quizID);
        tableContents.put("GroupID", this.groupID);
        tableContents.put("TotalScore", this.totalScore);
        tableContents.put("DisplayName", this.name);
        tableContents.put("Description", this.description);
        tableContents.put("Status", this.status);
        tableContents.put("IsAssignment", this.isAssignment);
        String tableName = "Quizzes";
        DatabaseCRUD.create(tableName, tableContents);

    }

    /**
     * sets the instances ID from the name, from searching the database
     */
    public void getIDFromDatabase(){
        ArrayList<String> quizTableColumns = new ArrayList<>();
        quizTableColumns.add("QuizID");
        quizTableColumns.add("DisplayName");
        String condition = " WHERE " + "DisplayName = " + "'" + this.name + "'";
        HashMap<String, ArrayList<String>> quizMap = DatabaseCRUD.read("Quizzes", quizTableColumns, condition);
        this.quizID = quizMap.get("QuizID").get(0);
    }

    /**
     * updates the database from the instance of the class
     */
    public void updateDatabase() {
        String condition = "QuizID = '" + this.quizID + "'";
        DatabaseCRUD.update("Quizzes", "TotalScore", this.totalScore, condition);
        DatabaseCRUD.update("Quizzes", "DisplayName", this.name, condition);
        DatabaseCRUD.update("Quizzes", "Description", this.description, condition);
        DatabaseCRUD.update("Quizzes", "Status", this.status, condition);
        DatabaseCRUD.update("Quizzes", "IsAssignment", this.isAssignment, condition);
    }

    /**
     * updates the class from the database using the instances ID
     */
    public void updateClassFromDatabase() {
        ArrayList<String> quizTableColumns = new ArrayList<>();
        quizTableColumns.add("QuizID");
        quizTableColumns.add("GroupID");
        quizTableColumns.add("TotalScore");
        quizTableColumns.add("Description");
        quizTableColumns.add("DisplayName");
        quizTableColumns.add("Status");
        quizTableColumns.add("IsAssignment");
        String condition = " WHERE " + "QuizID = " + "'" + this.quizID + "'";
        HashMap<String, ArrayList<String>> data = DatabaseCRUD.read("Quizzes", quizTableColumns, condition);
        if(data != null) {
            this.groupID = data.get("QuizID").get(1);
            this.totalScore = data.get("QuizID").get(2);
            this.description = data.get("QuizID").get(3);
            this.name = data.get("QuizID").get(4);
            this.status = data.get("QuizID").get(5);
            this.isAssignment = data.get("QuizID").get(6);
        }
    }

    /**
     * from a quiz ID, finds all questions in the quiz, removes questions and then the quiz
     */
    public void removeFromDatabase() {
        ArrayList<String> questionIDs = this.getQuestionIDsInQuiz();
        if (questionIDs != null){
            for (String questionID : questionIDs){
                Question question = new Question();
                question.setQuestionID(questionID);
                question.removeFromDatabase();
            }
        }
        String condition = " WHERE QuizID = '" + this.quizID + "'";
        DatabaseCRUD.delete("Quizzes", condition);
        //DatabaseCRUD.delete("Quizzes", "WHERE DisplayName = 'temp'" );
    }

    public void addQuestionID(String questionID) {
        this.questionIDs.add(questionID);
    }

    public void removeQuestionID(String questionID) {
        this.questionIDs.remove(questionID);
    }

    /**
     * finds all questions in a quiz
     * @return an array list of all questions in a quiz
     */
    public ArrayList<String> getQuestionsInQuiz() {
        ArrayList<String> questArray = new ArrayList<>();
        ArrayList<String> questionsTableColumns = new ArrayList<>();
        questionsTableColumns.add("QuestionID");
        questionsTableColumns.add("QuizID");
        questionsTableColumns.add("Question");
        questionsTableColumns.add("Choice1");
        questionsTableColumns.add("Choice2");
        questionsTableColumns.add("Choice3");
        questionsTableColumns.add("Choice4");
        questionsTableColumns.add("CorrectIndex");
        questionsTableColumns.add("MarkWeight");
        String condition = " WHERE " + "QuizID = " + "'" + this.quizID + "'";
        HashMap<String, ArrayList<String>> questionsMap = DatabaseCRUD.read("Question", questionsTableColumns, condition);
        if(questionsMap!= null) {
            questArray = questionsMap.get("QuestionID");
        }
        return questArray;
    }

    /**
     * Gets the results data for each student in a list and return an array list of array lists which include the
     * students username, score and percentage
     *
     * @param usernames an array list of usernames in the group
     * @return an array list of array lists which represent username, score pairs
     */
    public ArrayList<ArrayList<String>> getResultData(ArrayList<String> usernames) {
        try {
            Question question = new Question();
            ArrayList<String> questionIDsInQuiz = this.getQuestionIDsInQuiz();
            HashMap<String,String> studentScores = question.getScoresFromID(questionIDsInQuiz, usernames);
            ArrayList<String> studentSubmission = new ArrayList<>();
            for (Map.Entry<String, String> usernameScorePair : studentScores.entrySet()) {
                String username = usernameScorePair.getKey();
                String score = usernameScorePair.getValue();
                studentSubmission.add(username);
                studentSubmission.add(score);
                if (score.equals("incomplete")){
                    String percentage = "incomplete";
                    studentSubmission.add(percentage);
                }else{
                    String percentage = General.calcPercentage(score,this.getTotalScore());
                    studentSubmission.add(percentage);
                }
            }
            return splitByAmount(studentSubmission,3);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * finds all question IDs in a quiz and adds them to an array list
     *
     * @return an array list of question IDs in the quiz
     */
    public ArrayList<String> getQuestionIDsInQuiz(){
        ArrayList<String> questionsArray = new ArrayList<>();
        ArrayList<String> questionsTableColumns = new ArrayList<>();
        questionsTableColumns.add("QuizID");
        questionsTableColumns.add("QuestionID");
        String condition = " WHERE " + "QuizID = " + "'" + this.quizID + "'";
        HashMap<String, ArrayList<String>> questionsMap = new HashMap<>();
        questionsMap = DatabaseCRUD.read("Question", questionsTableColumns, condition);
        ArrayList<String> questArray = questionsMap.get("QuestionID");
        ArrayList<ArrayList<String>> questIDArrays = splitByAmount(questArray,2);
        for (ArrayList<String> ID : questIDArrays){
            questionsArray.add(ID.get(1));
        }
        return questionsArray;
    }

    /**
     * sets a quiz as an assignment
     *
     * @param assignmentID the assignment ID to link to the quiz ID, to set the current quiz instance as an
     *                     assignment
     */
    public void setAsAssignment(String assignmentID){
        try {
            Map<String, String> tableContents = new HashMap<>();
            tableContents.put("QuizID", this.quizID);
            tableContents.put("AssignmentID", assignmentID);
            String tableName = "AssignmentQuizzes";
            DatabaseCRUD.create(tableName, tableContents);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * loops through questions in a quiz and submits the an answer for each question
     *
     * @param username the username of the student to submit the quiz about
     * @param answers an array list of the the students answer
     * @param questions An array list of array lists on question
     */
    public void submitQuiz(String username ,ArrayList<String> answers, ArrayList<ArrayList<String>> questions){
        for (ArrayList<String> questionInArray : questions) {
            Question question = new Question();
            question.setCorrectIndex(questionInArray.get(7));
            question.setMarkWeight(questionInArray.get(8));
            question.setQuestionID(questionInArray.get(0));
            question.submitQuestion(username, answers.get(questions.indexOf(questionInArray)));
        }
    }

    public static void updateQuizQuestionData(Quiz quiz, Model model,
                                              @ModelAttribute("userSession") HashMap<String, Object> userSession){
        ArrayList<String> questionsArray = quiz.getQuestionsInQuiz();
        ArrayList<ArrayList<String>> questions = new ArrayList<>();
        ArrayList<String> answers = new ArrayList<>();
        try{
            questions = splitByAmount(questionsArray, 9);
            for (ArrayList<String> question : questions){
                answers.add(null);
            }
        } catch(Exception e){
            model.addAttribute("questionMessage","No Questions");
        }
        userSession.put("quizQuestions",questions);
        userSession.put("quizAnswers", answers);
    }

    public static void setQuizData(String quizID, Model model,
                                   @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Quiz quiz = new Quiz();
        quiz.setQuizID(quizID);
        quiz.updateClassFromDatabase();
        userSession.put("Quiz",quiz);
        updateQuizQuestionData(quiz, model, userSession);
        //create array of questions in the quiz
    }

    public static void updateQuizList(Model model,
                                      @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Group group = (Group)userSession.get("Group");
        ArrayList<String> quizzesInGroup = getQuizzesFromGroupID(group.getGroupID(),model);
        ArrayList<ArrayList<String>> quizzes = splitByAmount(quizzesInGroup,7);
        ArrayList<String> quizNames = new ArrayList<>();
        for (ArrayList<String> quiz : quizzes){
            if(quiz.get(6).equals("0")){
                quizNames.add(quiz.get(3));
                quizNames.add(quiz.get(0));
                quizNames.add(quiz.get(5));
            }
        }

        if (quizNames.get(0) == null){
            model.addAttribute("quizMessage", "No Quizzes, create a quiz!");
        }
        ArrayList<ArrayList<String>> quizzesArray = splitByAmount(quizNames, 3);
        userSession.put("quizzesInGroup", quizzesArray);
    }
}