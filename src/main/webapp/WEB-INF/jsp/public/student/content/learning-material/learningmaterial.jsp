<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Student.username} - Learning Material</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/collapsible/style.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - LEARNING MATERIAL</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>

<h1>${userSession.Group.name} - Learning Material</h1><br>
<c:forEach begin="0" end="${fn:length(userSession.Topics)}"
           var="topic" varStatus="loop" items="${userSession.Topics}">
    <%-- For each topic found in the same group as the user create a button of that topic --%>
    <c:if test="${topic[2] == userSession.Group.groupID}">
        <button type="button" class="collapsible">${topic[1]}</button>
    </c:if>
    <div class="material">
    <%-- Find the learning material that belongs to that topic and add it --%>
    <c:forEach begin="0" end="${fn:length(userSession.learningMaterial)}"
       var="learningMaterial" varStatus="loop" items="${userSession.learningMaterial}">
        <%-- Only show material that is live --%>
        <c:if test="${learningMaterial[5] == 'Live'}">
            <c:if test="${topic[0] == learningMaterial[6]}">
                <form style="display: flex; justify-content: space-between; margin-left: 10%; margin-right: 10%"
                      action="/selectLearningMaterial" enctype="multipart/form-data" method="POST">
                    <input type="hidden" id="learningMaterial" name="learningMaterial" value="${loop.index}">
                    <button type="submit" style="background: none; border: none;">${learningMaterial[3]}</button>
                    <p>Date Created: ${learningMaterial[4]}</p>
                </form>
            </c:if>
        </c:if>
    </c:forEach>
    </div>
</c:forEach>

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/collapsible.js"></script>

</body>
</html>