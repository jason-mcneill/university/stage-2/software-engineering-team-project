package com.team34.notepic.Content;

import com.team34.notepic.Database.DatabaseCRUD;
import org.springframework.security.core.parameters.P;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LearningMaterial {

    private String learningMaterialID;
    private String groupID;
    private String pdfLink;
    private String name;
    private String dateCreated;
    private String status;
    private String topicID;
    private String averageTimeSpent;
    private String averageRating;
    private String dateTimeFirstAccessed;

    public void setLearningMaterialID(String learningMaterialID) {
        this.learningMaterialID = learningMaterialID;
    }
    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }
    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }
    public void setName(String name) { this.name = name; }
    public void setDateCreated(String dateCreated) { this.dateCreated = dateCreated; }
    public void setStatus(String status) { this.status = status; }
    public void setTopicID(String topicID) { this.topicID = topicID; }
    public void setAverageRating(String averageRating) { this.averageRating = averageRating; }
    public void setAverageTimeSpent(String averageTimeSpent) { this.averageTimeSpent = averageTimeSpent; }
    public void setDateTimeFirstAccessed(String dateTimeFirstAccessed) { this.dateTimeFirstAccessed = dateTimeFirstAccessed; }

    public String getLearningMaterialID() { return this.learningMaterialID; }
    public String getGroupID() { return this.groupID; }
    public String getPdfLink() { return this.pdfLink; }
    public String getName() { return this.name; }
    public String getDateCreated() { return this.dateCreated; }
    public String getStatus() { return this.status; }
    public String getTopicID() { return this.topicID; }
    public String getAverageRating() { return this.averageRating; }
    public String getAverageTimeSpent() { return this.averageTimeSpent; }
    public String getDateTimeFirstAccessed() { return this.dateTimeFirstAccessed; }


    /**
     * Adds a new learning material row into the database using values found from the instance.
     * @author Jason
     */
    public void addToDatabase() {
        Map<String,String> tableContents = new HashMap<String, String>();
        tableContents.put("LearningMaterialID", this.learningMaterialID);
        tableContents.put("GroupID", this.groupID);
        tableContents.put("PDFLink", this.pdfLink);
        tableContents.put("DisplayName", this.name);
        tableContents.put("DateCreated", this.dateCreated);
        tableContents.put("Status", this.status);
        tableContents.put("TopicID", this.topicID);
        String tableName = "LearningMaterial";
        DatabaseCRUD.create(tableName, tableContents);
    }

    /**
     * Deletes the row from the AccessMaterial and LearningMaterial table where the learningMaterialID equals the
     * learningMaterialID attribute of the instance.
     * @author Jason
     */
    public void deleteFromDatabase() {
        DatabaseCRUD.delete("AccessMaterial", " WHERE LearningMaterialID = '" + this.learningMaterialID + "'");
        DatabaseCRUD.delete("LearningMaterial", " WHERE LearningMaterialID = '" + this.learningMaterialID + "'");
    }

    /**
     * Reads either a single row or multiple learning material rows from the database. If a single row is to be returned,
     * the learningMaterialID will be searched for in the database, otherwise the GroupID will be searched for. The values
     * are then returned and appended to a 2D array.
     * @param learningMaterialID The id of the learning material to be read
     * @return A 2d array of either one or more learning material rows or null if no rows are found
     * @author Jason
     */
    public String[][] readFromDatabase(String learningMaterialID) {
        ArrayList<String> learningMaterialColumns = new ArrayList<>();
        learningMaterialColumns.add("LearningMaterialID");
        learningMaterialColumns.add("GroupID");
        learningMaterialColumns.add("PDFLink");
        learningMaterialColumns.add("DisplayName");
        learningMaterialColumns.add("DateCreated");
        learningMaterialColumns.add("Status");
        learningMaterialColumns.add("TopicID");
        String condition;
        if (learningMaterialID != null) {
            condition= " WHERE learningMaterialID = " + "'" + learningMaterialID + "'";
        } else {
            condition= " WHERE GroupID = " + "'" + this.groupID + "'";
        }
        // Append learning material averages about each class onto the end of the learning material array
        return acquireLearningMaterialAverages(DatabaseCRUD.readAlt("LearningMaterial", learningMaterialColumns, condition));
    }

    /**
     * Acquire the total average time that every student has spent viewing the learning material and append
     * the results to the learning material array.
     * @param learningMaterial
     * @return A 2D array that contains information about each learning material, including the average viewing
     * time for that content.
     * @throws SQLException
     * @author Ben
     */
    public String[][] acquireLearningMaterialAverages(String[][] learningMaterial) {
        // Add the averages values
        ArrayList<ArrayList<String>> learningMaterialArray = new ArrayList<>();
        for (String[] material : learningMaterial){
            ArrayList<String> materialArray = new ArrayList<>();
            this.learningMaterialID = material[0];
            String[] averages = getMaterialAverages();
            materialArray.addAll(Arrays.asList(material));
            materialArray.addAll(Arrays.asList(averages));
            learningMaterialArray.add(materialArray);
        }
        String[][] converted = new String[learningMaterialArray.size()][];
        for (int i = 0; i < converted.length; i++)
            converted[i] = learningMaterialArray.get(i).toArray(new String[learningMaterialArray.get(i).size()]);
        return converted;
    }

    /**
     * The function returns the name of the learning material based on the value of the instance's learningMaterialID.
     * @author Ben
     */
    public void getNameFromID() {
        System.out.println();
        ArrayList<String> learningMaterialColumns = new ArrayList<>();
        learningMaterialColumns.add("LearningMaterialID");
        learningMaterialColumns.add("DisplayName");
        String condition= " WHERE learningMaterialID = " + "'" + learningMaterialID + "'";
        String[][] learningMaterial = DatabaseCRUD.readAlt("LearningMaterial",
                learningMaterialColumns, condition);
        if (learningMaterial != null) {
            this.name = learningMaterial[0][1];
        }
    }

    /**
     * Updates the learning material row columns where the learning material ID equals the learningMaterialID
     * attribute of the instance.
     * @author Jason
     */
    public void updateDatabase() {
        String condition = "LearningMaterialID = '" + this.learningMaterialID + "'";
        DatabaseCRUD.update("LearningMaterial", "DisplayName", this.name, condition);
        DatabaseCRUD.update("LearningMaterial", "PDFLink", this.pdfLink, condition);
        DatabaseCRUD.update("LearningMaterial", "Status", this.status, condition);
        DatabaseCRUD.update("LearningMaterial", "TopicID", this.topicID, condition);
    }

    /**
     * Reads one or more rows from the AccessMaterial table where the username of the student is matched and the
     * learning material ID of the instance is matched, returning a 2D array.
     * @param username The username of the student.
     * @return Student statistics of learning material they've accessed.
     * @author Jason
     */
    public String[][] readAccessMaterial(String username) {
        ArrayList<String> learningMaterialColumns = new ArrayList<>();
        learningMaterialColumns.add("Username");
        learningMaterialColumns.add("LearningMaterialID");
        learningMaterialColumns.add("TimeSpent");
        learningMaterialColumns.add("DateTimeFirstAccessed");
        learningMaterialColumns.add("Rating");
        String condition = " WHERE Username = " + "'" + username + "'" + " AND LearningMaterialID = " + "'" + this.learningMaterialID + "'";
        // There will only ever be one row returned, therefore we can return that one row at index 0
        String[][] learningMaterial = DatabaseCRUD.readAlt("AccessMaterial",
                learningMaterialColumns, condition);
        return learningMaterial;
    }

    /**
     * This function will add a new row to the Access Material table using the student's username and learning
     * material ID to generate a unique key.
     * @param username The username of the student that the access material data is about.
     * @author Jason
     */
    public void createAccessMaterial(String username) {
        Map<String,String> tableContents = new HashMap<String, String>();
        tableContents.put("Username", username);
        tableContents.put("LearningMaterialID", this.learningMaterialID);
        tableContents.put("TimeSpent", this.averageTimeSpent);
        tableContents.put("DateTimeFirstAccessed", this.dateTimeFirstAccessed);
        tableContents.put("Rating", this.averageRating);
        String tableName = "AccessMaterial";
        DatabaseCRUD.create(tableName, tableContents);
    }

    /**
     * Update the time that the user has spent on certain learning material using the learning material ID and username
     * to identify the correct row.
     * @param username The username of the student whose access material data is being updated.
     * @author Jason
     */
    public void updateAccessMaterialTimeSpent(String username) {
        String condition = "Username = '" + username + "'" + " AND LearningMaterialID = " + "'" + this.learningMaterialID + "'";
        DatabaseCRUD.update("AccessMaterial", "TimeSpent", this.averageTimeSpent, condition);
    }

    /**
     * Update the student's rating on a particular learning material using the learning material ID and username to
     * identify the row.
     * @param username The username of the student whose access material data is being updated.
     * @author Jason
     */
    public void updateRating(String username) {
        String condition = "Username = '" + username + "'" + " AND LearningMaterialID = " + "'" + this.learningMaterialID + "'";
        DatabaseCRUD.update("AccessMaterial", "Rating", this.averageRating, condition);
    }

    /**
     * This will calculate the average time and rating that the group spends on particular learning material.
     * @return A string array containing the average rating and time spent on learning material.
     * @author Ben
     */
    public String[] getMaterialAverages() {
        ArrayList<String> columns = new ArrayList<>();
        columns.add("Username");
        columns.add("LearningMaterialID");
        columns.add("TimeSpent");
        columns.add("DateTimeFirstAccessed");
        columns.add("Rating");
        String condition = " WHERE LearningMaterialID = " + "'" + this.learningMaterialID + "'";
        String[][] studentData = DatabaseCRUD.readAlt("AccessMaterial", columns, condition);
        int averageTime = 0;
        double averageRating = 0;
        if(studentData.length != 0){
            int timeCount = 0;
            int rateCount = 0;
            for (String[] data : studentData){
                if (data[2] != null){
                    averageTime += Integer.parseInt(data[2]);
                    timeCount += 1;
                }if(data[4] != null){
                    averageRating += Integer.parseInt(data[4]);
                    rateCount += 1;
                }
            }
            if (averageTime != 0){
                averageTime = averageTime/timeCount;
            }if (averageRating != 0){
                averageRating = averageRating/rateCount;
            }
        }
        return new String[]{Integer.toString(averageTime), Double.toString(averageRating)};
    }

    /**
     * This will acquire student material data for a student in a group and add it to an array list of data they have
     * on the material
     *
     * @param username the students username to collect the data for
     * @return an array of the learning material name, students time spent, rating and the material ID
     * @author Ben
     */
     public ArrayList<String> getStudentMaterialData(String username) {
         ArrayList<String> data = new ArrayList<>();
         ArrayList<String> columns = new ArrayList<>();
         columns.add("Username");
         columns.add("LearningMaterialID");
         columns.add("TimeSpent");
         columns.add("DateTimeFirstAccessed");
         columns.add("Rating");
         String condition = " WHERE LearningMaterialID = '" + this.learningMaterialID + "' AND Username = '" + username + "'";
         String[][] learningMaterial = DatabaseCRUD.readAlt("AccessMaterial", columns, condition);
         this.getNameFromID();
         if(learningMaterial != null && learningMaterial.length != 0) {
             data.add(this.name);
             data.add(learningMaterial[0][2]);
             data.add(learningMaterial[0][4]);
             data.add(this.learningMaterialID);
         }else{
             data.add(this.name);
             data.add("0");
             data.add("N/A");
             data.add(this.learningMaterialID);
         }
         return data;
     }
}