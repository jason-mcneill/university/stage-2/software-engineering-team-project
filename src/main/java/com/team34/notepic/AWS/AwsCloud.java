package com.team34.notepic.AWS;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AwsCloud {

    AwsClient awsClient = new AwsClient();

    /**
     * Uploads the multipart file passed through from the respective webpage forms, using the bucket, folder and filename
     * to generate a path to upload the file to in AWS S3.
     * @param file The multipart file that is uploaded to the server.
     * @param folder The folder where the file will be stored.
     * @param filename The name that identifies the file.
     * @param bucket The root bucket/folder which contains the file and folder.
     * @return A message saying that the file was uploaded successfully or failed.
     * @author Jason
     */
    public String uploadFile(@RequestPart(value = "file") MultipartFile file, String folder, String filename, String bucket) {
        awsClient.initializeAmazon();
        return awsClient.uploadFile(file, folder, filename, bucket);
    }

    /**
     * Deletes the file in AWS S3 using the fileURL as a key to identify where the object is stored in the bucket/
     * folder.
     * @param fileUrl The file url which contains the filename that is used to locate the file.
     * @param folder The folder where the file will be found.
     * @param bucket The root bucket/folder containing the folder and file.
     * @return A message saying the file was deleted successfully.
     * @author Jason
     */
    public String deleteFile(@RequestPart(value = "url") String fileUrl, String folder, String bucket) {
        awsClient.initializeAmazon();
        return awsClient.deleteFileFromS3Bucket(fileUrl, folder, bucket);
    }
}
