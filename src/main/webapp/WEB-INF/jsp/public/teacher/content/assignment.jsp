<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Group.name} - Assignment</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW ASSIGNMENTS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<body>
<h1>${userSession.Group.name} Assignments: </h1>
<h3>Add an assignment: </h3>
<p><a href="/teacheraddassignment">Add</a></p>
<h3>Select an Assignment to Amend: </h3>
<form id="teacherSelectAssignment" action="/teacheramendassignment" method="POST">
    <select id="assignmentData" name="assignmentData">
        <option value="" selected disabled>Select Assignment</option>
        <c:forEach var="name" items="${userSession.assignments}">
            <option value="${name}">${name[3]}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Submit">
</form>
<p>${assignmentMessage}</p>
<h3>Select an Assignment to Delete: </h3>
<form id="teacherDeleteAssignment" action="/teacherdeleteassignment" method="POST">
    <select id="assignmentDataDel" name="assignmentData">
        <option value="" selected disabled>Select Assignment</option>
        <c:forEach var="name" items="${userSession.assignments}">
            <option value="${name}">${name[3]}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Submit">
</form>
<p>${deleleMessage}</p>
<p>${assignmentsMessage}</p>
<h2>Live Assignments</h2>
<table>
    <tr>
        <th>Display Name</th>
        <th>Assignment Type</th>
        <th>Total Marks</th>
        <th>Due Date</th>
        <th>Topic</th>
        <th>View Results</th>
    </tr>
    <ul>
        <c:forEach var="assignment" items="${userSession.liveAssignments}">
            <tr>
                <td>${assignment[3]}</td>
                <td>${assignment[2]}</td>
                <td>${assignment[4]}</td>
                <td>${assignment[6]}</td>
                <td>${assignment[8]}</td>
                <form action="/teacherviewassignmentresults" method="post">
                    <input type="hidden" id="livAssignDataResults" name="assignmentDataResults" value="${assignment}">
                    <td>
                        <button type="submit">Results</button>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </ul>
</table>
<p>${liveAssignMessage}</p>
<h2>Closed Assignments</h2>
<table>
    <tr>
        <th>Display Name</th>
        <th>Assignment Type</th>
        <th>Total Marks</th>
        <th>Due Date</th>
        <th>Topic</th>
        <th>View Results</th>
    </tr>
    <ul>
        <c:forEach var="assignment" items="${userSession.closedAssignments}">
            <tr>
                <td>${assignment[3]}</td>
                <td>${assignment[2]}</td>
                <td>${assignment[4]}</td>
                <td>${assignment[6]}</td>
                <td>${assignment[8]}</td>
                <form action="/teacherviewassignmentresults" method="post">
                    <input type="hidden" id="cloAssignDataResults" name="assignmentDataResults" value="${assignment}">
                    <td>
                        <button type="submit">Results</button>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </ul>
</table>
<p>${complAssignMessage}</p>

<h2>Hidden Assignments</h2>
<table>
    <tr>
        <th>Display Name</th>
        <th>Assignment Type</th>
        <th>Total Marks</th>
        <th>Due Date</th>
        <th>Topic</th>
        <th>View Results</th>
    </tr>
    <ul>
        <c:forEach var="assignment" items="${userSession.hiddenAssignments}">
            <tr>
                <td>${assignment[3]}</td>
                <td>${assignment[2]}</td>
                <td>${assignment[4]}</td>
                <td>${assignment[6]}</td>
                <td>${assignment[8]}</td>
                <form action="/teacherviewassignmentresults" method="post">
                    <input type="hidden" id="hidAssignDataResults" name="assignmentDataResults" value="${assignment}">
                    <td>
                        <button type="submit">Results</button>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </ul>
</table>
<p>${hidAssignMessage}</p>
<p><a href="/teacherviewgroup">Back to ${userSession.Group.name} Overview</a></p>
</body>
</html>