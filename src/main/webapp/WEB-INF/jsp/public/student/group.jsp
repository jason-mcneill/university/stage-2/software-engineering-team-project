<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Student.username} - Group ${userSession.Group.name}</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - GROUP</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>${userSession.Group.name} Members: </h1>
<ul>
    <c:forEach var="user" items="${userSession.usersInGroup}">
        <li>${user}</li>
    </c:forEach>
</ul>
<p>This is an example group (one of many) that the student selects</p>
<i>Each link here represents a list of assignments/quizzes/learning material found within this particular group which the student can select and view</i>
<br>
<p><a href="/studentviewlearningmaterial">Learning Material</a></p>
<p><a href="/studentviewquizzes">Quizzes</a></p>
<p><a href="/studentviewassignments">Assignments</a></p>
<p><a href="/studentviewprogress">View Progress</a></p>
<p><a href="/studentoverview">Return to overview</a></p>
</body>
</html>