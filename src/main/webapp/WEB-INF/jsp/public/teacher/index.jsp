<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!DOCTYPE HTML>
<html>
<head>
    <title>Teacher - Overview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER OVERVIEW</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

    <h3>Current User: </h3>
    <form id="teacherDetails">
        <label>Username: ${userSession.Teacher.username}</label><br>
        <label>Title: ${userSession.Teacher.title}</label><br>
        <label>First Name: ${userSession.Teacher.firstName}</label><br>
        <label>Last Name: ${userSession.Teacher.lastName}</label><br>
        <label>Teacher Role: ${userSession.Teacher.teacherRole}</label><br>
    </form>

    <p>Welcome, ${userSession.Teacher.firstName}.</p>

    <form id="studentSelectGroup" action="/teacherviewgroup" method="POST">
        <label for="groupName">Groups:</label><br><br>
        <select id="groupName" name="groupName">
            <option value="" selected disabled>Select Group</option>
            <c:forEach var="name" items="${userSession.groupNames}">
                <option value="${name}">${name}</option>
            </c:forEach>
        </select><br><br>
        <input type="submit" value="Select">
    </form>
    <p>${teacherOverviewMessage}</p>
</body>
</html>