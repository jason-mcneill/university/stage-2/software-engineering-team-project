<!DOCTYPE HTML>
<html>
<head>
    <title>Headway</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/visitor.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>HEADWAY</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/adminlogin" id="navtext">ADMINISTRATOR</a>
            <a href="/login"><button class="btn" id="login" style="margin-right: 10px">LOGIN</button></a>
        </div>
    </div>
</div>
<body>

<div style="margin-left: 30px">
    <h1>Welcome, to Headway.</h1>
    <p>See what you can accomplish.</p>
</div>

</body>
</html>