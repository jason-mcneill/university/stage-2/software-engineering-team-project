<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Assignment - Amend</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - AMEND ASSIGNMENT</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Assignment ID: ${userSession.Assignment.assignmentID}</h1>

<form id="assignmentNameForm" action="/teacherupdateassignmentname" method="POST">
    <h3>Name: ${userSession.Assignment.name}   </h3>
    <label for="assignmentName">Change to:</label>
    <input type="text" id="assignmentName" name="assignmentName">
    <input type="submit" value="Submit">
</form>
<p>${nameMessage}</p>
<form id="assignmentMarksForm" action="/teacherupdateassignmentmarks" method="POST">
    <h3>Total Marks: ${userSession.Assignment.markWeight}   </h3>
    <label for="assignmentMarks">Change to:</label>
    <input type="number" id="assignmentMarks" name="assignmentMarks">
    <input type="submit" value="Submit">
</form>
<p>${markMessage}</p>
<form id="assignmentStatusForm" action="/teacherupdateassignmentstatus" method="POST">
    <h3>Status: ${userSession.Assignment.status}   </h3>
    <label for="live">Change Status</label><br>
    <input type="radio" id="live" name="assignmentStatus" value="Live">
    <label for="live">Live</label><br>
    <input type="radio" id="hidden" name="assignmentStatus" value="Hidden">
    <label for="hidden">Hidden</label><br><br>
    <input type="submit" value="Submit">
</form>
<p>${statusMessage}</p>
<form id="assignmentDueDateForm" action="/teacherupdateassignmentduedatetime" method="POST">
    <h3>Due Date: ${userSession.Assignment.dueDateTime}   </h3>
    <label for="assignmentDueDateTime">Change to:</label>
    <input type="datetime-local" id="assignmentDueDateTime" name="assignmentDueDateTime">
    <input type="submit" value="Submit">
</form>
<p>${dateMessage}</p>
<br>
<form id="assignmentTopicForm" action="/teacherupdateassignmenttopic" method="POST">
<label for="topicID">Topic:</label><br>
<select id="topicID" name="topicID">
    <option value="" selected disabled >Select Topic</option>
    <c:forEach var="name" items="${userSession.Topics}">
        <option value="${name[0]}">${name[1]}</option>
    </c:forEach>
    <input type="submit" value="Submit">
</select><br>
</form>
<p>${topicMessage}</p>
<p>${quizMessage}</p>
<p><a href="/teacherviewassignments">Return to ${userSession.Group.name} Assignments</a></p>
</body>
</html>