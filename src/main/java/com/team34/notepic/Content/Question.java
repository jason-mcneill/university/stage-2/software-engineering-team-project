package com.team34.notepic.Content;

import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.General.General;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ben
 */
public class Question {

    private String questionID;
    private String quizID;
    private String choice1;
    private String choice2;
    private String choice3;
    private String choice4;
    private String correctIndex;
    private String markWeight;
    private String name;

    public void setQuestionID(String questionID) { this.questionID = questionID; }
    public void setQuizID(String quizID) { this.quizID = quizID; }
    public void setChoice1(String choice1) { this.choice1 = choice1; }
    public void setChoice2(String choice2) { this.choice2 = choice2; }
    public void setChoice3(String choice3) { this.choice3 = choice3; }
    public void setChoice4(String choice4) { this.choice4 = choice4; }
    public void setCorrectIndex(String correctIndex) { this.correctIndex = correctIndex; }
    public void setMarkWeight(String markWeight) { this.markWeight = markWeight; }
    public void setName(String name) { this.name = name; }

    public String getQuestionID() { return this.questionID; }
    public String getQuizID() { return this.quizID = quizID; }
    public String getChoice1() { return this.choice1; }
    public String getChoice2() { return this.choice2; }
    public String getChoice3() { return this.choice3; }
    public String getChoice4() { return this.choice4; }
    public String getCorrectIndex() { return this.correctIndex; }
    public String getMarkWeight() { return this.markWeight; }
    public String getName() { return this.name; }

    /**
     * adds the instance of question to the database
     */
    public void addToDatabase() {
        Map<String, String> tableContents = new HashMap<>();
        tableContents.put("QuestionID", this.questionID);
        tableContents.put("QuizID", this.quizID);
        tableContents.put("Choice1", this.choice1);
        tableContents.put("Choice2", this.choice2);
        tableContents.put("Choice3", this.choice3);
        tableContents.put("Choice4", this.choice4);
        tableContents.put("CorrectIndex", this.correctIndex);
        tableContents.put("MarkWeight", this.markWeight);
        tableContents.put("Question", this.name);
        String tableName = "Question";
        DatabaseCRUD.create(tableName, tableContents);
    }

    /**
     * retrieves the question name from the instance ID
     */
    public void getIDFromDatabase() {
        ArrayList<String> questionTableColumns = new ArrayList<>();
        questionTableColumns.add("QuizID");
        questionTableColumns.add("QuestionID");
        String condition = " WHERE " + "Question = " + "'" + this.name + "'";
        HashMap<String, ArrayList<String>> quizMap = DatabaseCRUD.read("Question", questionTableColumns, condition);
        assert quizMap != null: "quizMap is 'null'";
        this.questionID = quizMap.get("QuizID").get(1);
    }

    /**
     * calls a read function on the database from the instance ID
     * @return the mark of the question
     */
    public String getMarkWeightFromID() {
        try {
            ArrayList<String> questionTableColumns = new ArrayList<>();
            questionTableColumns.add("MarkWeight");
            questionTableColumns.add("QuestionID");
            String condition = " WHERE " + "QuestionID = " + "'" + this.questionID + "'";
            HashMap<String, ArrayList<String>> quizMap = DatabaseCRUD.read("Question", questionTableColumns, condition);
            assert quizMap != null : "quizMap is null";
            return quizMap.get("QuestionID").get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Removes all question submissions for a question and then its questions
     */
    public void removeFromDatabase() {
        String condition = " WHERE QuestionID = '" + this.questionID + "'";
        try {
            DatabaseCRUD.delete("QuestionSubmission", condition);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            DatabaseCRUD.delete("Question", condition);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * creates a temporary question row in the database to retrieve the generated ID made by the database
     */
    public void createTemporary() {
        this.questionID = null;
        this.choice1 = this.choice2 = this.choice3 = this.choice4
                = this.correctIndex = this.markWeight =  "0";
        this.name = "temp";
        this.addToDatabase();
        this.getIDFromDatabase();
    }

    /**
     * updates the database with the instance of question it is called on
     */
    public void updateDatabase()  {
        String condition = "QuestionID = '" + this.questionID + "'";
        DatabaseCRUD.update("Question", "Question", this.name, condition);
        DatabaseCRUD.update("Question", "Choice1", this.choice1, condition);
        DatabaseCRUD.update("Question", "Choice2", this.choice2, condition);
        DatabaseCRUD.update("Question", "Choice3", this.choice3, condition);
        DatabaseCRUD.update("Question", "Choice4", this.choice4, condition);
        DatabaseCRUD.update("Question", "CorrectIndex", this.correctIndex, condition);
        DatabaseCRUD.update("Question", "MarkWeight", this.markWeight, condition);
    }

    /**
     * adds a student question entry to the database
     *
     * @param username the students username who completed the question
     * @param answer the index they chose for the question
     */
    public void submitQuestion(String username, String answer) {
        try {
            if (!this.correctIndex.equals(answer)) {
                this.markWeight = "0";
            }
            Map<String, String> tableContents = new HashMap<>();
            tableContents.put("QuestionID", this.questionID);
            tableContents.put("Username", username);
            tableContents.put("IndexChosen", answer);
            tableContents.put("Mark", this.markWeight);
            DatabaseCRUD.create("QuestionSubmission", tableContents);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     *
     * @param username the username of student to check is the question has been completed
     * @return boolean value for if the student has answered the question or not
     */
    public boolean isCompleted(String username) {
        HashMap<String, Object> returnVals = new HashMap<>();
        returnVals.put("boolean", false);
        try {
            ArrayList<String> questionSubColumns = new ArrayList<>();
            questionSubColumns.add("QuestionID");
            questionSubColumns.add("Mark");
            String condition = " WHERE " + "QuestionID = '" + this.questionID + "' AND Username = '" + username + "'";
            HashMap<String, ArrayList<String>> quizMap = DatabaseCRUD.read("QuestionSubmission", questionSubColumns, condition);
            assert quizMap != null: "QuizMap equals 'null'";
            if (quizMap.get("QuestionID") != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * finds the mark for an answer of a question by a student
     *
     * @param username the username of the student
     * @return the mark of the student for a question if available
     */
    public String getSubmissionMark(String username) {
        HashMap<String, Object> returnVals = new HashMap<>();
        returnVals.put("boolean", false);
        try {
            ArrayList<String> questionSubColumns = new ArrayList<>();
            questionSubColumns.add("QuestionID");
            questionSubColumns.add("Mark");
            //userGroupTableColumns.add("Username");
            String condition = " WHERE " + "QuestionID = '" + this.questionID + "' AND Username = '" + username + "'";
            HashMap<String, ArrayList<String>> quizMap = DatabaseCRUD.read("QuestionSubmission", questionSubColumns, condition);
            if (quizMap.isEmpty()){
                return "0";
            }else{
                String mark = quizMap.get("QuestionID").get(1);
                return mark;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *  calculates the total score for a list on students for multiple questions and adds it to an hashmap relating to
     *  the student
     *
     * @param questionIDsInQuiz an array list of question IDs
     * @param usernames an array list of usernames to calculate a total score for
     * @return a hashmap of student username keys and their total score as values
     */
    public HashMap<String,String> getScoresFromID(ArrayList<String> questionIDsInQuiz, ArrayList<String> usernames){
        HashMap<String,String> scores = new HashMap<>();
        try {
            for (String username : usernames) {
                ArrayList<String> questionSubColumns = new ArrayList<>();
                questionSubColumns.add("Username");
                questionSubColumns.add("QuestionID");
                questionSubColumns.add("Mark");
                String condition = " WHERE Username = '" + username + "'";
                HashMap<String, ArrayList<String>> quizMap = DatabaseCRUD.read("QuestionSubmission",
                        questionSubColumns, condition);
                if(!quizMap.isEmpty()){
                    ArrayList<ArrayList<String>> questionIDs = General.splitByAmount(quizMap.get("Username"),3);
                    int score = 0;
                    boolean completed = false;
                    for (String questionID : questionIDsInQuiz){
                        for (ArrayList<String> questionIDfromDB : questionIDs){
                            if (questionIDfromDB.get(1).equals(questionID)){
                                score += Integer.parseInt(questionIDfromDB.get(2));
                                completed = true;
                            }
                        }
                    }
                    if (completed){
                        scores.put(username,Integer.toString(score));
                    }else{
                        scores.put(username,"incomplete");
                    }

                }else{
                    scores.put(username,"incomplete");
                }
                }

            return scores;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Converts the question instance to an array
     *
     * @return array to represent the instance
     */
    public ArrayList<String> questionToArray(){
        ArrayList<String> questionArray = new ArrayList<>();
        questionArray.add(this.questionID);
        questionArray.add(this.quizID);
        questionArray.add(this.name);
        questionArray.add(this.choice1);
        questionArray.add(this.choice2);
        questionArray.add(this.choice3);
        questionArray.add(this.choice4);
        questionArray.add(this.correctIndex);
        questionArray.add(this.markWeight);
        return questionArray;
    }
}
