<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Group.name} - Quizzes</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - QUIZZES</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>${userSession.Group.name} Quizzes: </h1>
<h2>To do:</h2>
<p>${toDoMessage}</p>
<table>
    <tr>
        <th>Title</th>
        <th>Total Marks</th>
    </tr>
    <ul>
        <c:forEach var="quiz" items="${userSession.quizzesToDo}">
            <tr>
                <td>${quiz[1]}</td>
                <td>${quiz[2]}</td>
            </tr>
        </c:forEach>
    </ul>
</table>
<%--
can submit without choosing anything in select needs validation
--%>
<h2>Select a quiz to complete below.</h2>
<form id="studentSelectQuiz" action="/studentviewquiz" method="POST">
    <select id="quizData" name="quizData">
        <option value="" selected disabled>Select Quiz</option>
        <c:forEach var="name" items="${userSession.quizzesToDo}">
            <option value="${name[0]}">${name[1]}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Submit">
</form>
<p>${quizMessage}</p>
<h2>Completed Quizzes:</h2>
<table>
    <tr>
        <th>Title</th>
        <th>Total Marks</th>
        <th>Your Marks</th>
        <th>Percentage%</th>
    </tr>
    <ul>
        <c:forEach var="quiz" items="${userSession.quizzesCompleted}">
            <tr>
                <td>${quiz[1]}</td>
                <td>${quiz[2]}</td>
                <td>${quiz[3]}</td>
                <td>${quiz[4]}</td>
            </tr>
        </c:forEach>
    </ul>
</table>
<p>${completedMessage}</p>
<p><a href="/studentviewgroup">Back to ${userSession.Group.name} Overview</a></p>
</body>
</html>