<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Student.username} - Overview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/collapsible/style.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/></head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT OVERVIEW</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<p>This is the student overview page</p>

<form id="teacherDetails">
    <h3>Current User: </h3>
    <label>Username: ${userSession.Student.username}</label><br>
    <label>First Name: ${userSession.Student.firstName}</label><br>
    <label>Last Name: ${userSession.Student.lastName}</label><br>
    <label>Year of Study: ${userSession.Student.yearOfStudy}</label><br>
</form>
<p>Welcome, ${userSession.Student.firstName}.</p>

<label for="groupName">Groups:</label><br><br>
<form id="teacherSelectGroup" action="/studentviewgroup" method="POST">
    <select id="groupName" name="groupName">
        <option value="" selected disabled>Select Group</option>
        <c:forEach var="name" items="${userSession.groupNames}">
            <option value="${name}">${name}</option>
        </c:forEach>
    </select><br><br>
    <input type="submit" value="Select">
</form>
<p>${studentOverviewMessage}</p>

</body>
</html>