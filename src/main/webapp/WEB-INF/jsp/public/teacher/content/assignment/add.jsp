<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Assignment - Add</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - ADD ASSIGNMENT</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Create an Assignment for ${userSession.Group.name}: </h1>
Will not work if there is an assignment name is blank or = "temp", done validation in code, not jsp tho
<form id="addQuiz" action="/teachercreateassignment" enctype="multipart/form-data" method="POST">
    <label for="name">Assignment Name:</label><br>
    <input type="text" id="name" name="name"><br><br>

    <label for="duedatetime">Due Date and Time:</label><br>
    <input type="datetime-local" id="duedatetime" name="dueDateTime"><br><br>

    <label for="setTopic">Topic:</label><br>
    <select id="setTopic" name="topicID">
        <option value="" selected disabled>Select Topic</option>
        <c:forEach var="name" items="${userSession.Topics}">
            <option value="${name[0]}">${name[1]}</option>
        </c:forEach>
    </select><br>
    <p>${topicMessage}</p>

    <label for="live">Status:</label><br>
    <input type="radio" id="live" name="status" value="Live">
    <label for="live">Live</label><br>
    <input type="radio" id="hidden" name="status" value="Hidden" checked="checked">
    <label for="hidden">Hidden</label><br><br>

    <label for="assignmentType">Assignment Type:</label><br>
    <select id="assignmentType" name="assignmentType" onchange="displayUserFields()" required>
        <option value="" selected disabled>Select Type</option>
        <option value="Quiz">Quiz</option>
        <option value="PDF">PDF</option>
    </select><br><br>

    <div id="pdfLink" style="display: none">
        <input type="file" accept=".pdf" id="file" name="file">
        <br><br>
        <label for="marks">Total Marks:</label><br>
        <input type="text" id="marks" name="marks"><br>
    </div>
    <br>
    <input type="submit" value="Create Assignment">
</form>
<p>${createAssignmentMessage}</p>
<p><a href="/teacherviewassignments">Back to Assignments</a></p>
<p><a href="/teacherviewgroup">Back to ${userSession.Group.name} Overview</a></p>
</body>
<script>
    function displayUserFields() {
        var assignmentType = document.getElementById("assignmentType").value;
        var pdfLink = document.getElementById("pdfLink");

        switch (assignmentType) {
            case "PDF":
                pdfLink.style.display = "block";
                break;
            case "Quiz":
                pdfLink.style.display = "none";
                break;
        }
    }
</script>
</html>