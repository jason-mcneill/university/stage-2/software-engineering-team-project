<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Quiz - Add</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
</head>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - ADD QUIZ</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<body>
<h1>Create a Quiz</h1>
<h3>Name: ${userSession.Quiz.name}   </h3>
<form id="quizNameForm" action="/teacherupdatequizname" method="POST">
    <label for="quizName">Change to:</label>
    <input type="text" id="quizName" name="quizName">
    <input type="submit" value="Submit">
</form>
<h3>Description: ${userSession.Quiz.description}   </h3>
<form id="quizDescriptionForm" action="/teacherupdatequizdescription" method="POST">
    <label for="quizDescription">Change to:</label>
    <input type="text" id="quizDescription" name="quizDescription">
    <input type="submit" value="Submit">
</form>
<h3>Status: ${userSession.Quiz.status}   </h3>
<form id="quizStatusForm" action="/teacherupdatequizstatus" method="POST">
    <label for="live">Change Status</label><br>
    <input type="radio" id="live" name="quizStatus" value="Live">
    <label for="live">Live</label><br>
    <input type="radio" id="hidden" name="quizStatus" value="Hidden">
    <label for="hidden">Hidden</label><br><br>
    <input type="submit" value="Submit">
</form>
<p>${quizMessage}</p>
<h1>Questions: </h1>
<table>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Choice 1</th>
        <th>Choice 2</th>
        <th>Choice 3</th>
        <th>Choice 4</th>
        <th>Correct Choice</th>
        <th>Mark's Worth</th>
    </tr>
    <c:forEach var="question" items="${userSession.quizQuestions}">
        <tr>
            <td>${question[0]}</td>
            <td>${question[2]}</td>
            <td>${question[3]}</td>
            <td>${question[4]}</td>
            <td>${question[5]}</td>
            <td>${question[6]}</td>
            <td>${question[7]}</td>
            <td>${question[8]}</td>
        </tr>
    </c:forEach>
</table>
<h3>TOTAL SCORE: ${userSession.Quiz.totalScore}</h3>
<br>

<form id="addQuestion" name="addQuestion" action="/teacheraddquizquestion" method="POST">
    <label for="question">Question:</label><br>
    <input type="text" id="question" name="question"><br>

    <label for="choice1">Choice 1:</label><br>
    <input type="text" id="choice1" name="choice1"><br>

    <label for="choice2">Choice 2:</label><br>
    <input type="text" id="choice2" name="choice2"><br>

    <label for="choice3">Choice 3:</label><br>
    <input type="text" id="choice3" name="choice3"><br>

    <label for="choice4">Choice 4:</label><br>
    <input type="text" id="choice4" name="choice4"><br><br>

    <select id="correctChoice" name="correctChoice">
        <option value="" selected disabled>Select Correct Choice</option>
        <option value="1">Choice 1</option>
        <option value="2">Choice 2</option>
        <option value="3">Choice 3</option>
        <option value="4">Choice 4</option>
    </select><br><br>
    <label for="marksWorth">Marks Worth:</label><br>
    <input type="text" id="marksWorth" name="marksWorth"><br>
    <br>
    <input type="submit" value="Add Question">
</form>
<p>${questionMessage}</p>
<p><a href="/teacherviewquizzes">Create Quiz</a></p>
<p><a href="/teacherviewquizzes">Back to ${userSession.Group.name} Quizzes</a></p>
</body>
<script>
$("form[name='addQuestion']").validate({
    rules: {
        question: {
        required: true,
        },
        choice1:{
        required: true,
        },
        choice2:{
            required: true,
        },
        choice3:{
            required: true,
        },
        choice4:{
            required: true,
        },
        correctChoice:{
            required: true,
        },
        marksWorth:{
            required: true,
        },
   }
})
</script>
</html>