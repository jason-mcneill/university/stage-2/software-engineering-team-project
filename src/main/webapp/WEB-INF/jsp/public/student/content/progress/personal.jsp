<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>${userSession.Student.username} - Progress</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/collapsible/style.css"/>
    <head>
        <nav class="nav-bar">
        </nav>
        <div class="navbar topnav-default">
            <div class="navbar-left">
                <h1>STUDENT - PROGRESS</h1>
            </div>
            <div class="navbar-icon-top">
                <div class="navbar-right">
                    <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
                    <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
                </div>
            </div>
        </div>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type="text/javascript">
            window.onload = function() {

                var dps1 = [[]];
                var dps2 = [[]];
                var dps3 = [[]];
                var dps4 = [[]];

                var xValue;
                var yValue1;
                var yValue2;
                var yValue3;
                var label1;

                var chart = new CanvasJS.Chart("chartContainer", {
                    theme: "dark1", // "light1", "dark1", "dark2"
                    animationEnabled: true,
                    exportEnabled: true,
                    axisY: [{
                        title: "Mark - Percent",
                        suffix: "%",
                        lineColor: "#ff53e7",
                        tickColor:"#ff53e7",
                        labelFontColor: "#ff53e7",
                        titleFontColor: "#ff53e7"
                    },{
                        title: "Average Topic Material Rating",
                        lineColor: "#d4ff33",
                        tickColor: "#d4ff33",
                        labelFontColor: "#d4ff33",
                        titleFontColor:"#d4ff33"
                    },{
                        title: "Average Topic Materail Time Spent",
                        lineColor: "#27fff3",
                        tickColor: "#27fff3",
                        labelFontColor: "#27fff3",
                        titleFontColor:"#27fff3"
                    }],
                    data: [{
                        type: "line",
                        axisYIndex: 0,
                        color: "#d4ff33",
                        dataPoints: dps1[0]
                    },{
                        type: "line",
                        name: "Rating",
                        axisYIndex: 1,
                        color: "#ff53e7",
                        dataPoints: dps2[0]
                    },{
                        type: "line",
                        name: "Time",
                        axisYIndex: 2,
                        color: "#27fff3",
                        dataPoints: dps3[0]
                    }]
                });

                var chart1 = new CanvasJS.Chart("chartContainer1", {
                    theme: "dark1", // "light1", "dark1", "dark2"
                    animationEnabled: true,
                    exportEnabled: true,
                    axisY: [{
                        title: "Mark - Percent",
                        suffix: "%",
                        lineColor: "#32F647",
                        tickColor:"#32F647",
                        labelFontColor: "#32F647",
                        titleFontColor: "#32F647"
                    }],
                    axisX:{
                        valueFormatString: "DD MMM"
                    },
                    data: [{
                        type: "line",
                        color: "#32F647",
                        xValueType: "dateTime",
                        dataPoints: dps4[0]
                    }]
                });

                <c:forEach items='${assignmentDataPoints}' var="dataPoints" varStatus="loop">
                <c:forEach items="${dataPoints}" var="dataPoint">
                label1 = "${dataPoint.label}";
                yValue1 = parseFloat("${dataPoint.y1}");
                yValue2 = parseFloat("${dataPoint.y2}");
                yValue3 = parseFloat("${dataPoint.y3}");
                xValue = Date.parse( "${dataPoint.x}");

                dps1[parseInt("${loop.index}")].push({
                    label : label1,
                    y : yValue1
                });
                dps2[parseInt("${loop.index}")].push({
                    label : label1,
                    y : yValue2
                });
                dps3[parseInt("${loop.index}")].push({
                    label : label1,
                    y : yValue3
                });
                dps4[parseInt("${loop.index}")].push({
                    x : xValue,
                    y : yValue1,
                    label : label1
                });
                </c:forEach>
                </c:forEach>

                function compare(dataPointDate1, dataPointDate2) {
                    return dataPointDate1.x - dataPointDate2.x;
                }

                // Sort the dates in order
                chart.options.data[0].dataPoints.sort(compare);
                chart1.options.data[0].dataPoints.sort(compare);

                chart.render();
                chart1.render();

            }
        </script>
</head>
<body>
<h2>Personal Goal and Feedback:</h2>
<div id="amendPersonalGoal">
    <form action="/amend-personal-goal" method="post">
        <textarea id="personalgoal" name="personalgoal" rows="4" cols="50">${userSession.studentGoalFeedback[0]}</textarea><br>
        <button id="submitgoal" name="submitgoal" type="submit">Amend</button>
    </form>
    <b>Feedback</b><br>
    <i>${userSession.studentGoalFeedback[1]}</i>
</div>

<p>${personalGoalMessage}</p>

<h2>Your Progress broken down by Topic:</h2>
<c:forEach var="topic" varStatus="loop" items="${userSession.StudentTopicData}">
    <%-- For each topic found in the same group as the user create a button of that topic --%>
    <button type="button" class="collapsible" style="display: flex; justify-content: space-between; margin-left: 25%; margin-right: 25%">${topic.key.get(1)}</button>
    <div class="material">
        <button type="button" class="collapsible">Learning Material | Average Time: ${topic.value.get(0).get(0).get(0)} min | Average Rating: ${topic.value.get(0).get(0).get(1)}</button>
        <div class="material">
            <c:forEach var="learningMaterial"  items="${topic.value.get(1)}">
                <p>${learningMaterial.get(0)}  |  Time Spent: ${learningMaterial.get(1)} min |  Rating: ${learningMaterial.get(2)}</p>
            </c:forEach>
        </div>
        <button type="button" class="collapsible">Assignments | Average %: ${topic.value.get(0).get(0).get(2)}</button>
        <div class="material">
            <c:forEach var="assignment" items="${topic.value.get(2)}">
                <p>${assignment.get(0)} | Percentage: ${assignment.get(1)}</p>
            </c:forEach>
        </div>
    </div>
</c:forEach>
<h2>Assignment Trends:</h2>
<i>Below are your assignment marks alongside the average time you spent and your average rating of material in the topic of the assignment.</i>
<div id="chartContainer" style="height: 400px; width: 80%;"></div><br>
<div id="chartContainer1" style="height: 400px; width: 80%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/collapsible.js"></script>

<script>
    // Determine what div to show based on whether the personal goal has been set yet
    var personalGoal = String('${userSession.studentGoalFeedback[0]}');
    console.log(personalGoal);
    if (personalGoal === "") {
        document.getElementById("personalgoal").value = "Add a personal goal you want to achieve here.";
    }
</script>
</body>
</html>