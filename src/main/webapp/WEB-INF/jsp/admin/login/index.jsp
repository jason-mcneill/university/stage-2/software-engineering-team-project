<!DOCTYPE HTML>
<html>
<head>
    <title>Admin - Page</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/admin.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>HEADWAY</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/adminlogin" id="navtext">ADMINISTRATOR</a>
            <a href="/login"><button class="btn" id="login" style="margin-right: 10px">LOGIN</button></a>
        </div>
    </div>
</div>
<body>

<div class="absent-nav">
    <h2>Login to an admin account or register as an admin for a school</h2><br><br>
    <div class="left-pane">
    <button class="btn" onclick="adminLogin()">Admin Login</button>
        <button class="btn-alt" onclick="adminRegister()">Admin Registration</button>
    </div>
</div>

<script>
    function adminLogin() {
        //window.location = "../user/login.jsp";
        window.location.href = "/login";
    }

    function adminRegister() {
        window.location.href = "/registrationPage";
    }
</script>

</body>
</html>