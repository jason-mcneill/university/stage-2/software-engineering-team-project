package com.team34.notepic.User;

import com.team34.notepic.AWS.AwsCloud;
import com.team34.notepic.Content.*;
import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.Security.UserSession;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.team34.notepic.General.General;
import com.team34.notepic.Group;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
@SessionAttributes("userSession")
@RequestMapping(method={RequestMethod.POST,RequestMethod.GET})
public class Student extends User{

    private String yearOfStudy;

    public void setYearOfStudy(String yearOfStudy) { this.yearOfStudy = yearOfStudy; }
    public String getYearOfStudy() {return this.yearOfStudy;}

    /**
     * calls parent to add student to the user class, then adds extra info to the student class
     *
     * @throws SQLException
     * @author Ben
     */
    public void addToDatabase() throws SQLException {
        super.addToDatabase();
        Map<String, String> values = new HashMap<>();
        values.put("Username", this.getUsername());
        values.put("YearOfStudy", this.getYearOfStudy());
        DatabaseCRUD.create("Student", values);
    }

    /**
     * create temporary learning material to get generated ID from the database
     *
     * @param userSession linked hash map
     * @author Jason
     */
    public void createTempLearningMaterialSession(@ModelAttribute("userSession") HashMap<String, Object> userSession)  {
        LearningMaterial learningMaterial = new LearningMaterial();
        Group group = (Group) userSession.get("Group");
        learningMaterial.setGroupID(group.getGroupID());
        userSession.put("learningMaterial", learningMaterial.readFromDatabase(null));
    }

    /**
     * creates access material for when a student accesses some learning material and stores the data collected in the
     * from the student opening the material in an instance of learning material
     *
     * @param userSession linked hash map
     * @author Jason
     */
    public void createAccessMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession)  {
        LearningMaterial learningMaterial = new LearningMaterial();
        Student student = (Student) userSession.get("Student");
        String[] tempLearningMaterial = (String[]) userSession.get("selectedLearningMaterial");
        // The rating column should be left blank
        learningMaterial.setLearningMaterialID(tempLearningMaterial[0]);
        learningMaterial.setDateTimeFirstAccessed(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
        learningMaterial.setAverageTimeSpent("0");
        learningMaterial.createAccessMaterial(student.getUsername());
    }

    /**
     * Finds if a student has accessed a piece of learning material by reading data from database with the material ID
     *
     * @param userSession linked hash map
     * @return boolean, true is the user has accessed the material
     */
    public boolean hasAccessedMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        LearningMaterial learningMaterial = new LearningMaterial();
        Student student = (Student) userSession.get("Student");
        String[] tempLearningMaterial = (String[]) userSession.get("selectedLearningMaterial");
        learningMaterial.setLearningMaterialID(tempLearningMaterial[0]);
        String[][] accessMaterial = learningMaterial.readAccessMaterial(student.getUsername());
        // Return true is there is access material and false if not
        return accessMaterial.length != 0;
    }

    /**
     * gets the personal goal the user enters and updates the database with it
     *
     * @param userSession linked hash map
     * @param parameters the personal goal entered by the student
     * @param attributes a container for data passed from controller to jsps
     * @return redirect back to student progress
     */
    @PostMapping("/amend-personal-goal")
    public ModelAndView amendPersonalGoal(@ModelAttribute("userSession") HashMap<String, Object> userSession,
                                          @RequestParam Map<String, String> parameters, RedirectAttributes attributes) {
        if(parameters.get("personalgoal") != null && !parameters.get("personalgoal").equals("")) {
            Student student = (Student) userSession.get("Student");
            Group group = (Group) userSession.get("Group");
            DatabaseCRUD.update("User_Group", "PersonalGoal", parameters.get("personalgoal"), "Username = " + "'" + student.getUsername() + "'" + " AND GroupID = '" + group.getGroupID() + "'");
            // Update the local session value so the student can see the value change
            ((String[]) userSession.get("studentGoalFeedback"))[0] = (parameters.get("personalgoal"));
            attributes.addFlashAttribute("personalGoalMessage", "Your personal goal was amended.");
        }
        return new ModelAndView("redirect:/studentviewprogress");
    }

    /**
     * adds a students rating of a material to an instance of learning material
     *
     * @param userSession linked hash map
     * @param parameters the of the learning material set by the student
     * @param attributes a container for data passed from controller to jsps
     * @return mapping to view learning material
     */
    @PostMapping("/add-rating")
    public ModelAndView addRating(@ModelAttribute("userSession") HashMap<String, Object> userSession,
                                  @RequestParam Map<String, String> parameters, RedirectAttributes attributes) {
        LearningMaterial learningMaterial = new LearningMaterial();
        Student student = (Student) userSession.get("Student");
        String[] tempLearningMaterial = (String[]) userSession.get("selectedLearningMaterial");
        learningMaterial.setLearningMaterialID(tempLearningMaterial[0]);
        learningMaterial.setAverageRating(parameters.get("rating"));
        learningMaterial.updateRating(student.getUsername());
        attributes.addFlashAttribute("ratingMessage", "Your rating was submitted.");
        return new ModelAndView("redirect:/studentlearningmaterial");
    }

    /**
     *updates the time a student has spent on a piece of learning material
     *
     * @param userSession linked hash map
     */
    @RequestMapping("/update-time-spent")
    @ResponseStatus(value = HttpStatus.OK)
    public void updateTimeSpent(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        // Find how long the user has spent on the page by the difference between the current date/time and session date/time
        Date firstAccessed = (Date) userSession.get("FirstAccessed");
        Student student = (Student) userSession.get("Student");
        Date currentDateTime = new Date();
        double timeDifference = currentDateTime.getTime() - firstAccessed.getTime();
        // Get the time difference in minutes
        timeDifference = (timeDifference / (1000 * 60)) % 60;
        // Reinsert the first time accessed as the current time, so after another interval passes, the same difference is added
        userSession.put("FirstAccessed", new Date());
        // Add the time difference to the database
        LearningMaterial learningMaterial = new LearningMaterial();
        String[] selectedLearningMaterial = (String[]) userSession.get("selectedLearningMaterial");
        learningMaterial.setLearningMaterialID(selectedLearningMaterial[0]);
        // We also need to find the latest time spent from the database and add the result
        String[][] tempLearningMaterial = (String[][]) learningMaterial.readAccessMaterial(student.getUsername());
        int newTimeSpent = Integer.parseInt(tempLearningMaterial[0][2]) + (int) timeDifference;
        learningMaterial.setAverageTimeSpent(String.valueOf(newTimeSpent));
        learningMaterial.updateAccessMaterialTimeSpent(student.getUsername());
    }

    /**
     * finds if the learning material has been access and adds time stamp and gives student a option to rate the material
     *
     * @param userSession linked hash map
     * @return mapping to view learning material
     */
    @GetMapping("/studentlearningmaterial")
    public ModelAndView learningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        // If access material doesn't exist for learning material the user selects
        if (!hasAccessedMaterial(userSession)) {
            // Create the new access information
            createAccessMaterial(userSession);
        } else {
            // Collect the current date/time that the student accessed the page and add it to the session
            userSession.put("FirstAccessed", new Date());
            // Add the rating to the user session if found
            UserSession.addRatingToSession(userSession);
        }
        return new ModelAndView("/public/student/content/learning-material/view");
    }

    /**
     * adds topics and learning material to the session
     *
     * @param userSession linked hash map
     * @return mapping for viewing learning material overview
     */
    @GetMapping("/studentviewlearningmaterial")
    public String viewLearningMaterial(@ModelAttribute("userSession") HashMap<String, Object> userSession){
        UserSession.addTopicsToSession(userSession);
        // Import the learning material
        createTempLearningMaterialSession(userSession);
        return "/public/student/content/learning-material/learningmaterial";
    }

    /**
     * gets auth and adds student to the session from it
     *
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to student overview
     */
    @GetMapping("/studentoverview")
    public String studentOverview(Model model, @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getName();
        if (!UserSession.doesSessionExist(userSession)) {
            UserSession.createUserSession("Student", authentication.getName(), userSession, model);
        }
        // Clear the learning material
        userSession.remove("selectedLearningMaterial");
        return "/public/student/index";
    }


    /**
     * @param parameters the group name of group to view
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to either group page or overview depending on if group name is selected
     */
    @PostMapping("/studentviewgroup")
    public String studentViewGroup(@RequestParam Map<String, String> parameters, Model model,
                                   @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        if(parameters.get("groupName") != null) {
            Group.getGroupDataFromName(parameters.get("groupName"), model, userSession);
            //read quizzes with the group ID and return them in an array
            return "/public/student/group";
        }else{
            model.addAttribute("studentOverviewMessage", "Please enter a group.");
            return "public/student/index";
        }
    }

    /**
     * starts a quiz for a student, shows first question
     *
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return call on setNextFunction with value of 1 for index for first question
     */
    @PostMapping("/studentcompletequiz")
    public String studentCompleteQuiz(Model model, @ModelAttribute("userSession") HashMap<String, Object> userSession){
        //create array of questions
        Quiz quiz = (Quiz)userSession.get("Quiz");
        Quiz.updateQuizQuestionData(quiz, model, userSession);
        return setNextQuestion(model,userSession, 1);
    }

    /**
     * Adds an answer of a student to the associated index in the answers array found from the question in the array
     * of questions
     *
     * @param parameters the next question number passed from jsp, dependant on which button is pressed for different
     *                   amounts
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return call to set next question
     */
    @PostMapping("/studentanswerquestion")
    public String studentAnswerQuestion(@RequestParam Map<String, String> parameters, Model model,
                                        @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Question question = (Question)userSession.get("Question");
        ArrayList<String> questionArray = question.questionToArray();
        ArrayList<String> answers = (ArrayList<String>) userSession.get("quizAnswers");
        ArrayList<ArrayList<String>> questions = (ArrayList<ArrayList<String>>) userSession.get("quizQuestions");
        int questionNumber = questions.indexOf(questionArray);
        if(questionNumber == -1){
            questionNumber = 0;
        }
        answers.set(questionNumber, parameters.get("answer"));
        userSession.put("quizAnswers", answers);
        //set next question in session
        return setNextQuestion(model, userSession, questionNumber+1);
    }

    /**
     * If students selects the next question, the index is found and passed onto set next question
     *
     * @param parameters the question number or a string of a question array
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return call to set next question
     */
    @PostMapping("/studentselectquestion")
    public String studentSelectQuestion(@RequestParam Map<String, String> parameters, Model model,
                                        @ModelAttribute("userSession") HashMap<String, Object> userSession){
        ArrayList<ArrayList<String>> questions = (ArrayList<ArrayList<String>>) userSession.get("quizQuestions");
        String questionParam = parameters.get("questionNumber");
        int questionChosen = 0;
        if (questionParam.equals("+1")){
            Question question = (Question)userSession.get("Question");
            ArrayList<String> questionArray = question.questionToArray();
            questionChosen = questions.indexOf(questionArray)+1;
            if(questionChosen >= questions.size()){
                questionChosen = questions.size() - 1;
            }
        }else{
            String[] questionChosenSArray = General.getArrayFromDataString(questionParam);
            int count = 0;
            for(ArrayList<String> question : questions){
                if(question.get(0).equals(questionChosenSArray[0])){
                    questionChosen = count;
                }else{
                    count += 1;
                }
            }
            if (questions.size() == 1){
                questionChosen = 1;
            }
        }
        //set next question in session
        questionChosen += 1;
        return setNextQuestion(model, userSession, questionChosen);
    }

    /**
     * Populates an array to show the results and compares with the correct answers to show if the students answers
     * are correct, and displays them appropriately. Students results are added to the quiz they have just completed,
     * an assignment submission is completed if the quiz is an assignment too.
     *
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to either assignment quiz complete or quiz complete
     * @author Ben
     */
    @PostMapping("/studentsubmitquiz")
    public String studentSubmitQuiz(Model model, @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        Student student = (Student)userSession.get("Student");
        ArrayList<String> answers = (ArrayList<String>) userSession.get("quizAnswers");
        ArrayList<ArrayList<String>> questions = (ArrayList<ArrayList<String>>) userSession.get("quizQuestions");
        Quiz quiz = (Quiz) userSession.get("Quiz");
        quiz.submitQuiz(student.getUsername(),answers,questions);
        ArrayList<ArrayList<String>> questionAnswers = new ArrayList<>();
        int i = 0;
        for (ArrayList<String> question : questions) {
            ArrayList<String> answersToShow = new ArrayList<>();
            answersToShow.add(question.get(2));
            answersToShow.add(answers.get(i));
            answersToShow.add(question.get(7));
            if (question.get(7).equals(answers.get(i))) {
                answersToShow.add("Correct");
                answersToShow.add(question.get(8));
            }else {
                answersToShow.add("Incorrect");
                answersToShow.add("0");
            }
            answersToShow.add(question.get(8));
            questionAnswers.add(answersToShow);
        }
        model.addAttribute("questionAnswers", questionAnswers);
        boolean isAssignment = (boolean) userSession.get("quizIsAssignment");
        if(isAssignment){
            Assignment assignment = new Assignment();
            assignment.setAssignmentID((String)userSession.get("assignmentID"));
            int total = 0;
            for (ArrayList<String> answer : questionAnswers){
                total += Integer.parseInt(answer.get(4));
            }
            assignment.submitAssignmentQuiz(student.getUsername(),Integer.toString(total));
            return "/public/student/content/assignment/quizcomplete";
        }else{
            return "/public/student/content/quiz/complete";
        }
    }

    /**
     * Loads the next question into the session using the index passed in and the questions array.
     *
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @param questionNumber the index of the desired question
     * @return mapping for question view
     * @author Ben
     */
    public String setNextQuestion(Model model,@ModelAttribute("userSession") HashMap<String, Object> userSession, int questionNumber){
        try {
            ArrayList<ArrayList<String>> questions = (ArrayList<ArrayList<String>>) userSession.get("quizQuestions");
            userSession.put("quizQuestions", questions);
            if(questionNumber > questions.size()){
                model.addAttribute("questionMessage", "You are on the last question. Either chose another or submit!");
            }else {
                ArrayList<String> questionValues = questions.get(questionNumber-1);
                Question question = new Question();
                question.setQuestionID(questionValues.get(0));
                question.setQuizID(questionValues.get(1));
                question.setName(questionValues.get(2));
                question.setChoice1(questionValues.get(3));
                question.setChoice2(questionValues.get(4));
                question.setChoice3(questionValues.get(5));
                question.setChoice4(questionValues.get(6));
                question.setCorrectIndex(questionValues.get(7));
                question.setMarkWeight(questionValues.get(8));
                userSession.put("Question", question);
            }
            return "/public/student/content/quiz/question";
        }catch (Exception e){
            e.printStackTrace();
            return "/public/student/content/quiz/question";
        }
    }

    /**
     * Gets the feedback from jsp form and updates it in the database
     * @param userSession linked hash map
     * @param fromStudent a boolean to see if function is from student
     * @return returns a 2D array list of the entry for the student in a group, their personal gaol and feedback
     */
    public String[][] getStudentGoalFeedback(@ModelAttribute("userSession") HashMap<String, Object> userSession, boolean fromStudent) {
        Group group = (Group) userSession.get("Group");
        Student student = new Student();
        if (fromStudent) {
            // A string is found in the Student session when this method is called on from the teacher class
            student = (Student) userSession.get("Student");
        } else {
            // Acquire the username from student
            student.setUsername((String) userSession.get("student"));
        }
        ArrayList<String> userGroupTableColumns = new ArrayList<>();
        userGroupTableColumns.add("PersonalGoal");
        userGroupTableColumns.add("Feedback");
        String condition = " WHERE " + "Username = " + "'" + student.getUsername() + "'" + " AND GroupID = " + "'" + group.getGroupID() + "'";
        return DatabaseCRUD.readAlt("User_Group", userGroupTableColumns, condition);
    }

    /**
     * Adds the student progress data to the session to display in the jsp
     * @param userSession linked hash map
     * @param model a container for data passed from controller to jsps
     * @return mapping for student progress
     * @throws SQLException
     */
    @GetMapping("/studentviewprogress")
    public String studentViewProgress(@ModelAttribute("userSession") HashMap<String, Object> userSession, Model model) throws SQLException {
        Student student = (Student)userSession.get("Student");
        String username = student.getUsername();
        UserSession.addTopicsToSession(userSession);
        getStudentProgressData(userSession, username, model);
        // Since there will only be one row, add the 1D array to the session
        userSession.put("studentGoalFeedback", getStudentGoalFeedback(userSession, true)[0]);
        return "/public/student/content/progress/personal";
    }

    /**
     * Finds all topics, then finds all learning material and assignments in the topic, finds the students data for
     * material and mark for assignment for each, then calculates averages for the students learning material and
     * assignments. Also adds data required for the progress graphs of the student
     *
     * @param userSession linked hash map
     * @param username the username of the student
     * @param model a container for data passed from controller to jsps
     * @throws SQLException
     * @author Ben
     */
    public static void getStudentProgressData(@ModelAttribute("userSession") HashMap<String, Object> userSession, String username, Model model) throws SQLException {
        LinkedHashMap<ArrayList<String>, ArrayList<ArrayList<ArrayList<String>>>> studentTopics = new LinkedHashMap<>();
        String[][] topics = (String[][])userSession.get("Topics");
        List<Map<Object,Object>> assignmentDataPoint = new ArrayList<Map<Object,Object>>();
        HashMap<Object, Object> assignmentDataMap;
        List<List<Map<Object,Object>>> assignmentData = new ArrayList<List<Map<Object,Object>>>();
        for (String[] topicData:topics){
            ArrayList<String> averages = new ArrayList<>();
            ArrayList<String> topicIdentifiers = new ArrayList<>();
            Topic topic = new Topic();
            topic.setTopicID(topicData[0]);
            String[][] materialIDs = topic.getLearningMaterialInTopic();
            String[][] assignmentIDs = topic.getAssignmentsInTopic();
            for (String[] materialID : materialIDs){
                LearningMaterial learningMaterial = new LearningMaterial();
                learningMaterial.setLearningMaterialID(materialID[1]);
                String[][] learningMaterialData = learningMaterial.readFromDatabase(learningMaterial.getLearningMaterialID());
                if (!learningMaterialData[0][5].equals("Hidden")) {
                    topic.studentLearningMaterialData.add(learningMaterial.getStudentMaterialData(username));
                }
            }
            double averageRating = 0;
            int averageTime = 0;
            int count = 0;
            int count1 = 0;
            for (ArrayList<String> sMaterialData : topic.studentLearningMaterialData){
                //calc average learning material time and rating + add to topic student averages
                if(sMaterialData != null) {
                    averageTime += Double.parseDouble(sMaterialData.get(1));
                    count += 1;
                    if (sMaterialData.get(2) != null && !sMaterialData.get(2).equals("N/A") ) {
                        averageRating += Double.parseDouble(sMaterialData.get(2));
                        count1 += 1;
                    }
                }
            }
            String avgTime = "0";
            String avgRating = "0";
            if (averageTime != 0){
                avgTime = Integer.toString(averageTime/count);
            }
            if (averageRating != 0) {
                avgRating = Double.toString(averageRating / count1);
                avgRating = avgRating.substring(0, 3);
            }
            averages.add(avgTime);
            averages.add(avgRating);

            for (String[] assignmentID : assignmentIDs){
                Assignment assignment = new Assignment();
                assignment.setAssignmentID(assignmentID[1]);
                assignment.updateClassFromDatabase();
                ArrayList<String> data = null;
                if(!assignment.getStatus().equals("Hidden")) {
                    data = assignment.getStudentPercentage(username);
                    //returns array of assignment data
                    topic.studentAssignmentData.add(data);
                    if (data.get(2) != null){
                        assignmentDataMap = new HashMap<Object, Object>();
                        assignmentDataMap.put("label", data.get(0));
                        assignmentDataMap.put("y1", data.get(1));
                        assignmentDataMap.put("y2", avgRating);
                        assignmentDataMap.put("y3", avgTime);
                        assignmentDataMap.put("x", data.get(2));
                        assignmentDataPoint.add(assignmentDataMap);
                }

                }
            }
            int count2 = 0;
            double averagePercent = 0;
            for (ArrayList<String> sAssignmentData : topic.studentAssignmentData){
                //calc average score (percent)
                if (sAssignmentData != null) {
                    averagePercent += Double.parseDouble(sAssignmentData.get(1));
                    count2 += 1;
                }
            }

            if(averagePercent == 0){
                averages.add("0");
            }else{
                averages.add(Double.toString(averagePercent/count2).substring(0, 4));
            }
            topic.studentAverages.add(averages);
            topicIdentifiers.add(topicData[0]);
            topicIdentifiers.add(topicData[1]);
            ArrayList<ArrayList<ArrayList<String>>>  studentTopicData = new ArrayList<>();
            studentTopicData.add(topic.studentAverages);
            studentTopicData.add(topic.studentLearningMaterialData);
            studentTopicData.add(topic.studentAssignmentData);
            studentTopics.put(topicIdentifiers, studentTopicData);
        }

        assignmentData.add(assignmentDataPoint);

        model.addAttribute("assignmentDataPoints", assignmentData);
        userSession.put("StudentTopicData",studentTopics);
    }

    /**
     * Updates the list of quizzes and splits them up by completed or not
     *
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping for viewing quizzes
     */
    @GetMapping("/studentviewquizzes")
    public String studentViewQuizzes(Model model, @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Quiz.updateQuizList(model,userSession);
        try {
            splitQuizzesByCompleted(userSession, model);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "/public/student/content/quiz";
    }

    /**
     * Splits the list of quizzes into completed and to do, with associated data to display
     * @param userSession linked hash map
     * @param model a container for data passed from controller to jsps
     * @author Ben
     */
    public void splitQuizzesByCompleted(@ModelAttribute("userSession") HashMap<String, Object> userSession, Model model) {
        ArrayList<ArrayList<String>> quizNames = (ArrayList<ArrayList<String>>) userSession.get("quizzesInGroup");
        ArrayList<ArrayList<String>> completedQuizzes = new ArrayList<>();
        ArrayList<ArrayList<String>> todoQuizzes = new ArrayList<>();
        int i = 0;
        for (ArrayList<String> quizName : quizNames) {
            // if quiz question is in the question submission -> add to completed
            Quiz quiz = new Quiz();
            quiz.setQuizID(quizName.get(1));
            quiz.updateClassFromDatabase();
            ArrayList<String> questions = quiz.getQuestionsInQuiz();
            if (questions != null) {
                ArrayList<ArrayList<String>> questionData = General.splitByAmount(questions, 9);
                if (!questionData.isEmpty()){
                    Question question = new Question();
                    //checks to see if the first question is answered, then all questions must be answered
                    question.setQuestionID(questionData.get(0).get(0));
                    Student student = (Student) userSession.get("Student");
                    boolean completed = question.isCompleted(student.getUsername());
                    ArrayList<String> quizCompleteData = new ArrayList<>();
                    ArrayList<String> quizToDoData = new ArrayList<>();
                    if (completed) {
                        quizCompleteData.add(quiz.getQuizID());
                        quizCompleteData.add(quiz.getName());
                        quizCompleteData.add(quiz.getTotalScore());
                        int totalMarks = 0;
                        for (ArrayList<String> quest : questionData) {
                            question.setQuestionID(quest.get(0));
                            String mark = question.getSubmissionMark(student.getUsername());
                            totalMarks += Integer.parseInt(mark);
                        }
                        quizCompleteData.add(Integer.toString(totalMarks));
                        String percentage = General.calcPercentage(Integer.toString(totalMarks), quiz.getTotalScore());
                        quizCompleteData.add(percentage);
                        completedQuizzes.add(quizCompleteData);

                    } else {
                        quizToDoData.add(quiz.getQuizID());
                        quizToDoData.add(quiz.getName());
                        quizToDoData.add(quiz.getTotalScore());
                        if (!quiz.getStatus().equals("hidden")) {
                            todoQuizzes.add(quizToDoData);
                        }
                    }
                    questionData.remove(0);
                    i += 1;
                }
            }
        }
        if(todoQuizzes.isEmpty()){
            model.addAttribute("toDoMessage", "You currently have no quizzes to complete!");
        }
        if(completedQuizzes.isEmpty()){
            model.addAttribute("completedMessage", "You not completed any quizzes yet!");
        }
        userSession.put("quizzesToDo", todoQuizzes);
        userSession.put("quizzesCompleted", completedQuizzes);

    }

    /**
     * Displays a quiz depending on the ID from the jsp
     * @param parameters the quiz ID selected by the user
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping for a quiz view or the quizzes
     */
    @GetMapping("/studentviewquiz")
    public String studentViewQuiz(@RequestParam Map<String, String> parameters, Model model,
                                     @ModelAttribute("userSession") HashMap<String, Object> userSession){
        if(parameters.get("quizData") != null) {
            String quizID = parameters.get("quizData");
            Quiz.setQuizData(quizID, model, userSession);
            userSession.put("quizIsAssignment", false);
            return "/public/student/content/quiz/view";
        }else{
            model.addAttribute("quizMessage", "Please select a quiz.");
            return  "/public/student/content/quiz";
        }
    }

    /**
     * Creates 3 array lists which are populated with assignments by type to be displayed in the jsp
     * @param userSession linked hash map
     * @return mapping to view assignments
     */
    @GetMapping("/studentviewassignments")
    public String studentViewAssignments(@ModelAttribute("userSession") HashMap<String, Object> userSession){
        Assignment assignment = new Assignment();
        ArrayList<ArrayList<String>> assignmentsToDo = new ArrayList<>();
        ArrayList<ArrayList<String>> assignmentsCompleted = new ArrayList<>();
        ArrayList<String> assignToDo = new ArrayList<>();
        ArrayList<String> assignComp = new ArrayList<>();
        Student student = (Student) userSession.get("Student");
        String username = student.getUsername();
        Group group = (Group) userSession.get("Group");
        assignment.setGroupID(group.getGroupID());
        ArrayList<ArrayList<String>> assignmentsFromDB = assignment.getAssignmentsFromGroupID();
        for (ArrayList<String> DBAssignment : assignmentsFromDB) {
            assignment.setName(DBAssignment.get(3));
            assignment.setAssignmentType(DBAssignment.get(2));
            assignment.setDueDateTime(DBAssignment.get(6));
            assignment.setStatus(DBAssignment.get(7));
            assignment.setMarkWeight(DBAssignment.get(4));
            assignment.setAssignmentID(DBAssignment.get(1));
            assignment.setQuestionPDFLink(DBAssignment.get(5));
            assignment.setTopicID(DBAssignment.get(8));
            String topicName = General.getTopicName(assignment.getTopicID());
            if (!assignment.getStatus().equals("Hidden")) {
                ArrayList<String> mark = assignment.submissionData(username);
                if (mark.get(0).equals("Incomplete")) {
                    assignToDo.add(assignment.getName());
                    assignToDo.add(assignment.getAssignmentType());
                    assignToDo.add(assignment.getMarkWeight());
                    assignToDo.add(assignment.getDueDateTime());
                    assignToDo.add(assignment.getAssignmentID());
                    assignToDo.add(assignment.getQuestionPDFLink());
                    assignToDo.add(topicName);
                } else if (mark.get(0).equals("unmarked")) {
                    assignComp.add(assignment.getName());
                    assignComp.add(assignment.getAssignmentType());
                    assignComp.add(mark.get(0));
                    assignComp.add(assignment.getMarkWeight());
                    assignComp.add("0");
                    assignComp.add(assignment.getDueDateTime());
                    assignComp.add(assignment.getQuestionPDFLink());
                    assignComp.add(topicName);
                    assignComp.add(mark.get(2));
                } else if (!mark.get(0).equals("unmarked") || !mark.get(0).equals("Incomplete")) {
                    assignComp.add(assignment.getName());
                    assignComp.add(assignment.getAssignmentType());
                    assignComp.add(mark.get(0));
                    assignComp.add(assignment.getMarkWeight());
                    assignComp.add(mark.get(1));
                    assignComp.add(assignment.getDueDateTime());
                    assignComp.add(assignment.getQuestionPDFLink());
                    assignComp.add(topicName);
                    assignComp.add(mark.get(2));
                }
            }
        }
        assignmentsToDo = General.splitByAmount(assignToDo, 7);
        assignmentsCompleted = General.splitByAmount(assignComp, 9);
        userSession.put("assignmentsToDo", assignmentsToDo);
        userSession.put("assignmentsCompleted", assignmentsCompleted);
        return "/public/student/content/assignment";
    }

    /**
     * when an assignment is selected, data is gathered from the assignment so the student can complete it
     *
     * @param parameters an array which is converted into a string by parameters
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping either to submit pdf or begin quiz
     */
    @PostMapping("/studentcompleteassignment")
    public String studentCompleteAssignment(@RequestParam Map<String, String> parameters, Model model,
                                            @ModelAttribute("userSession") HashMap<String, Object> userSession){
        if(parameters.get("assignmentData") != null) {
            //student submit assignment here
            String assignmentDataString = parameters.get("assignmentData");
            String[] assignmentData = General.getArrayFromDataString(assignmentDataString);
            if (assignmentData[1].equals("Quiz")) {
                Assignment assignment = new Assignment();
                assignment.setAssignmentID(assignmentData[4]);
                String quizID = assignment.getQuizIDFromAssignmentID();
                Quiz quiz = new Quiz();
                quiz.setQuizID(quizID);
                try {
                    quiz.updateClassFromDatabase();
                    userSession.put("Quiz", quiz);
                    Quiz.updateQuizQuestionData(quiz, model, userSession);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                userSession.put("quizIsAssignment", true);
                userSession.put("assignmentID", assignmentData[4]);
                return "/public/student/content/quiz/view";
            } else {
                // Add the selected assignment to the user session
                userSession.put("assignmentPDF", assignmentData);
                return "/public/student/content/assignment/submitpdf";
            }
        }else{
            model.addAttribute("assignmentMessage", "Please select an assignment.");
            return "/public/student/content/assignment";
        }
    }

    /**
     * adds a pdf to AWS storage
     * @param file the file to upload
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @param bucket a collection of files in AWS
     * @return a link to aws file storage with folder and filename
     * @author Jason
     */
    public String uploadContent(@RequestPart(value = "file") MultipartFile file, Model model,
                                @ModelAttribute("userSession") HashMap<String, Object> userSession,
                                String bucket) {
        Group group = (Group) userSession.get("Group");
        Student student = (Student) userSession.get("Student");
        String[] assignmentData = (String[]) userSession.get("assignmentPDF");
        // Create the filename and bucket location
        String filename = (assignmentData[0] + "-" + new Date().getTime() + ".pdf");
        String folder = (group.getGroupID() + "/" + student.username);
        // Upload the pdf file to AWS S3
        AwsCloud awsCloud = new AwsCloud();
        model.addAttribute("awsMessage", awsCloud.uploadFile(file, folder, filename, bucket));
        return "https://" + bucket + ".s3.eu-west-2.amazonaws.com" + "/" + folder + "/" + filename;
    }

    /**
     * submits an assignment of type pdf, creates instance and call to add to AWS
     *
     * @param attributes a container for data passed from controller to jsps
     * @param file the file the student submits
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to view assignments
     * @author Jason
     */
    @PostMapping("/submitassignmentpdf")
    public ModelAndView submitAssignmentPDF(RedirectAttributes attributes, @RequestPart(value = "file") MultipartFile file,
                                            Model model, @ModelAttribute("userSession") HashMap<String, Object> userSession) {
        // Upload the content to aws
        String filePath = uploadContent(file, model, userSession, "assignment-answers");
        // Add relevant information to the database
        Assignment assignment = new Assignment();
        Student student = (Student) userSession.get("Student");
        // Acquire the assignment ID from the name
        String[] assignmentPDF = (String[]) userSession.get("assignmentPDF");
        assignment.setName(assignmentPDF[0]);
        assignment.getIDFromName();
        // Upload submission details to the database
        assignment.submitAssignmentPDF(student.getUsername(), filePath);
        attributes.addFlashAttribute("completedMessage", "Your answers have been submitted.");
        return new ModelAndView("redirect:/studentviewassignments");
    }

}