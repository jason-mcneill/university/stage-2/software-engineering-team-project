<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${userSession.Student.username} - Quiz Completed</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - QUIZ COMPLETE</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Assignment Complete!</h1>
<h2>Question Summary:</h2>
<table>
    <tr>
        <th>Question</th>
        <th>Your Choice</th>
        <th>Correct Choice</th>
        <th>Result</th>
        <th>Mark</th>
        <th>/ Out Of?</th>
    </tr>
    <ul>
        <c:forEach var="question" items="${questionAnswers}">
            <tr>
                <td>${question[0]}</td>
                <td>${question[1]}</td>
                <td>${question[2]}</td>
                <td>${question[3]}</td>
                <td>${question[4]}</td>
                <td>/ ${question[5]}</td>
            </tr>
        </c:forEach>
    </ul>
</table>
<p><a href="/studentviewassignments">Back to ${userSession.Group.name} Assignments</a></p>

</body>
</html>
