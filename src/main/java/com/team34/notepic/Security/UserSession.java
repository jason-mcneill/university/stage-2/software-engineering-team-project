package com.team34.notepic.Security;

import com.team34.notepic.Content.LearningMaterial;
import com.team34.notepic.Content.Topic;
import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.Group;
import com.team34.notepic.User.Admin;
import com.team34.notepic.User.Student;
import com.team34.notepic.User.Teacher;
import com.team34.notepic.User.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserSession {
    /**
     * gets the user session
     * @return a linked hash map stored in the user session
     */
    @ModelAttribute("userSession")
    public LinkedHashMap<String, Object> setUpUserSession() {
        return new LinkedHashMap<String, Object>();
    }

    /**
     * Adds the appropriate instance for the user to the session
     *
     * @param role the user role - admin/teacher/student
     * @param username the username of the user
     * @param userSession linked hash map
     * @param model a container for data passed from controller to jsps
     * @author Ian & Jason
     * */
    public static void createUserSession(String role, String username, @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession, Model model) {
        HashMap<String, String> results = new HashMap<>();
        HashMap<String, User> selectedUserType = new HashMap<>();
        ArrayList<String> tables = new ArrayList<>();
        ArrayList<String> primaryKey = new ArrayList<>();
        ArrayList<String> conditions = new ArrayList<>();
        ArrayList<String> joinedTableColumns = new ArrayList<>();
        joinedTableColumns.add("Username"); joinedTableColumns.add("Password"); joinedTableColumns.add("UserRole");
        joinedTableColumns.add("Firstname"); joinedTableColumns.add("Lastname"); joinedTableColumns.add("DOB");
        joinedTableColumns.add("Email");
        tables.add("Users");
        primaryKey.add("Username");
        primaryKey.add("Username");
        conditions.add(" WHERE " + "Users.Username = " + "'" + username + "'" + "");
        // Add the groups the user is in to the session
        ArrayList<String> groupNames = new ArrayList<>();
        groupNames = Group.getGroupNamesFromUsername(model, username);
        userSession.put("groupNames", groupNames);
        switch (role) {
            case "Admin":
                tables.add("Admin");
                joinedTableColumns.add("NameOfSchool");
                results = DatabaseCRUD.readJoined(tables, primaryKey, conditions, joinedTableColumns);
                selectedUserType.put("Admin", new Admin());
                Admin admin = (Admin) createUserInstance(selectedUserType, results).get("Admin");
                admin.setNameOfSchool(results.get("NameOfSchool"));
                // Add admin details to the session, using Admin as a key to search the contents
                userSession.put("Admin", admin);
                break;
            case "Teacher":
                tables.add("Teacher");
                joinedTableColumns.add("Title");
                joinedTableColumns.add("TeacherRole");
                results = DatabaseCRUD.readJoined(tables, primaryKey, conditions, joinedTableColumns);
                selectedUserType.put("Teacher", new Teacher());
                Teacher teacher = (Teacher) createUserInstance(selectedUserType, results).get("Teacher");
                teacher.setTitle(results.get("Title"));
                teacher.setTeacherRole(results.get("TeacherRole"));
                // Add teacher details to the session, using Teacher as a key to search the contents
                userSession.put("Teacher", teacher);
                break;
            case "Student":
                tables.add("Student");
                joinedTableColumns.add("YearOfStudy");
                results = DatabaseCRUD.readJoined(tables, primaryKey, conditions, joinedTableColumns);
                selectedUserType.put("Student", new Student());
                Student student = (Student) createUserInstance(selectedUserType, results).get("Student");
                student.setYearOfStudy((results.get("YearOfStudy")));
                userSession.put("Student", student);
                break;
        }
    }

    /**
     * creates instance of a user type
     *
     * HELP PLZ JASON
     *
      * @param selectedUserType
     * @param results
     * @return
     */
    private static HashMap<String, User> createUserInstance(HashMap<String, User> selectedUserType, HashMap<String, String> results) {
        for (Map.Entry<String, User> userType : selectedUserType.entrySet()) {
            selectedUserType.get(userType.getKey()).setUsername(results.get("Username"));
            selectedUserType.get(userType.getKey()).setPassword(results.get("Password"));
            selectedUserType.get(userType.getKey()).setFirstName(results.get("Firstname"));
            selectedUserType.get(userType.getKey()).setLastName(results.get("Lastname"));
            selectedUserType.get(userType.getKey()).setDOB(results.get("DOB"));
            selectedUserType.get(userType.getKey()).setEmail(results.get("Email"));
        }
        return selectedUserType;
    }

    /**
     * finds if the user is currently stored in the session
     *
     * @param userSession linked hash map
     * @return boolean - if the session exists or not
     * @author Jason
     */
    public static boolean doesSessionExist(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        if ((userSession.get("Admin") == null) && (userSession.get("Teacher") == null) && userSession.get("Student") == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * clears all values from the hash map
     *
     * @param userSession linked hash map
     */
    public static void clearUserSession(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        userSession.clear();
    }

    /**
     * Adds all topics in a group to the user session
     *
     * @param userSession linked hash map
     * @author Jason
     */
    public static void addTopicsToSession(@ModelAttribute("userSession") HashMap<String, Object> userSession)  {
        // Clear the existing topic in session
        userSession.remove("Topics");
        Topic topic = new Topic();
        Group group = (Group) userSession.get("Group");
        topic.setGroupID(group.getGroupID());
        String[][] topics = topic.readFromDatabase(false);
        userSession.put("Topics", topics);
    }

    /**
     * adds progress data in a topic to the session so that it can be shown in the progress jsp
     *
     * @param userSession  linked hash map
     * @author Ben
     */
    public static void addTopicDataToSession(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        // Clear the existing topic in session
        userSession.remove("Topics");
        Topic topic = new Topic();
        Group group = (Group) userSession.get("Group");
        topic.setGroupID(group.getGroupID());
        String[][] topics = topic.readFromDatabase(false);
        topics = topic.addProgressDataToTopics(topics, Teacher.getStudentsInGroup(userSession));
        userSession.put("Topics", topics);
    }

    /**
     * Adds a rating for a piece of learning material to the session
     *
     * @param userSession linked hash map
     * @author Jason
     */
    public static void addRatingToSession(@ModelAttribute("userSession") HashMap<String, Object> userSession) {
        LearningMaterial learningMaterial = new LearningMaterial();
        String[] selectedLearningMaterial = (String[]) userSession.get("selectedLearningMaterial");
        Student student = (Student) userSession.get("Student");
        learningMaterial.setLearningMaterialID(selectedLearningMaterial[0]);
        String[][] accessMaterial = learningMaterial.readAccessMaterial(student.getUsername());
        userSession.put("Rating", accessMaterial[0][4]);
    }
}
