<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Group.name} - Topics</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
</head>
<body>

<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>TEACHER - VIEW TOPICS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>

<h1>Your topics</h1>
<h2>Create</h2>
<i>Create a new topic for ${userSession.Group.name}</i><br><br>
<form action="/createtopic" method="POST">
    <label for="createtopicname">Topic name:</label><br>
    <input type="text" id="createtopicname" name="createtopicname"><br>
    <input type="submit" value="Add">
</form>

<h2>Amend or Delete</h2>

<div class="left-pane">
    <select id="topics" name="topics" onclick="showButtons()">
        <option value="" selected disabled>${userSession.Group.name} Topics</option>
        <c:forEach begin="0" end="${fn:length(userSession.Topics)}"
                   var="topic" varStatus="loop" items="${userSession.Topics}">
            <option value="${topic[0]}">${topic[1]}</option>
        </c:forEach>
    </select><br><br>

    <div id="buttons" style="display: none">
        <button id="amend" onclick="showAmendDetails()">Amend</button>
        <button id="delete" onclick="showDeleteDetails()">Delete</button>
    </div>

    <div id="amendDetails" style="display: none">
        <form action="/amendtopic" method="POST">
            <br>
            <label for="topicname">Topic Name:</label><br>
            <input type="text" id="topicname" name="topicname"><br><br>
            <%--        This value will be set via javascript--%>
            <input type="hidden" id="topicamendid" name="topicamendid" value="">
            <input type="submit" value="Update">
        </form>
    </div>

    <div id="deleteDetails" style="display: none">
        <form action="/deletetopic" method="POST">
            <br>
            <i>Confirm deletion - you will have to update your learning material topics</i><br><br>
            <%--        This value will be set via javascript--%>
            <input type="hidden" id="topicdeleteid" name="topicdeleteid" value="">
            <input type="submit" value="Confirm">
        </form>
    </div>

    <p>${topicMessage}</p>
</div>

<script>
    // Set the hidden topic id value when the user clicks on a topic via the selector
    var displayButtons = document.getElementById("topics");
    displayButtons.addEventListener("click", function(){
        var value = document.querySelector("#topics").value;
        document.getElementById("topicdeleteid").value = value;
        document.getElementById("topicamendid").value = value;
    });

    function showButtons() {
        document.getElementById("buttons").style.display = "inline";
    }

    function showAmendDetails() {
        document.getElementById("amendDetails").style.display = "inline";
        document.getElementById("deleteDetails").style.display = "none";
        // Disable the amend button
        document.getElementById("amend").disabled = true;
        // Enable the delete button
        document.getElementById("delete").disabled = false;
    }
    function showDeleteDetails() {
        document.getElementById("deleteDetails").style.display = "inline";
        document.getElementById("amendDetails").style.display = "none";
        // Disable the delete button
        document.getElementById("delete").disabled = true;
        // Enable the amend button
        document.getElementById("amend").disabled = false;
    }
    $("form[name='amendDetails']").validate({
        rules: {
            topicamendid: {
                required: true,
            },
            topicname: {
                required: true,
            },
        },
        deleteDetails: {
            topicdeleteid: {
                required: "Please enter the Material Name.",
            },
        }
    })
</script>

</body>
</html>