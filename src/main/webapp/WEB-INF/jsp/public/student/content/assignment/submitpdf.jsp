<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${userSession.Student.username} - Submit PDF Assignment</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - SUBMIT PDF</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>Title: ${userSession.assignmentPDF[0]}</h1>
<h3>Date Due: ${userSession.assignmentPDF[3]}</h3>
<iframe id="assignment" width="900px" height="400px" src="${userSession.assignmentPDF[5]}"></iframe>
<h3>Submit your answers below</h3>
<form action="/submitassignmentpdf" name="pdfForm" enctype="multipart/form-data" method="POST">
    <input type="file" accept=".pdf" id="file" name="file"><br><br>
    <input type="submit" value="Submit">
</form>
</body>
<script>
    $("form[name='pdfForm']").validate({
        rules: {
            file: {
                required: true,
            },

        }
    })
</script>
</html>