<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Admin - Group Overview</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/admin.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>ADMINISTRATOR - VIEW GROUP</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<h1>${userSession.Group.name} Members: </h1>
<table>
    <tr>
        <th>Role</th>
        <th>Username</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
    </tr>
    <ul>
        <c:forEach var="user" items="${userSession.groupUsersData}">
            <tr>
                <td>${user[3]}</td>
                <td>${user[0]}</td>
                <td>${user[1]}</td>
                <td>${user[2]}</td>
                <td>${user[4]}</td>
            </tr>

        </c:forEach>
    </ul>
</table>
<p>${viewGroupMessage}</p>
<br>
<form id="addUser" name="addUser" action="/addgroupuser" method="GET">
    <label for="usernameToAdd">Enter a username to add to group: </label>
    <input type="text" id="usernameToAdd" name="usernameToAdd">
    <input type="submit" value="Submit">
</form>
<form id="deleteUser" name="deleteUser" action="/removegroupuser" method="GET">
    <label for="usernameToDelete">Enter a username to remove from group: </label>
    <input type="text" id="usernameToDelete" name="usernameToDelete">
    <input type="submit" value="Submit">
</form>
<p>${message}</p>
<p><a href="/adminoverview">Admin Overview</a></p>
</body>
<script>
    $("form[name='addUser']").validate({
        rules: {
            usernameToAdd:{
                required: true,
            },
        }
    })

    $("form[name='deleteUser']").validate({
        rules: {
            usernameToDelete: {
                required: true,

        }
    }})
</script>
</html>