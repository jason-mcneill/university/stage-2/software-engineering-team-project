package com.team34.notepic.Content;
import com.team34.notepic.Database.DatabaseCRUD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Topic {

    private String topicID;
    private String topicName;
    private String groupID;
    private String averageTime;
    private String averageRating;
    private String averagePercent;

    public ArrayList<ArrayList<String>> studentAverages = new ArrayList<>();
    public ArrayList<ArrayList<String>> studentLearningMaterialData = new ArrayList<>();
    public ArrayList<ArrayList<String>> studentAssignmentData = new ArrayList<>();

    public void setTopicID(String topicID) { this.topicID = topicID; }
    public void setTopicName(String topicName) { this.topicName = topicName; }
    public void setGroupID(String groupID) { this.groupID = groupID; }
    public void setAverageTime(String averageTime) { this.averageTime = averageTime; }
    public void setAverageRating(String averageRating) { this.averageRating = averageRating; }
    public void setAveragePercent(String averagePercent) { this.averagePercent = averagePercent; }

    public String getTopicID() { return this.topicID; }
    public String getTopicName() { return this.topicName; }
    public String getGroupID() { return this.groupID; }
    public String getAverageTime() { return this.averageTime; }
    public String getAverageRating() { return this.averageRating; }
    public String getAveragePercent() { return this.averagePercent; }

    /**
     * Adds a new topic to the database.
     * @author Jason
     */
    public void addToDatabase(){
        Map<String,String> tableContents = new HashMap<String, String>();
        tableContents.put("TopicName", this.topicName);
        tableContents.put("GroupID", this.groupID);
        String tableName = "Topics";
        DatabaseCRUD.create(tableName, tableContents);
    }

    /**
     * Deletes a topic from the database.
     * @author Jason
     */
    public void deleteFromDatabase() {
        DatabaseCRUD.delete("Topics", " WHERE TopicID = '" + this.topicID + "'");
    }
    /**
     * reads a either a single topic in a 2D array from a topic ID or multiple topics in the 2D array gathered from
     * using the group ID so finds all topics in the group
     *
     * @param singleTopic Boolean to represent if only one topic is to be searched or multiple.
     * @return A two-dimensional array of topics.
     * @author Jason
     */
    public String[][] readFromDatabase(boolean singleTopic) {
        String condition;
        if (singleTopic) {
            // If singleTopic is true, we only want to acquire information about a single topic
            condition = " WHERE TopicID = " + "'" + this.topicID + "'";
        } else {
            // If false, we want information about all topics in the group
            condition = " WHERE GroupID = " + "'" + this.groupID + "'";
        }
        ArrayList<String> topicColumns = new ArrayList<>();
        topicColumns.add("TopicID");
        topicColumns.add("TopicName");
        topicColumns.add("GroupID");
        return DatabaseCRUD.readAlt("Topics", topicColumns, condition);
    }

    /**
     * Assigns student progress information to their respective topics.
     *
     * @param topics A two-dimensional array of topics
     * @param studentsInGroup An arraylist of all students in a given group
     * @return a String array of progress data
     * @author Ben
     * */
    public String[][] addProgressDataToTopics(String[][] topics, ArrayList<String> studentsInGroup) {
        ArrayList<ArrayList<String>> topicsArray = new ArrayList<>();
        //add topic averages to array
        for (String[] topic : topics){
            ArrayList<String> topicArray = new ArrayList<>();
            this.topicID = topic[0];
            ArrayList<String> averages = getTopicAverages(this.groupID, studentsInGroup);
            topicArray.addAll(Arrays.asList(topic));
            topicArray.addAll(averages);
            topicsArray.add(topicArray);
        }
        String[][] converted = new String[topicsArray.size()][];
        for (int i = 0; i < converted.length; i++)
            converted[i] = topicsArray.get(i).toArray(new String[topicsArray.get(i).size()]);
        return converted;
    }

    /**
     * Updates a topic using the latest value in the database using the instances topic ID.
     * @author Jason
     */
    public void updateClassFromDatabase() {
        String condition = " WHERE TopicID = " + "'" + this.topicID + "'";
        ArrayList<String> topicColumns = new ArrayList<>();
        topicColumns.add("TopicID");
        topicColumns.add("TopicName");
        topicColumns.add("GroupID");
        String[][] topic = DatabaseCRUD.readAlt("Topics", topicColumns, condition);
        this.topicName = topic[0][1];
        this.groupID = topic[0][2];
    }

    /**
     * Updates the topic information in the database.
     * @author Jason
     */
    public void updateDatabase() {
        String condition = "TopicID = '" + this.topicID + "'";
        DatabaseCRUD.update("Topics", "TopicName", this.topicName, condition);
    }

    /**
     * Gets all learning material assigned to a topic.
     * @return Returns a two-dimensional array of IDs corresponding to pieces of learning material
     * @author Ben
     */
    public String[][] getLearningMaterialInTopic() {
        ArrayList<String> columns = new ArrayList<>();
        columns.add("TopicID");
        columns.add("LearningMaterialID");
        String condition = " WHERE TopicID = " + "'" + this.topicID + "'";
        String[][] materialIDs = DatabaseCRUD.readAlt("LearningMaterial",
                columns, condition);
        return materialIDs;
    }

    /**
     * Gets all assignments assigned to a topic.
     *
     * @return Returns a two-dimensional array of IDs corresponding to pieces of learning material
     * @author Ben
     */
    public String[][] getAssignmentsInTopic(){
        ArrayList<String> columns = new ArrayList<>();
        columns.add("TopicID");
        columns.add("AssignmentID");
        String condition = " WHERE TopicID = " + "'" + this.topicID + "'";
        return DatabaseCRUD.readAlt("Assignments", columns, condition);
    }

    /**
     * For a all topics in a group, loops through a learning material in a topic, finds the student results for a piece
     * of learning material, calculates an average for the learning material, then for all the material in that topic,
     * finds a total material average for all the material in the topic.
     *
     * Also loops through all assignments in a topic, finds the average mark (%) for each assignment, then calculates an
     * average for all assignments in a topic.
     *
     * Does all this for each topic.
     *
     * @param groupID The ID of the group that contains the topic.
     * @param studentsInGroup An arraylist of students in the given group
     * @return An arraylist of the calulated averages by topic.
     * @author Ben
     */
    public ArrayList<String> getTopicAverages(String groupID, ArrayList<String> studentsInGroup) {
        ArrayList<String> averages = new ArrayList<>();
        String[][] materialIDs = getLearningMaterialInTopic();
        // iterate through the learning material in the topic and find the average time spent and rating for all the
        // pieces of learning material in the topic
        int totalAverageTime = 0;
        double totalAverageRating = 0;
        if (materialIDs.length != 0){
            int count = 0;
            int matAverageTime;
            double matAverageRating;
            for (String[] IDPair : materialIDs){
                LearningMaterial learningMaterial = new LearningMaterial();
                learningMaterial.setLearningMaterialID(IDPair[1]);
                String[][] lMat = learningMaterial.readFromDatabase(learningMaterial.getLearningMaterialID());
                matAverageTime = 0;
                matAverageRating = 0;
                if (lMat.length != 0){
                    int count2 = 0;
                    for (String[] mat : lMat){
                        matAverageTime += Integer.parseInt(mat[7]);
                        matAverageRating += Double.parseDouble(mat[8]);
                        count2 += 1;
                    }
                    matAverageTime = matAverageTime/count2;
                    matAverageRating = matAverageRating/count2;
                }
                totalAverageTime += matAverageTime;
                totalAverageRating += matAverageRating;
                count += 1;
            }
            totalAverageTime = totalAverageTime/count;
            totalAverageRating = totalAverageRating/count;
        }
        averages.add(Integer.toString(totalAverageTime));
        averages.add(Double.toString(totalAverageRating).substring(0,3));
        // iterate through all the assignments in the topic to find the total average mark (percent)
        this.setGroupID(groupID);
        Assignment assignment = new Assignment();
        ArrayList<ArrayList<String>> assignmentsData = assignment.getAssignmentDataForTopic(this.topicID, studentsInGroup);
        int count = 0;
        double avgPercentage = 0;
        for (ArrayList<String> assignmentData : assignmentsData){
            avgPercentage += Double.parseDouble(assignmentData.get(4));
            count += 1;
        }
        if (avgPercentage == 0){
            averages.add("0");
        }else {
            avgPercentage = avgPercentage / count;
            averages.add(Double.toString(avgPercentage).substring(0,4));
        }
        return averages;
    }
}
