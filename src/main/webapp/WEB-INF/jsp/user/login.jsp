<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="messages" />
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="https://www.thymeleaf.org"
      xmlns:sec="https://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/visitor.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>

<body>

<div class="absent-nav">
    <div class="module" style="width: 300px; margin-left: 40%; margin-right: 40%">
        <h1>Login:</h1>
        <form action="/process_login" method="POST">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username"><br>

            <label for="password">Password:</label><br>
            <input type="password" id="password" name="password"><br><br>

            <input type="submit" value="Submit">
        </form>
        <br>
        <c:if test="${param.error != null}">
            <div id="error">
                <spring:message code="message.badCredentials">
                </spring:message>
            </div>
        </c:if>
        <c:if test="${param.logout != null}">
            <p>
                <spring:message code="message.logout">
                </spring:message>
            </p>
        </c:if>
    </div>
</div>

</body>

</html>