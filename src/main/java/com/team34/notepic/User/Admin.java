package com.team34.notepic.User;

import com.team34.notepic.Security.UserSession;
import com.team34.notepic.Database.DatabaseCRUD;
import com.team34.notepic.Group;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;


@Controller
@SessionAttributes("userSession")
@RequestMapping(
        method={RequestMethod.POST,RequestMethod.GET}
)
public class Admin extends User {

    private String nameOfSchool;

    public void setNameOfSchool(String nameOfSchool) { this.nameOfSchool = nameOfSchool; }
    public String getNameOfSchool() {return this.nameOfSchool;}

    /**
     * Gets the user session
     * @return linked hash map
     * @author Jason
     */
    @ModelAttribute("userSession")
    public LinkedHashMap<String, Object> setUpUserSession() {
        return new LinkedHashMap<String, Object>();
    }

    /**
     * calls parent to add to user table, then adds admin extra info to the admin table
     * @throws SQLException
     * @author Ben
     */
    public void addToDatabase() throws SQLException {
        super.addToDatabase();
        Map<String, String> values = new HashMap<String, String>();
        values.put("Username", this.getUsername());
        values.put("NameOfSchool",this.getNameOfSchool());
        DatabaseCRUD.create("Admin", values);
    }


    @GetMapping("/adminlogin")
    public ModelAndView adminLogin(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        return new ModelAndView("/admin/login/index");
    }

    @GetMapping("/createpublicaccount")
    public String createPublicAccount(){
        return "/admin/createpublicaccount/index";
    }

    /**
     * selects a group from the groups the admin has added
     *
     * @param parameters the group name the user selects
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to either the user overview or group overview
     * @author Ben
     */
    @GetMapping("/adminviewgroup")
    public String viewGroup(@RequestParam Map<String, String> parameters, Model model,
                            @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        if (parameters.get("groupName") != null) {
            updateGroupUsersData(parameters.get("groupName"), model, userSession);
            return "/admin/overview/group";
        }else{
            model.addAttribute("selectGroupMessage", "Please select a group.");
            return  "/admin/overview/index";
        }
    }

    /**
     * Updates a array list of array lists in the user session which shows the users in a group
     *
     * @param groupName group name to update
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @author Ben
     */
    public void updateGroupUsersData(String groupName, Model model,
                                     @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        Group.getGroupDataFromName(groupName, model, userSession);
        ArrayList<String> usernames = (ArrayList<String>) userSession.get("usersInGroup");
        ArrayList<ArrayList<String>> groupUsersData = getUserDataFromUsernames(usernames);
        userSession.put("groupUsersData", groupUsersData);
    }

    /**
     * Loops through users in a group and adds data about them to an array list, these are added to another array list,
     * data is username, fn, ln, ext.
     *
     * @param usernamesInGroup an array of usernames in the group
     * @return array list of array lists which contain data on the users in the group
     * @author Ben
     */
    public ArrayList<ArrayList<String>> getUserDataFromUsernames(ArrayList<String> usernamesInGroup){
        ArrayList<ArrayList<String>> groupUsersData = new ArrayList<>();
        for (String username : usernamesInGroup) {
            groupUsersData.add(getUserDataFromUsername(username));
        }
        return groupUsersData;
    }

    /**
     * Gets data about a user
     *
     * @param username the username of the user to gather the data on
     * @return array list of data on the user
     * @author Ben
     */
    public ArrayList<String> getUserDataFromUsername(String username){
        HashMap<String, ArrayList<String>> userDataMap = new  HashMap<String, ArrayList<String>>();
        ArrayList<String> userTableColumns = new ArrayList<>();
        userTableColumns.add("Username");
        userTableColumns.add("Firstname");
        userTableColumns.add("Lastname");
        userTableColumns.add("UserRole");
        userTableColumns.add("Email");
        //userGroupTableColumns.add("Username");
        String condition = " WHERE " + "Username = " + "'" + username + "'";
        userDataMap = DatabaseCRUD.read("Users", userTableColumns, condition);
        // for-each loop to add all group ids from database to an array list
        return userDataMap.get("Username");
    }

    /**
     * Mapping for admin from login to overview
     *
     * @param userSession linked hash map
     * @param model a container for data passed from controller to jsps
     * @return mapping to overview
     * @author Jason
     */
    @GetMapping("/adminoverview")
    public ModelAndView viewOverview(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getName();
        if (!UserSession.doesSessionExist(userSession)) {
            UserSession.createUserSession("Admin", authentication.getName(), userSession, model);
        }
        return new ModelAndView("/admin/overview/index");
    }

    /**
     * removes a user (teacher/student) from a group if it matches a result in the database
     *
     * @param parameters the username of the user to remove
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to group overview
     * @author Ben
     */
    @PostMapping("/removegroupuser")
    public String removeUserFromGroup(@RequestParam Map<Object, Object> parameters, Model model,
                                      @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        String username = (String) parameters.get("usernameToDelete");
        String condition = " WHERE " + "Username = " + "'" + username + "'";
        try {
            DatabaseCRUD.delete("User_Group", condition);
            model.addAttribute("message", username + " has been removed from the group.");
        }
        catch(Exception e){
            model.addAttribute("message", "Cannot find the username - "+username + ".");
        }
        Group group = (Group) userSession.get("Group");
        updateGroupUsersData(group.getName(),model,userSession);
        return "/admin/overview/group";
    }

    /**
     * adds a user (teacher/student) from a group if it matches a result in the database
     *
     * @param parameters the username of the user to add
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to group overview
     * @author Ben
     */
    @PostMapping("/addgroupuser")
    public String addUserFromGroup(@RequestParam Map<Object, Object> parameters, Model model,
                                      @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        String username = (String) parameters.get("usernameToAdd");
        try {
            Map<String, String> values = new HashMap<String, String>();
            values.put("Username", username);
            Group group = (Group) userSession.get("Group");
            values.put("GroupID",group.getGroupID());
            DatabaseCRUD.create("User_Group", values);
            model.addAttribute("message", username + " has been added to the group.");
        }
        catch(Exception e){
            model.addAttribute("message", "Cannot find the username - "+username + ".");
        }
        Group group = (Group) userSession.get("Group");
        updateGroupUsersData(group.getName(),model,userSession);
        return "/admin/overview/group";
    }

    /**
     * deletes a group from the system
     *
     * @param parameters the group name
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping to admin overview
     * @author Ben
     */
    @GetMapping("/admindeletegroup")
    public String deleteGroup(@RequestParam Map<Object, Object> parameters, Model model,
                              @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        if (!parameters.get("nameToDelete").equals("")) {
            // delete all users in the group from users_group link table then delete the group
            Group group = new Group();
            group.setName((String) parameters.get("nameToDelete"));
            Admin admin = (Admin) userSession.get("Admin");
            String username = admin.getUsername();
            try {
                group.setGroupID(Group.getGroupIDFromGroupName(group.getName()));
                deleteFromUserGroup(group.getGroupID());
                group.deleteFromDatabase();
                model.addAttribute("groupmessage", "Selected group has been deleted.");
            } catch (Exception e) {
                model.addAttribute("groupmessage", "Error deleting the group.");
            }
            ArrayList<String> groupNames = Group.getGroupNamesFromUsername(model, username);
            userSession.put("groupNames", groupNames);
            // add admin to the group
        }else{
            model.addAttribute("groupmessage", "Please enter a group name.");
        }
        return "/admin/overview/index";
    }

    /**
     * Creates a group in the system
     *
     * @param parameters name of group to create
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * @return mapping back to admin overview
     * @author Ben
     */
    @GetMapping("/admincreategroup")
    public String createGroup(@RequestParam Map<Object, Object> parameters, Model model,
                              @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession) {
        if (!parameters.get("nameToCreate").equals("")) {
            Group group = new Group();
            Admin admin = (Admin) userSession.get("Admin");
            String username = admin.getUsername();
            try {
                group.setGroupID(null);
                group.setName((String) parameters.get("nameToCreate"));
                group.setAssignmentAverage("assignAvg");
                group.setQuizAverage("quizAvg");
                group.addToDatabase();
                group.updateFromDatabase();
                model.addAttribute("groupmessage", "A new group has been created.");
            } catch (Exception e) {
                model.addAttribute("groupmessage", "Error creating a new group.");
            }
            addAdminToGroup(group.getGroupID(), model, username, userSession);
            ArrayList<String> groupNames = Group.getGroupNamesFromUsername(model, username);
            userSession.put("groupNames", groupNames);
            // add admin to the group
        }else{
            model.addAttribute("groupmessage", "Please enter a group name.");
        }
        return "/admin/overview/index";
    }

    /**
     * When an admin creates a group, they are added to it so they can see all data in the group
     *
     * @param groupID group ID of group to add admin to
     * @param model  a container for data passed from controller to jsps
     * @param username username of admin to add to group
     * @param userSession linked hash map
     * @author Ben
     */
    public void addAdminToGroup(String groupID, Model model, String username, @SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        Map<String,String> tableContents = new HashMap<String, String>();
        tableContents.put("GroupID", groupID);
        tableContents.put("Username", username);
        DatabaseCRUD.create("User_Group", tableContents);
        model.addAttribute("creategroupmessage", "A new group has been created.");
    }


    /**
     * iterates through a csv file and adds data in the file to the system to populate it with users
     *
     * @param parameters the csv file
     * @param model  a container for data passed from controller to jsps
     * @return mapping back to page
     * @throws SQLException
     */
    @GetMapping("/adminimportpublicdata")
    public String importPublicData(@RequestParam Map<String, String> parameters, ModelMap model) throws SQLException {
        String filename = parameters.get("file");
        if(!filename.equals("")) {
            List<List<String>> records = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(",");
                    records.add(Arrays.asList(values));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            User user = new User();
            for (List<String> record : records){
                if(record == records.get(0)) continue;
                if (record.get(5).equals("Teacher")){
                    Teacher teacher = new Teacher();
                    teacher.setTitle(record.get(0));
                    teacher.setFirstName(record.get(1));
                    teacher.setLastName(record.get(2));
                    teacher.setEmail(record.get(3));
                    teacher.setDOB(record.get(4));
                    teacher.setTeacherRole(record.get(6));
                    String username = record.get(1).charAt(0) + record.get(2);
                    teacher.setUsername(username);
                    teacher.setPassword(parameters.get("teacherTempPassword"));
                    teacher.setUserRole("Teacher");
                    teacher.addToDatabase();
            }
                else{
                    Student student = new Student();
                    student.setFirstName(record.get(1));
                    student.setLastName(record.get(2));
                    student.setEmail(record.get(3));
                    student.setDOB(record.get(4));
                    student.setYearOfStudy(record.get(7));
                    student.setUserRole("Student");
                    String username = record.get(1).charAt(0) + record.get(2);
                    student.setUsername(username);
                    student.setPassword(parameters.get("studentTempPassword"));
                    student.addToDatabase();
                }
            }
            model.addAttribute("message","message val");
        }
        return "/admin/overview/importdata";
    }

    /**
     * @param userSession linked hash map
     * @return mapping to the admin import data from csv page
     */
    @GetMapping("/adminviewimportpublicdata")
    public ModelAndView adminViewImportPublicData(@SessionAttribute("userSession") LinkedHashMap<String, Object> userSession){
        return new ModelAndView("/admin/overview/importdata");
    }

    /**
     * Removes the admin from the group
     *
     * @param GroupID the group to remove the user from
     */
    public static void deleteFromUserGroup(String GroupID)  {
        String condition = " WHERE " + "GroupID = " + "'" + GroupID + "'";
        DatabaseCRUD.delete("User_Group",condition);
    }

}
