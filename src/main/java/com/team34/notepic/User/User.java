package com.team34.notepic.User;

import com.team34.notepic.Database.DatabaseCRUD;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ben
 */
public class User {
    protected String username;
    protected String password;
    protected String userRole;
    protected String firstName;
    protected String lastName;
    protected String DOB;
    protected String email;

    /**
     * Adds the user and necessary information into the database.
     * @throws SQLException Thrown when the SQL query constructed in DatabaseCrud.create is not valid.
     */
    public void addToDatabase() throws SQLException {
        Map<String,String> values = new HashMap<String, String>();
        values.put("Username",this.getUsername());
        values.put("Password",this.getPassword());
        values.put("UserRole",this.getUserRole());
        values.put("Firstname",this.getFirstName());
        values.put("Lastname",this.getFirstName());
        values.put("DOB",this.getDOB());
        values.put("Email",this.getEmail());
        DatabaseCRUD.create("Users", values);
    }

    /**
     * Updates the user's password
     * @param password The user's new password.
     * @throws SQLException Thrown when the SQL query constructed in DatabaseCrud.update is not valid.
     */
    public void updatePassword(String password) throws SQLException {
        String condition = "Username = '" + this.getUsername() + "'";
        DatabaseCRUD.update("Users", "Password", password, condition);
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    };
    public String getUsername() {return this.username;};

    public void setPassword(String password) {
        this.password = password;
    };
    public String getPassword() {return this.password;};

    public String getUserRole() {return this.userRole;};
    public void setUserRole(String userRole) {this.userRole = userRole;};

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    };
    public String getFirstName() {return this.firstName;};

    public void setLastName(String lastname) {this.lastName = lastname;};
    public String getLastName() {return this.lastName;};

    public void setDOB(String DOB) {this.DOB = DOB;};
    public String getDOB() {return this.DOB;};

    public void setEmail(String email) {this.email = email;};
    public String getEmail() {return this.email;};



}
