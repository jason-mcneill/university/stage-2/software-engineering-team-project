<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${userSession.Quiz.name} - Complete</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/student.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<body>
<h1>Quiz Complete!</h1>
<h2>Question Summary:</h2>
<table>
    <tr>
        <th>Question</th>
        <th>Your Choice</th>
        <th>Correct Choice</th>
        <th>Result</th>
        <th>Mark</th>
        <th>/ Out Of?</th>
    </tr>
    <ul>
        <c:forEach var="question" items="${questionAnswers}">
            <tr>
                <td>${question[0]}</td>
                <td>${question[1]}</td>
                <td>${question[2]}</td>
                <td>${question[3]}</td>
                <td>${question[4]}</td>
                <td>/ ${question[5]}</td>
            </tr>
        </c:forEach>
    </ul>
</table>
<p><a href="/studentviewquizzes">Back to ${userSession.Group.name} Quizzes</a></p>
</body>
</html>
