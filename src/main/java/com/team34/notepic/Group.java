package com.team34.notepic;

import com.team34.notepic.Database.DatabaseCRUD;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Ben
 */
public class Group {

    private String groupID;
    private String name;
    private String assignmentAverage;
    private String quizAverage;

    public void setGroupID(String groupID) { this.groupID = groupID; }
    public void setName(String name){ this.name = name; }
    public void setAssignmentAverage(String assignmentAverage){ this.assignmentAverage = assignmentAverage; }
    public void setQuizAverage(String quizAverage) {
        this.quizAverage = quizAverage;
    }

    public String getGroupID() { return this.groupID;}
    public String getName(){return this.name;}


    /**
     * Adds a new group to the Groups table in the database.
     */
    public void addToDatabase() {
        Map<String,String> tableContents = new HashMap<String, String>();
        tableContents.put("GroupID", this.groupID);
        tableContents.put("Name", this.name);
        tableContents.put("AssignmentAverage", this.assignmentAverage);
        tableContents.put("QuizAverage", this.quizAverage);
        String tableName = "Groups";
        DatabaseCRUD.create(tableName, tableContents);
    }

    /**
     * Updates instance of the class from the database
     */
    public void updateFromDatabase()  {
        ArrayList<String> groupsTableColumns = new ArrayList<>();
        HashMap<String,  ArrayList<String>> groupDataMap = new HashMap<>();
        groupsTableColumns.add("GroupID");
        groupsTableColumns.add("Name");
        groupsTableColumns.add("AssignmentAverage");
        groupsTableColumns.add("QuizAverage");
        String condition = " WHERE " + "Name = " + "'" + this.getName() + "'";
        groupDataMap = DatabaseCRUD.read("Groups", groupsTableColumns, condition);
        Set<String> setOfKeySet = groupDataMap.keySet();
        // for-each loop to add all group ids from database to an array list
        ArrayList<String> groupData = new ArrayList<>();
        groupData = groupDataMap.get("GroupID");
        this.setGroupID(groupData.get(0));
        this.setName(groupData.get(1));
        this.setAssignmentAverage(groupData.get(2));
        this.setQuizAverage(groupData.get(3));
    }

    /**
     * Deletes a group from the database.
     */
    public void deleteFromDatabase() {
        String condition = " WHERE " + "GroupID = " + "'" + this.getGroupID() + "'";
        DatabaseCRUD.delete("Groups",condition);
    }


    /**
     * Gets the usernames of all users in the group
     *
     * @param groupID for the group
     * @return array list of all usernames in group
     */
    public static ArrayList<String> getUsernamesFromID(String groupID){
        ArrayList<String> usernamesInGroup = new ArrayList<>();
        ArrayList<String> groupUserColumns = new ArrayList<>();
        groupUserColumns.add("Username");
        String condition = " WHERE " + "GroupID = " + "'" + groupID + "'";
        HashMap<String,  ArrayList<String>> usernames = new HashMap<>();
        usernames = DatabaseCRUD.read("User_Group", groupUserColumns, condition);
        Set<String> setOfKeySet = usernames.keySet();
        // for-each loop
        for(String key : setOfKeySet) {
            usernamesInGroup.addAll(usernames.get(key));
        }
        return usernamesInGroup;
    }

    /**
     * Get all the quizzes in the group with the group ID from the quizzes table
     *
     * @param groupID the ID of the group
     * @param model a container for data passed from controller to jsps
     * @return array list of quizzes
     */
    public static ArrayList<String> getQuizzesFromGroupID(String groupID, Model model) {
        ArrayList<String> quizzes = new ArrayList<>();
        ArrayList<String> quizTableColumns = new ArrayList<>();
        quizTableColumns.add("QuizID");
        quizTableColumns.add("GroupID");
        quizTableColumns.add("TotalScore");
        quizTableColumns.add("DisplayName");
        quizTableColumns.add("Description");
        quizTableColumns.add("Status");
        quizTableColumns.add("IsAssignment");
        //userGroupTableColumns.add("Username");
        String condition = " WHERE " + "GroupID = " + "'" + groupID + "'";
        HashMap<String,  ArrayList<String>> quizMap = DatabaseCRUD.read("Quizzes", quizTableColumns, condition);
        quizzes = quizMap.get("QuizID");
        // for-each loop to add all group ids from database to an array list
        return quizzes;
    }

    /**
     * Finds the group IDs of the groups a user is in
     *
     * @param username the username of the user
     * @return array list of group IDs
     */
    public static ArrayList<String> getGroupIDFromUsername(String username) {
        ArrayList<String> userGroupTableColumns = new ArrayList<>();
        userGroupTableColumns.add("GroupID");
        //userGroupTableColumns.add("Username");
        String condition = " WHERE " + "Username = " + "'" + username + "'";
        HashMap<String,  ArrayList<String>> groupIDsFromDatabase = DatabaseCRUD.read("User_Group", userGroupTableColumns, condition);
        Set<String> setOfKeySet = groupIDsFromDatabase.keySet();
        // for-each loop to add all group ids from database to an array list
        ArrayList<String> groupIDs = new ArrayList<>();
        for(String key : setOfKeySet) {
            groupIDs.addAll(groupIDsFromDatabase.get(key));
        }
        return groupIDs;
    }

    /**
     * finds the group names of all the groups a user is in
     *
     * @param model a container for data passed from controller to jsps
     * @param username the username if the user
     * @return an array list of group names
     */
    public static ArrayList<String> getGroupNamesFromUsername(Model model, String username){
        ArrayList<String> groupNamesArray = new ArrayList<>();
        // should get the teacher username from parameters when logged in or session
        try {
            //read user details
            ArrayList<String> groupIDs = getGroupIDFromUsername(username);
            HashMap<String,  ArrayList<String>> groupNames = new HashMap<>();
            for(String id : groupIDs){
                ArrayList<String> groupTableColumns = new ArrayList<>();
                //groupTableColumns.add("GroupID");
                groupTableColumns.add("Name");
                String condition = " WHERE " + "GroupID = " + "'" + id + "'";
                groupNames = DatabaseCRUD.read("Groups", groupTableColumns, condition);
                Set<String> setOfKeySetGN = groupNames.keySet();
                // for-each loop
                for(String key : setOfKeySetGN) {
                    groupNamesArray.addAll(groupNames.get(key));
                }
            }
            return groupNamesArray;
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("teacherOverviewMessage", "No Groups Found");
            return null;
        }
    }

    /**
     * Finds the users in a group and the group ID from a group name and adds it to the session
     *
     * @param groupName name of group to find
     * @param model a container for data passed from controller to jsps
     * @param userSession linked hash map
     * */
    public static void getGroupDataFromName(String groupName, Model model,
                                            @ModelAttribute("userSession") HashMap<String, Object> userSession){
        Group group = new Group();
        try{
            if (groupName != null) {
                group.setName(groupName);
                group.setGroupID(getGroupIDFromGroupName(groupName));
            }else{
                group = (Group)userSession.get("Group");
            }
            group.updateFromDatabase();
            userSession.remove("Quiz");
            userSession.remove("Question");
            userSession.remove("quizQuestions");
        }
        catch(Exception e){
            e.printStackTrace();
            model.addAttribute("viewGroupMessage", "Database Error.");
        }
        ArrayList<String> usernamesInGroup = getUsernamesFromID(group.getGroupID());
        if (usernamesInGroup.isEmpty()){
            model.addAttribute("viewGroupMessage", "Database Error.");
        }
        userSession.put("usersInGroup",usernamesInGroup);
        userSession.put("Group",group);
    }

    /**
     * finds the id of a group from a group name
     *
     * @param groupName the name of the group
     * @return the group ID
     */
    public static String getGroupIDFromGroupName(String groupName)  {
        ArrayList<String> groupColumns = new ArrayList<>();
        groupColumns.add("GroupID");
        //userGroupTableColumns.add("Username");
        String condition = " WHERE " + "Name = " + "'" + groupName + "'";
        HashMap<String,  ArrayList<String>> groupIDsFromDatabase = DatabaseCRUD.read("Groups", groupColumns, condition);
        Set<String> setOfKeySet = groupIDsFromDatabase.keySet();
        // for-each loop to add all group ids from database to an array list
        ArrayList<String> groupIDs = new ArrayList<>();
        for(String key : setOfKeySet) {
            groupIDs.addAll(groupIDsFromDatabase.get(key));
        }
        return groupIDs.get(0);
    }
}
