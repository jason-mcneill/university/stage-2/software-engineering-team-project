<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>${userSession.Student.username} - ${userSession.selectedLearningMaterial[3]}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/teacher.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>STUDENT - VIEW LEARNING MATERIAL</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>

<div id="details">
    <h2>${userSession.selectedLearningMaterial[3]} - View</h2>
    <p>Date Created: ${userSession.selectedLearningMaterial[4]}</p>
    <iframe width="400px" height="600px" src="${userSession.selectedLearningMaterial[2]}"></iframe>
</div><br>

<div id="ratingAvailable" style="display: none">
    <p>You gave this content a rating of: ${userSession.Rating}</p>
</div>

<div id="ratingUnavailable" style="display: none">
    <p>How difficult would you rate this content?</p>
    <form id="addRating" name="addRating" action="/add-rating">
        <input type="radio" id="verypoor" name="rating" value="1">
        <label for="verypoor">1 - Very Poor</label>
        <input type="radio" id="poor" name="rating" value="2">
        <label for="poor">2 - Poor</label>
        <input type="radio" id="okay" name="rating" value="3" checked="checked">
        <label for="okay">3 - Okay</label>
        <input type="radio" id="good" name="rating" value="4">
        <label for="good">4 - Good</label>
        <input type="radio" id="verygood" name="rating" value="5">
        <label for="verygood">5 - Very Good</label><br><br>
        <button type="submit" name="Submit">Submit</button>
    </form>
</div>

<p>${ratingMessage}</p>

<script>
    // Determine whether the addRating functionality should be displayed
    var rating = ${userSession.Rating}
    console.log(${userSession.Rating});
    if (rating === undefined) {
        document.getElementById("ratingUnavailable").style.display = "inline";
    } else {
        document.getElementById("ratingAvailable").style.display = "inline";
    }
</script>

<script>
    // Every minute the student is on the learning material, the time spent will be updated in the database
    setInterval(updateTimeSpent, 60 * 1000);
    function updateTimeSpent() {
        $.ajax({
            url : 'update-time-spent',
            success : function() {
                console.log("The student's time spent on this learning material has been updated.")
            },
        });
    }
</script>

</body>
</html>