<!DOCTYPE HTML>
<html>
<head>
    <title>Admin - Add Public Account</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/admin.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>

</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>ADMINISTRATOR - ADD ACCOUNTS</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>

<form id="registerPublic" name="registerPublic" action="/registerUser" method="POST">
    <h1>User Registration:</h1>
    <i>The username will be automatically generated.</i><br><br>
    <i>Set a temporary password for the student or teacher.</i><br>
    <i>Don't forget this as the teacher/student will need this to sign in.</i><br>

    <label for="password">Password:</label><br>
    <input type="password" id="password" name="password"><br>

    <label for="userRole">User Role:</label><br>
    <select id="userRole" name="userRole" onchange="displayUserFields()" required>
        <option value="" selected disabled>Select Role</option>
        <option value="Teacher">Teacher</option>
        <option value="Student">Student</option>

    </select><br>

    <label for="firstName">First Name:</label><br>
    <input type="text" id="firstName" name="firstName"><br>

    <label for="lastName">Last Name:</label><br>
    <input type="text" id="lastName" name="lastName"><br>

    <label for="dateOfBirth">Date of Birth:</label><br>
    <input type="text" id="dateOfBirth" name="dateOfBirth"><br>

    <label for="email">Email:</label><br>
    <input type="text" id="email" name="email"><br>


    <div id="teacherFields" style="display: none">
        <label for="title">Title:</label><br>
        <input type="text" id="title" name="title"><br>

        <label for="teacherRole">Teacher Role:</label><br>
        <input type="text" id="teacherRole" name="teacherRole"><br>
    </div>

    <div id="studentFields" style="display: none">
        <label for="yearOfStudy">Year of Study:</label><br>
        <input type="text" id="yearOfStudy" name="yearOfStudy"><br>
    </div>

    <br><input type="submit" value="Submit">
</form>

</body>
<p><a href="/adminoverview">Admin Overview</a></p>

<script>
    function displayUserFields() {
        var userRole = document.getElementById("userRole").value;
        var teacherFields = document.getElementById("teacherFields");
        var studentFields = document.getElementById("studentFields");


        switch (userRole) {
            case "Teacher":
                teacherFields.style.display = "block";
                studentFields.style.display = "none";
                break;
            case "Student":
                teacherFields.style.display = "none";
                studentFields.style.display = "block";
                break;
        }
    }

$("form[name='registerPublic']").validate({
    rules: {
        password: {
            required: true,
        },
        userRole:{
            required: true,
        },
        firstName:{
            required: true,
        },
        lastName:{
            required: true,
        },
        dateOfBirth:{
            required: true,
        },
        email:{
            required: true,
        },
        title:{
            required: true,
        },
        teacherRole:{
            required: true,
        },
        yearOfStudy:{
            required: true,
        },
    }
})
</script>
</html>
