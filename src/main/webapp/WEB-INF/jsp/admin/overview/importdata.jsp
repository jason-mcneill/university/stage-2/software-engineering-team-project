<!DOCTYPE HTML>
<html>
<head>
    <title>Admin - Import Public Data</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation/admin.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button/button.css"/>
</head>
<nav class="nav-bar">
</nav>
<div class="navbar topnav-default">
    <div class="navbar-left">
        <h1>ADMINISTRATOR - IMPORT DATA</h1>
    </div>
    <div class="navbar-icon-top">
        <div class="navbar-right">
            <a href="/userrequestpasswordchange" id="navtext">CHANGE PASSWORD</a>
            <a href="/logout"><button class="btn" id="login" style="margin-right: 10px">LOGOUT</button></a>
        </div>
    </div>
</div>
<body>
<strong>Enter the filename of the student and teacher data:</strong><br>
<i>Note: Just enter the filename, not '.csv'</i><br>
<form action="/adminimportpublicdata" name="importData" method="get" id="csvFilename">
    <div>
        <label for="file">Filename: </label>
        <input type="file" accept=".csv" id="file" name="file"><br><br>
        <label for="teacherTempPassword">Teacher's Temporary Password: </label>
        <input name="teacherTempPassword" id="teacherTempPassword"><br><br>
        <label for="studentTempPassword">Student's Temporary Password: </label>
        <input name="studentTempPassword" id="studentTempPassword"><br><br>
        <input type="submit" value="Import">
    </div>
</form>

<p><a href="/adminoverview">Admin Overview</a></p>
<script>
    $("form[name='importData']").validate({
        rules: {
                file: {
            required: true,
            },
            teacherTempPassword:{
            required: true,
            },
            studentTempPassword:{
            required: true,
            },
        }
})
</script>
</body>
</html>